<img src="https://atomjump.com/images/logo80.png">

# atomjumpcom-content-only
This repository is a public copy of the important content on https://atomjump.com, 
in case the servers should be down.



# Regional Laws

To use AtomJump in different countries, there may be special regional laws that apply. This page will try to keep track of the changes.

## United Kingdom

We are tracking the UK’s online safety bill: https://bills.parliament.uk/bills/3137. Several messaging products have stated that they may withdraw from the UK as a result of this bill (due to it breaking their end-to-end encryption).

Since AtomJump is fundamentally a ‘client-server’ system, we should be able to continue to operate the hosted version at atomjump.com within the UK, although some tweaks may be required for UK users.

As it stands, and to be sure, you may wish to consider setting up your own AtomJump Messaging Appliance, and add VPN access within your family or company. Download the Appliance. This will not be directly subject to the UK Online Safety Bill, since it is software you’re running privately.

For up-to-date news about the current state of AtomJump Messaging in the UK, please see this forum.

## Europe

We believe our software is compliant with the GDPR, but are open to any suggestions which would improve this. Currently our hosted version at atomjump.com is held on New Zealand-based servers, and this may be problematic in some countries, such as Russia, which require the software to be hosted on Russian-based servers. If in doubt, set up your own AtomJump Messaging Appliance and add VPN access Download the Appliance.

We do not currently have organisation representatives based in Europe, and Germany have expressed a desire for this to be the case from some messaging providers. We are always open to having direct discussions about how we can improve our services, and if there is a strong need for local representation we can attempt to set this up, however, please keep in mind that we are a non-profit and it may not be feasible from a financial perspective.

Although we are not yet subject to the EU Digital Service Act (DSA) or the EU Digit Markets Act (DMA), we are trying to be compliant, in anticipation. Due to our policy of allowing anyone to delete any content, most of the DSA is covered, we believe.

For up-to-date news about the current state of AtomJump Messaging in the EU, please see this forum.

## United States

We don’t think there are any federal or state issues around usage of AtomJump Messaging in the United States.

For up-to-date news about the current state of AtomJump Messaging in the USA, please see this forum.

## China

We are not aware of any issues around usage of AtomJump Messaging in China.

For up-to-date news about the current state of AtomJump Messaging in China, please see this forum.

## Rest of the World

For up-to-date news about the current state of AtomJump Messaging in other countries, please see this forum.

