# AtomJump Foundation - Use of profit funds

There are two main approaches for a non-profit organisation's excess profits:
 
* a) the excess earnings are re-invested in Research & Development, i.e. back into the organisation's primary purpose, and/or into reserves. With some level of reserve there may be a time when our subscriber numbers reduce, but we are still supporting staff, and these reserves could become useful.

* b) the excess earnings are used for other philanthropic reasons, which are not the primary purpose of the organisation, e.g. investing in local charities

If we are earning excess profits, we may also want to consider lowering our prices, and effectively keep it as near to "cost" as we can.
 
Now, on our public Manifesto, at https://atomjump.com/wp/manifesto/, we currently say that "Any income derived from this hosting goes directly back into the Foundation itself, supporting it's staff, systems, and other purchases deemed to be required to operate the Foundation."

So we're definitely currently in category a), above, rather than category b). A 'Yes' vote, or a 'Yes, under special circumstances' would switch us into category b).

This vote will conclude at 12pm on Feb 24th 2023.

## Question

Should the Foundation use any leftover profits from it's paid services (after administrative and staff costs have been subtracted), for any external philanthropic reasons (e.g. local charities) that are not mentioned in the manifesto, and are not it's primary purpose?

## Answers

* Yes
* No
* Yes, but only given special circumstances, e.g. larger than normal profits, or a natural disaster
