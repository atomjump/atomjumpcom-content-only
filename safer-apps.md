<img src="https://atomjump.com/images/logo80.png">

# atomjumpcom-content-only
This repository is a public copy of the important content on https://atomjump.com, 
in case the servers should be down.


# Safer Apps Program

"Safer Apps" is a certification program by the non-profit AtomJump Foundation to encourage a new level of app safety.

Why did we call this program "safer apps" rather than "safe apps"? Because, depending on how you define "safe", no app can ever be 100% "safe". There is always some level of security or enhancement needed to make an app safer. Therefore, it can only ever be an aspirational goal: which is why we've gone with the term "safer apps".	


## Safety Definition

Let's define what 'safe' means, as it can mean several things: 

* There is no level of "lock-in", or control, that the developer is trying to have over you by using their app. The acid test: If the app developer went out of business today, would you be stuck?
* Your private data stays private, and your personal, public data is not abused for commercial gain.
* The app cannot misuse your personal hardware e.g. your phone's camera, or microphone, in unexpected ways.
* Hackers cannot gain access to your personal data or device. This issue is an ongoing challenge for app developers, and the battle is never 100% won, but there are important steps which can be taken to help improve security.
* If there is any level of group discussion in the app, you can feel "safe" in the knowledge that you will not be verbally abused online. And yet you should still feel "safe" to be able to express a reasonable opinion. Your physical safety should also not be compromised.

We believe for an app to achieve the highest level of safety there are two crucial factors, which also form the requirements to be a part of this program: 

* __Criteria 1__:
The 'front-end' app that you put on your device is a 'Progressive Web App', meaning that it looks like an 'app' on your device home screen, with it's own icon, but it actually runs in your web browser when it is opened. These types of app come across to your phone in 'plain text', and are not a 'binary' file that could potentially be doing almost whatever it likes on your device. 

* __Criteria 2__:
The 'back-end' parts of the app (meaning where your personal data gets sent to, and where other processes on that data are carried out), are 100% open source, meaning that all of the software code that decides what to do with your personal data is publicly visible, and can be downloaded and independently analysed, or run from your own servers. This makes it almost impossible for app developers to misuse your data in unexpected ways, for either commercial gain or other exploitative reasons.


## App Earnings


You can find a list of 'Progressive Web Apps' at this app-store:
https://progressivewebapp.store/

and, while this is a good start, not every 'Progressive Web App' is a "safer app", as only the 1st criteria is satisfied; but not the 2nd criteria. There are many problematic apps in existence today (e.g. particularly in the social space) which can have a "safer" front-end, but which don't satisfy our safety definitions '1', '2','3', '4' and '5', from above, with their 'back-end'. 


## Open Source Apps

You can find a list of open source apps and tools on this page:
https://www.btw.so/open-source-alternatives

and, while this is a good set of tools, not every 'Open Source App' is a "safer app", as only the 2nd criteria is satisfied; but not the 1st criteria. If an open source app is distributed as a binary file via one of the app-stores, it is still potentially more vulnerable to security issues than a progressive web app.  For full safety, care should also be taken that the open source app stands fully independently of the entity behind it. E.g. Some open source projects are designed to make you dependent on the developer company's other commercial products.


## Are any apps I use on the iPhone and Google Play app-stores "safer apps"?

No, by definition, they are distributed as a binary app, and don't meet Criteria 1. While the companies behind these 1st generation app-stores will naturally try to sell you on their safety levels, we believe it is actually an impossible task to guarantee your "safety", as we define it here. 

* The app-stores themselves cannot modify a developer's binary file for an app, and if there are any dubious actions inserted by the developers themselves, the app-store cannot reliably detect it. While the app-stores may claim that there is a water-tight 'sandbox' around these binary apps, even their own libraries (which provide the sandbox for these apps), have been found to have leaks to the rest of the system. A simpler and significantly more restrictive sandbox around applications already exists, and it is used with 100% "plain text" apps, only, i.e. the web browser's sandbox.

* If the software libraries that a version of an app uses becomes out-of-date, or have a newly discovered security issue, the app-store likely won't know about it because they cannot see the source code that built the app (which only the app developers have access to, held privately). If the app developer never finds out about the security flaw (or has stopped publishing updates), there is no way that the app itself will ever be fixed up, leaving all users vulnerable without anyone's knowledge.

So, we believe we need a new set of safety standards, which is what this program is about. If your app fits Criteria 1 and Criteria 2, it is eligible to carry the ‘Certified Safer App’ badge that we publish on this page. 


## Social Apps


Social apps are a particularly popular category of app, and the Safety Definition 5, from above, is very relevant for them.

At AtomJump, we've been working on this problem for several years with our own AtomJump Messaging tool, and while there may be several different approaches that can be taken here, we have settled on the following solution:

"Anyone can say whatever they like" on the service, but, "anyone can also anonymously delete anything else that is written there".

So, you are accountable for whatever you say online. Anything that is said must be at least acceptable for everyone else on the group for it to stay there. Definitely, you can still make a reasoned argument, but if anyone finds it offensive, they can remove it.


## The "Certified Safer App" badge


This badge shows that an app fits the "Safer Apps" set of two required criteria, as judged by a panel from the AtomJump non-profit Foundation.

In future we will also hope to provide a full 'safer apps' app-store, but in the meantime, you can use this suggestions forum (https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps) to add your own general comments, or find/add apps in the categories listed below.

For the full icon asset files, tap on the icon below.
https://src.atomjump.com/atomjump/official-atomjump-branding/tree/master/assets/saferapps


## Show Me Some "Safer Apps"!

### Social / Messaging
Forget app lock-in.
https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps-social

Mastodon - social media without the exclusivity https://joinmastodon.org/
AtomJump Messaging - build your own safer messaging app https://atomjump.com


### Video Conferencing
Never be spied on again.
https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps-video

Jitsi video conferencing https://jitsi.org/


### Games
Instant action!
https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps-games

Floppy Bird https://nebez.github.io/floppybird/


### Business / Finance
Remove your dependencies on 'Big Finance'.
https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps-business

Tutanota Email https://tutanota.com
Invoice Ninja for invoices and finance https://www.invoiceninja.com/


### Office / Productivity
Escape the rat race.
https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps-office

Public software repository storage https://gitlab.com 
Remote screen sharing/controlling https://meshcentral.com  
Tutanota Email https://tutanota.com
Wordpress for blogging and website creation (Content Management System) https://wordpress.com or https://wordpress.org 
MedImage - transfer photos from your phone to your desktop http://medimage.co.nz 
For web document writing and editing https://www.collaboraoffice.com (usually combined with NextCloud)  
NextCloud for intranet files and embedded office software https://nextcloud.com/ 
AtomJump Live Wiki for internal company discussions and live logging http://atomjump.org/wp/live-wiki/ 


### Location
Don't be tracked.
https://atomjump.com/wp/safer-apps/#comment-open-ajps_saferapps-location

Maps and Navigation https://www.openstreetmap.org





