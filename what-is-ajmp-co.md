<img src="https://atomjump.com/images/logo80.png">

# atomjumpcom-content-only
This repository is a public copy of the important content on https://atomjump.com, 
in case the servers should be down.

# What is ajmp.co?

ajmp.co is a secure 'one-time-session' messaging forum service. It allows two or more people to group together and message or video chat each other, on a temporary space.

* Temporary 2-day secure messaging forums
* Send text, share photos, or video conference
* Free of charge
* Export content to your desktop with MedImage
* No app required

Find at:
http://ajmp.co


## Details

The non-profit AtomJump Foundation has created this service for use during the Covid-19 pandemic, and it has been initially designed to allow a doctor and patient to discuss their illness online, instead of face-to-face, so privacy is crucial. However, it can be used for any secure group discussions. Because sensitive information can be posted, or photos posted, the content of each new forum will auto-destruct after 2 days to minimize any risk of this information being stolen. If you wish to keep this information, please make sure you either export the spreadsheet of messages after your discussion, export a PDF, or save any images to your desktop (left-click the image for full detail, then right-click in your browser and click 'Save Image', or the equivalent).

You can alternatively use the MedImage Exporter (http://medimage.co.nz/how-to-standard-app/) after tapping the message itself on the forum (in the white-space to the right of an image), or by tapping into the 'Upload' page. To initiate a connection to your desktop, type "start medimage" in the forum comment box, which provides an option to export the forum photos, or a PDF of the whole forum, to the MedImage Server.

The whole service is available free of charge, and the Foundation intend to keep it this way. The source code is open-source and available here. https://src.atomjump.com/atomjump/doctor-patient This service hosting is subsidized by the private, permanent, forums held on atomjump.com, and is complemented by income from our sister company's medical connector tool product. http://medimage.co.nz/addon-ehrconnect/


## How does it work?

Each time you click 'New Session' a random forum is created with an 'unguessable', temporary, 9-digit code. The forum starter can then share this as a link to other members of the group, or, more securely, by calling the other members and giving them the 9-digit code to enter on their phone or desktop at ajmp.co.

Once each party enters the pop-up group, they can start entering messages / uploading photos / or join a video chat, anonymously and securely, immediately. There is no sign-up required and no requirement to download an app. The video chat feature will work without an app in most common browsers on a desktop, but iOS and Android users can download the free jitsi app (https://jitsi.org/) which presents itself on clicking the video button.

Once in the forum, you can give yourself a name in the Settings screen, and optionally sign in with an email address to receive notifications of new messages on the forum.


## What do I need to be wary of when sharing private information?

Any information online has a potential to be stolen or compromised. If you share the link to your private forum, you should be wary of who this may be seen by. Emails are not necessarily secure and can be hacked by other parties, so if the link within an email is compromised a 3rd party could see your messages.  It is much harder to hack a voice call, however (although not impossible), so this is the recommended route when sharing the 9-digit code within more private settings.

As an additional security layer, you have the option, inside a video call, to reject a new member if they come into the conversation.

Your whole forum's contents will be deleted 2 days after it's creation, including images. There are back-ups of the messaging contents kept for up to one day.

If your country has particular laws about the location of data online: currently the messaging data is stored on secure servers in New Zealand. Each photo has a unique 'unguessable' code associated with it, but if one of our systems were compromised these photos would be accessible (within the 2 day window). If you need this data to be stored completely on your own network, for maximum security, this can be done by installing AtomJump Messaging (http://atomjump.org), and creating your own internal interface. The Foundation have staff available to assist with this option, also.

More information on our privacy policy is available here. (privacy-policy.md)


