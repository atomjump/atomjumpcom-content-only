<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom-content-only
Ce référentiel est une copie publique du contenu important sur https://atomjump.com,
au cas où les serveurs seraient en panne.




# Guide de l'utilisateur de la messagerie


## Introduction
AtomJump Messaging est une « application Web progressive » multicouche, pour la messagerie en direct et la communication sur les forums. À la base se trouve une interface de messagerie qui gère un seul forum, et ce guide vous apprendra comment utiliser cette interface. Vous pouvez trouver les forums AtomJump en cliquant sur des liens particuliers sur un site Web, ce qui ouvrira le forum dans une boîte contextuelle de votre navigateur. N'importe quel site Web peut gérer ces forums, et AtomJump.com est l'un de ces sites Web.

Il existe une application de démarrage facultative (qui s'exécute dans un navigateur après avoir appuyé sur une icône de la page d'accueil), qui vous permet de rassembler vos forums préférés en un seul endroit et de vérifier les nouveaux messages de n'importe quel forum de messagerie AtomJump sur Internet. Tous ces composants sont open source et gratuits. AtomJump Messaging est une « application plus sûre » enregistrée.

* https://atomjump.com/wp/safer-apps/

Étant une application Web progressive, elle présente certains avantages par rapport aux applications de messagerie de l'App Store de 1ère génération :

* Vous pouvez discuter de quelque chose sur votre téléphone et passer immédiatement à une version de bureau de la même conversation, potentiellement sans même vous connecter.
* Vous pouvez inviter un groupe de personnes à un forum, qui ne se sont jamais connectées à AtomJump auparavant, simplement en envoyant un lien Web. Ils n’ont pas besoin de créer un compte.
* Pour qu'une nouvelle personne rejoigne la conversation, il n'est pas nécessaire de télécharger une application volumineuse, avec de nombreuses demandes de sécurité sur le téléphone.
* Chaque forum différent peut être géré par une organisation différente, et la fonctionnalité de ce forum est choisie par l'opérateur, étant assemblée à partir d'éléments de base (appelés « plugins »).

Pour commencer, vous pouvez essayer un forum sur par ex. https://atomjump.com

Pour obtenir l'application de démarrage AtomJump Messaging en option (iPhone/Android/Bureau) :

* https://app.atomjump.com

## Envoyer un message
Si vous êtes sur AtomJump.com, cliquez sur le gros bouton bleu « chat » pour accéder à un forum. D'autres sites utilisant le logiciel AtomJump afficheront la fenêtre contextuelle de messagerie après avoir cliqué sur un lien particulier.

Commencez à taper dans la case « Entrez votre commentaire » en haut et cliquez sur « envoyer ». C'est ça! Vous n’avez pas besoin de compte, même si vous vous appellerez « Anon XX » si vous ne vous êtes pas nommé.


## Pour rédiger un message
Vous pouvez saisir des raccourcis de messagerie courants pour différents types de médias.

__``Entrée``__ Crée

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``mdr`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭`` 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``S'il vous plaît, payez-moi 5 dollars`` <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1 bXAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY2 9kZSUzZFVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0ZWR HdWVzdCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">Veuillez me payer 5 dollars< /a> [avec lien vers Paypal]

``S'il vous plaît, payez-moi 3,30 livres`` <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvb Wp1bXAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxLYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW 5jeV9jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYl MjUzYU5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">Veuillez me payer 3,30 livres< /a> [avec lien vers Paypal]

``http://yoururl.com/yourvideo.mp4`` [affiche une icône vidéo grise, sur laquelle il faut cliquer]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [montre une image de la vidéo, sur laquelle cliquer]

``london@`` <a href="http://london.atomjump.com">london@</a> [avec lien vers http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [avec lien vers http://london.atomjump.com]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text. com">Développer</a> [avec lien vers http://a.very.long.link.with.lots.of.text.com]

``http://votrerl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [montre l'image]

``http://yoururl.com/youraudio.mp3`` [affiche une icône d'oreille grise sur laquelle cliquer]

``http://yoururl.com/yourdoc.pdf`` [affiche une icône PDF sur laquelle cliquer]


** Cette fonctionnalité est activée par défaut sur AtomJump.com, mais ce n'est peut-être pas le cas si votre fournisseur n'a pas inclus ces plugins.

## Vers la vidéoconférence

Lorsque vous êtes dans un forum, cliquez sur l'icône de l'appareil photo à côté du bouton « Envoyer ». Cela fournira un écran de confirmation avant d’accéder au forum vidéo. Certains navigateurs nécessitent que la caméra soit d'abord acceptée dans les autorisations de votre navigateur.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

Les utilisateurs de téléphones préféreront peut-être télécharger une application distincte.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

Pour rencontrer quelqu'un, il est préférable d'envoyer à d'autres personnes une URL du forum AtomJump (sur AtomJump.com, vous pouvez cliquer sur l'icône de partage) et de leur dire de cliquer sur la caméra. Vous trouverez plus d’instructions sur Jitsi. https://jitsi.org/user-faq/

Remarque : Ces forums vidéo sont publics, sauf si vous êtes sur un forum privé, et même si vous devez accepter les nouveaux utilisateurs avant de les rejoindre, vous devez ensuite être prudent avec ce que vous montrez aux autres personnes. Si vous souhaitez de la confidentialité ici, vous devez démarrer une session unique ou acheter un forum privé.

Méthode alternative : bien que vous devriez pouvoir cliquer sur le forum vidéo sur la plupart des plateformes, certaines plateformes, par ex. en particulier sur les iPad/Internet Explorer, vous pouvez également exécuter le bureau/l'application directement et y copier l'adresse du forum. Par exemple. Pour les pages AtomJump.com, l'adresse du forum sera « https://jitsi0.atomjump.com/[a unique code]ajps » suivi du sous-domaine. Vous pouvez également le trouver en regardant l’adresse de l’icône violette « lien ».


## Pour vous nommer
Cliquez sur « Paramètres » dans le coin inférieur gauche. Écrivez votre nom dans « Votre nom » et cliquez sur « Enregistrer les paramètres ». Vous n'avez pas besoin d'un compte complet, mais si vous souhaitez recevoir des notifications lorsque vous recevez un message privé, vous en aurez besoin (voir ci-dessous).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/your-name.png">


## Pour créer un compte

Créer un compte signifie que vous pouvez recevoir des messages privés. Cliquez sur « Paramètres » dans le coin inférieur gauche. Écrivez votre nom dans « Votre nom » et votre adresse e-mail dans « Votre e-mail ».

En option, vous pouvez ajouter un mot de passe sur le compte (ce qui empêche quelqu'un d'autre de lire vos messages privés). Si vous souhaitez l'ajouter, et c'est fortement recommandé, cliquez sur « Plus » et entrez votre mot de passe.

Cliquez sur « Enregistrer les paramètres ».

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

Vous recevrez des notifications par e-mail sur les messages privés, mais nous n'utilisons votre adresse e-mail à aucune autre fin.

## Pour vous connecter sur un nouveau PC ou téléphone
Utilisez le même e-mail et le même mot de passe que ceux que vous avez saisis sur votre premier appareil lors de la création du compte.

N'oubliez pas que si vous êtes sur un PC public, vous devez vous déconnecter chaque fois que vous quittez le PC, car vos informations de connexion seront mémorisées.

Remarque : chaque installation différente d'AtomJump Server possède sa propre base de données d'utilisateurs privés. Si votre compte a été configuré sur AtomJump.com, le compte peut être partagé entre d'autres propriétés AtomJump.com, mais il ne fonctionnera pas nécessairement dans toutes les versions de la fenêtre contextuelle de messagerie que vous pourriez trouver, car celles-ci sont gérées par d'autres sociétés. .

## Pour écrire un message privé à une seule personne
Cliquez ou appuyez sur le nom de la personne à côté de l’un de ses messages. Saisissez ensuite un message et appuyez sur « Envoyer à [leur nom] ».

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

Vous devrez cliquer sur « Go Public » pour commencer à publier des messages afin que tout le monde puisse les voir à nouveau.

## Pour écrire un message privé aux abonnés du forum
Cliquez sur « Devenir privé ». Saisissez les messages normalement.

Une fois que vous êtes prêt à revenir sur le forum public, cliquez à nouveau sur « Devenir public ».

## Pour répondre à un utilisateur de Twitter
Cliquez ou appuyez sur le nom de la personne à côté de l’un de ses messages. Saisissez ensuite un message et appuyez sur « Envoyer ».

Notez qu'une réponse Twitter sera toujours publique et vous n'avez pas besoin d'un compte Twitter. Un message sera automatiquement envoyé depuis notre compte AtomJump indiquant qu'un message les attend sur le forum sur lequel vous êtes actuellement. C'est à eux de décider s'ils liront leurs messages Twitter et y répondront.

## Pour devenir abonné au forum

Si vous <b><i>n'avez pas de compte</i></b> et souhaitez uniquement recevoir des notifications par e-mail (sans avoir à vous connecter), vous pouvez vous abonner en appuyant et en faisant passer l'oreille sur le vert. « oreille à l'écoute » dans la fenêtre de messagerie, qui vous demandera votre adresse e-mail. Cliquez ensuite sur « S'abonner ».

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>Pas abonné. Appuyez pour vous abonner.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>Abonnez-vouslit. Appuyez pour vous désabonner.</i>

Si vous <b><i>avez un compte</i></b>, vous devez d'abord vous connecter. Pour ce faire, cliquez sur « Paramètres », cliquez sur « Plus » pour la zone de mot de passe et entrez votre e-mail et votre mot de passe. Cliquez ensuite sur « Connexion ».

Maintenant, sur la page de messagerie, appuyez sur et passez l'oreille sur « l'oreille d'écoute » verte pour vous abonner à ce forum.

Pour <b>vous désabonner</b> d'un forum auquel vous êtes abonné, appuyez sur et passez à l'oreille rouge « oreille qui n'écoute pas » sur la page de messagerie.

## Pour créer un groupe

Entrez votre propre nom de groupe, ou un nom de groupe public existant, à l'endroit où il est indiqué « Entrez un nom de groupe » sur AtomJump.com, puis cliquez sur <span class="notranslate">« AtomJump »</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

par exemple. En saisissant <span class="notranslate">« Sailinglondon »</span> et en appuyant sur <span class="notranslate">« AtomJump »</span>, votre navigateur arrivera à l'adresse suivante :

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

Partagez le lien Web de votre forum avec votre groupe par e-mail, SMS, message instantané ou tout autre moyen. Vous pouvez trouver ce lien rapidement sur atomjump.com en appuyant sur l'icône « Partager » dans le coin supérieur droit de la page sous-jacente.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

Chaque individu peut ensuite choisir de devenir abonné au forum pour recevoir des notifications continues.

<b>Conseil avancé :</b> vous pouvez éventuellement ajouter <span class="notranslate">“/go/”</span> à la fin de votre lien Web, ce qui ouvrira immédiatement le forum en direct pour vos invités. (afin qu'ils n'aient pas à appuyer sur le gros bouton bleu « Go »).

<b>Adresses temporaires</b>

Si vous avez été envoyé à une adresse temporaire (par exemple http://sailinglondon.clst4.atomjump.com) et qu'on vous a dit qu'il faudrait 2 à 48 heures pour être pleinement actif, vous pouvez vérifier l'état du transfert de domaine sur https : //dnschecker.org. Entrez l'adresse prévue de votre page, par ex. "sailinglondon.atomjump.com" et recherchez. Ce que vous devez confirmer, c'est que toutes les adresses renvoyées sont différentes de "114.23.93.129" (qui est l'adresse personnelle existante d'atomjump.com). Une fois que c'est le cas, tout le monde peut voir le bon serveur et vous pouvez inviter d'autres personnes en toute sécurité sur le forum. Sinon, il est possible que certains utilisateurs écrivent sur le forum de messages, mais ces messages seront envoyés au mauvais serveur et ne seront pas visibles par les autres utilisateurs.

## Pour créer une salle privée

Sur AtomJump․com, vous pouvez saisir votre propre nom de salle unique là où il est écrit <span class="notranslate">« Entrez un nom de groupe »</span>, par exemple. <span class="notranslate">« fredfamily »</span> et appuyez sur l'entrée du bas <span class="notranslate">‘Create a PRIVATE room’ (« Créer une salle PRIVÉE »)</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

Sur AtomJump․com, cela coûte 15 $ NZ/an (environ 10 $ US/an). Vous pouvez également vous inscrire directement sur <a href="http://atomjump.org/wp/introduction/">cette page</a>. Sur d'autres sites, vous devez contacter votre administrateur système pour ajouter un mot de passe de forum privé.

Remarque : le mot de passe du forum que vous décidez d'avoir est différent de votre mot de passe personnel et doit être celui que vous pouvez envoyer confortablement par e-mail à chaque membre de la salle.

## Pour recevoir des notifications par SMS
Notez que sur AtomJump.com, cela a été remplacé par l'application, qui est gratuite et fournit des fenêtres contextuelles de messages graphiques. Ces instructions peuvent toutefois s'appliquer à d'autres installations du serveur.

Cliquez sur « Paramètres » dans le coin inférieur gauche. Cliquez sur « Plus » et entrez votre numéro de téléphone international sans le signe plus « + » et sans espaces, par ex. « +64 3 384 5613 » doit être saisi sous la forme « 6433845613 ».

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

Il y aura probablement un coût de la part de votre fournisseur (par exemple 16c par message), veuillez donc vérifier les instructions de votre fournisseur.

## Pour recevoir des notifications d'application

Les notifications d'application, bien que possibles, ne sont pas très visibles sur la messagerie AtomJump. En général, les personnes derrière AtomJump préfèrent une vie tranquille, il est donc préférable de traiter vos notifications de messages davantage comme vous traitez vos e-mails : vous vérifiez les nouveaux messages lorsque vous souhaitez les vérifier, pas lorsque tout le monde veut que vous les vérifiiez. En d’autres termes, vous contrôlez votre espace et vous respectez le souhait général des autres de rester silencieux depuis votre téléphone.

Accédez à l'adresse Web du forum. Cliquez sur « Paramètres ».

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Il existe des liens vers « Recevoir des notifications contextuelles » pour Android et iOS. S'il s'agit d'un serveur privé, vous devrez copier l'adresse du serveur mentionnée. Cliquez sur le lien de l'application puis, si vous ne l'avez pas déjà fait, « installez » l'application en suivant les étapes du lien « Installer » sur votre appareil Android, iPhone ou de bureau. Cela créera une icône de page d'accueil qui pourra être revisitée.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

Dans l'application, appuyez sur <span class="notranslate">"Register on an AtomJump group or a Private Server" ("S'inscrire sur un groupe AtomJump ou un serveur privé")</span> etentrez le nom du groupe de votre page xyz.atomjump.com (pour xyz.atomjump.com, vous tapez « xyz »), ou collez l'URL mentionnée sur votre forum privé, sous « Paramètres ».

Vous devriez être invité à vous connecter et vous pouvez créer un compte ou accéder à un compte existant. Une fois cette opération terminée, le navigateur devrait indiquer que vous avez associé votre application de démarrage à votre adresse e-mail sur ce serveur, et l'application de démarrage elle-même devrait indiquer <span class="notranslate">‘Listening for Messages’ ('Écoute des messages')</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

Cela signifie que tous les messages personnels de ce compte apparaîtront lorsque l'application est ouverte et, sur certains appareils (par exemple, ordinateur de bureau/Android), un ping retentira si l'application s'exécute dans un onglet en arrière-plan. Cependant, sur de nombreux téléphones Android, le son ping ne sera pas entendu si le téléphone est passé en mode veille.

Appuyez sur cet onglet pour obtenir une version complète du message, puis appuyez sur <span class="notranslate">‘Open Forum’ ('Ouvrir le forum')</span> pour accéder au forum lui-même, ce qui vous permet de répondre.

Pour <b>arrêter les notifications de messages</b> à tout moment, vous pouvez vous désinscrire sur un serveur particulier. Les notifications de messages seront effectuées par e-mail lorsque vous serez déconnecté. Une nouvelle inscription relancera les notifications.

Vous pouvez vous abonner à n'importe quel nombre de forums. Voir <a href="#become-owner">'Pour devenir abonné au forum'</a> ci-dessus.


## Pour enregistrer un raccourci de forum
Si vous êtes sur votre téléphone, nous vous recommandons d'installer l'application de démarrage AtomJump Messaging comme raccourci sur le bureau.

* https://app.atomjump.com

Appuyez sur « S'inscrire sur un groupe AtomJump ou un serveur privé » et entrez le nom du groupe de votre page xyz.atomjump.com (pour xyz.atomjump.com vous taperez « xyz »), ou collez l'URL mentionnée sur votre forum privé. , sous « Paramètres ».


## Pour publier une photo
Cliquez sur l'icône « télécharger » dans le coin inférieur gauche. Sélectionnez une photo .jpg sur votre lecteur et cliquez sur « Télécharger ». Remarque : vous pouvez choisir plusieurs photos.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

Remarque : si vous utilisez un appareil mobile, par ex. Android, pour accéder à votre galerie plutôt qu'à « Appareil photo », sélectionnez « Documents » puis « Galerie ».

Vous pouvez choisir de voir un aperçu zoomable de la photo en appuyant sur les aperçus miniatures qui apparaissent. Appuyez à nouveau sur la photo pour fermer l'aperçu complet.

## Pour poster un autocollant **
Cliquez sur le visage souriant dans le coin inférieur gauche. Cliquez ensuite sur votre autocollant.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## Pour télécharger des messages
Connectez-vous avec votre compte. Cliquez sur l'icône « Télécharger » dans le coin inférieur gauche. Cliquez sur le bouton « Télécharger ». Il téléchargera un fichier de feuille de calcul .xlsx, qui peut être ouvert par n'importe quel logiciel de feuille de calcul, par ex. Ouvrez Office, Google Docs, Microsoft Office.

## Pour changer de langue **
Cliquez sur « Paramètres » dans le coin inférieur gauche. Cliquez ensuite sur « Plus » et déroulez la case « Langue ». Choisissez votre langue. Cliquez ensuite sur « Enregistrer les paramètres ». Vous devrez peut-être saisir à nouveau votre mot de passe si vous êtes connecté.

Remarque : la zone grise au recto avec l'icône « Paramètres » ne changera de langue que si le site environnant a implémenté cette fonctionnalité, et vous devrez peut-être actualiser la page dans votre navigateur. Changer de langue modifiera les messages « guide » dans la boîte centrale. Cela n'affecte pas le texte saisi par les autres utilisateurs.

## Pour passer en plein écran **

Sur votre **téléphone Android**, lorsque vous êtes sur une page de messagerie AtomJump fréquentée, allez dans les « paramètres » du navigateur (souvent une icône à 3 barres près de l'adresse en haut) et sélectionnez l'option « Ajouter à l'écran d'accueil ». , ou quelque chose de similaire.

Sur votre **iPhone**, lorsque vous êtes sur une page de messagerie AtomJump fréquentée, appuyez sur l'icône « Partager » en bas de la page, puis sur l'icône « Ajouter à l'écran d'accueil » en bas, ou quelque chose de similaire.

Revenez ensuite à l'écran d'accueil de votre téléphone et appuyez sur l'icône. Cela utilisera ensuite l’espace plein écran pendant la messagerie.

## Pour supprimer des messages

N'importe qui peut supprimer n'importe quel message sur AtomJump.com, et nous autorisons cela afin que les forums soient en grande partie auto-modérés. Même si cela peut signifier que votre message est injustement supprimé par quelqu'un, cela signifie que seuls les messages approuvés par tout le monde resteront sur le forum. Si vous rencontrez des problèmes qui ne peuvent pas être résolus avec cette politique, veuillez nous contacter sur la page Confidentialité.

Pour supprimer un message, appuyez ou cliquez sur le message lui-même, et le message s'affichera avec une icône de poubelle en dessous. Appuyez sur l'icône de la poubelle et le message devrait être supprimé en quelques secondes.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

Remarque : Une fois qu'un message a été publié, vous disposez de 20 minutes (pour une installation par défaut d'AtomJump, et cela inclut AtomJump.com) pour supprimer le message avant que d'autres flux de robots aient la possibilité de copier le message.

## Pour arrêter les images scintillantes sur Android

Sur un téléphone Android, si vous vous connectez à un serveur de messagerie « http » non sécurisé et que le « Mode simplifié » du navigateur de votre téléphone est activé, vous constaterez probablement que jeles mages semblent scintiller toutes les 5 secondes environ. Nous vous recommandons de désactiver le « Mode simplifié » pour arrêter le scintillement sur la page Paramètres du navigateur Chrome de votre téléphone.

Alternativement, si un domaine « https » est associé au serveur, le problème devrait disparaître.
 
 

__** Cette fonctionnalité est activée par défaut sur AtomJump.com, mais elle peut ne pas l'être si votre fournisseur n'a pas inclus ces plugins.__
