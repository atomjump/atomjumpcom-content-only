<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom-content-only
Este repositório é uma cópia pública do conteúdo importante em https://atomjump.com,
caso os servidores estejam inativos.




# Guia do usuário de mensagens


## Introdução
AtomJump Messaging é um 'aplicativo Web progressivo' de várias camadas, para mensagens ao vivo e comunicação em fóruns. Basicamente, está uma interface de mensagens que opera um único fórum, e este guia ensinará como operar essa interface. Você pode encontrar os fóruns do AtomJump clicando em links específicos em um site, que abrirá o fórum em uma caixa pop-up no seu navegador. Qualquer site pode operar esses fóruns, e AtomJump.com é um desses sites.

Existe um aplicativo inicial opcional (que é executado em um navegador após tocar no ícone da página inicial), que permite coletar seus fóruns favoritos em um só lugar e verificar se há novas mensagens de qualquer fórum do AtomJump Messaging na Internet. Todos esses componentes são de código aberto e gratuitos. AtomJump Messaging é um 'aplicativo mais seguro' registrado.

* https://atomjump.com/wp/safer-apps/

Por ser um aplicativo da Web progressivo, há certas vantagens em relação aos aplicativos de mensagens da loja de aplicativos de primeira geração:

* Você pode discutir algo em seu telefone e mudar para uma versão desktop da mesma conversa, imediatamente, potencialmente sem fazer login.
* Você pode convidar um grupo de pessoas para um fórum, que ainda não tenham feito login no AtomJump, simplesmente enviando um link da web. Eles não precisam criar uma conta.
* Para que uma nova pessoa participe da conversa, não é necessário fazer o download complicado de um aplicativo grande, com muitas solicitações de segurança no telefone.
* Cada fórum diferente pode ser operado por uma organização diferente, e a funcionalidade desse fórum é escolhida pelo operador, sendo reunida a partir de blocos de construção básicos (chamados ‘plugins’).

Para começar, você pode tentar um fórum, por exemplo. https://atomjump.com

Para obter o aplicativo inicial opcional AtomJump Messaging (iPhone/Android/Desktop):

* https://app.atomjump.com

## Publique uma mensagem
Se você estiver no AtomJump.com, clique no grande botão azul ‘chat’ para entrar em um fórum. Outros sites que usam o software AtomJump mostrarão o pop-up de mensagens depois que você clicar em um link específico.

Comece a digitar na caixa ‘Digite seu comentário’ na parte superior e clique em ‘enviar’. É isso! Você não precisa de uma conta, mas será chamado de ‘Anon XX’ se não tiver seu nome.


## Para redigir uma mensagem
Você pode inserir alguns atalhos de mensagens comuns para vários tipos de mídia.

__``Enter``__ Cria

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``lol`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭`` 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``Por favor, pague 5 dólares para mim`` <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1b XAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY2 9kZSUzZFVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N 0ZWRHdWVzdCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">Por favor, pague 5 dólares para mim< /a> [com link para Paypal]

``Por favor, pague 3,30 libras para mim`` <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbW p1bXAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW 5jeV9jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYl MjUzYU5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">Por favor, pague 3,30 libras para mim< /a> [com link para Paypal]

``http://yoururl.com/yourvideo.mp4`` [mostra um ícone de vídeo cinza, para ser clicado]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [mostra uma imagem do vídeo, para ser clicado]

``london@`` <a href="http://london.atomjump.com">london@</a> [com link para http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [com link para http://london.atomjump.com]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text.com`` com">Expandir</a> [com link para http://a.very.long.link.with.lots.of.text.com]

`` http://seurl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [mostra a imagem]

``http://yoururl.com/youraudio.mp3`` [mostra um ícone de orelha cinza para ser clicado]

``http://yoururl.com/yourdoc.pdf`` [mostra um ícone de PDF para ser clicado]


** Este recurso está ativado por padrão em AtomJump.com, mas pode não estar se o seu provedor não tiver incluído esses plug-ins.

## Para videoconferência

Quando você estiver em um fórum, clique no ícone da câmera próximo ao botão ‘Enviar’. Isso fornecerá uma tela de confirmação antes de entrar no fórum de vídeo. Alguns navegadores precisam que a câmera seja aceita primeiro nas permissões do navegador.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

Os usuários de telefone podem preferir baixar um aplicativo separado.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

Para conhecer alguém, é melhor enviar a outras pessoas uma URL do fórum AtomJump (em AtomJump.com você pode clicar no ícone de compartilhamento) e dizer-lhes para clicarem na câmera. Você encontrará mais instruções em Jitsi. https://jitsi.org/user-faq/

Nota: Esses fóruns de vídeo são públicos, a menos que você esteja em um fórum privado, e embora você deva aceitar novos usuários antes que eles entrem, depois disso você deve ser cauteloso com o que mostra às outras pessoas. Se quiser privacidade aqui, você deve iniciar uma sessão única ou adquirir um fórum privado.

Método alternativo: embora você deva conseguir clicar no fórum de vídeo na maioria das plataformas, algumas plataformas, por exemplo. particularmente iPads/Internet Explorer, você pode alternativamente executar o desktop/aplicativo diretamente e copiar o endereço do fórum nele. Por exemplo. Para páginas AtomJump.com, o endereço do fórum será 'https://jitsi0.atomjump.com/[um código exclusivo]ajps' seguido pelo subdomínio. Você também pode encontrar isso olhando o endereço do ícone roxo de “link”.


## Para se nomear
Clique em ‘Configurações’ no canto inferior esquerdo. Escreva seu nome em ‘Seu nome’ e clique em ‘Salvar configurações’. Você não precisa de uma conta completa, mas se quiser receber notificações ao receber uma mensagem privada, você precisará de uma (veja abaixo).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/seu-nome.png">


## Para configurar uma conta

Configurar uma conta significa que você pode receber mensagens privadas. Clique em ‘Configurações’ no canto inferior esquerdo. Escreva seu nome em ‘Seu Nome’ e seu endereço de e-mail em ‘Seu e-mail’.

Opcionalmente, você pode adicionar uma senha à conta (o que impede que outra pessoa possa ler suas mensagens privadas). Se você quiser adicionar isso, e é altamente recomendado, clique em ‘Mais’ e digite sua senha.

Clique em ‘Salvar configurações’.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

Você receberá notificações por e-mail em mensagens privadas, mas não utilizamos seu endereço de e-mail para nenhuma outra finalidade.

## Para fazer login em um novo PC ou telefone
Use o mesmo e-mail e senha que você digitou no seu primeiro dispositivo ao configurar a conta.

Lembre-se, se você estiver em um PC público, você deve sair sempre que sair do PC, pois seus dados de login serão lembrados.

Nota: cada instalação diferente do AtomJump Server possui seu próprio banco de dados de usuários privados. Se sua conta foi configurada no AtomJump.com, então a conta pode ser compartilhada entre outras propriedades do AtomJump.com, mas não funcionará necessariamente em todas as versões do pop-up de mensagens que você encontrar, porque elas são administradas por outras empresas .

## Para escrever uma mensagem privada para uma pessoa
Clique ou toque no nome da pessoa ao lado de uma de suas mensagens. Em seguida, digite uma mensagem e toque em ‘Enviar para [nome]’.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

Você precisará clicar em ‘Tornar público’ para começar a postar mensagens para que todos possam ver novamente.

## Para escrever uma mensagem privada para os assinantes do fórum
Clique em ‘Ir para privado’. Insira mensagens normalmente.

Quando estiver pronto para voltar a postar no fórum público, clique em ‘Tornar público’ novamente.

## Para responder a um usuário do Twitter
Clique ou toque no nome da pessoa ao lado de uma de suas mensagens. Em seguida, digite uma mensagem e toque em ‘Enviar’.

Observe que uma resposta no Twitter sempre será pública e você não precisa de uma conta no Twitter. Uma mensagem será enviada automaticamente de nossa conta AtomJump informando que há uma mensagem esperando por eles no fórum em que você está. Cabe a eles decidir se irão ler suas mensagens no Twitter e responder.

## Para se tornar um assinante do fórum

Se você <b><i>não tem uma conta</i></b> e deseja apenas receber notificações por e-mail (sem precisar fazer login), você pode se inscrever tocando e mudando a orelha para verde 'ouvido atento' na janela de mensagens, que solicitará seu endereço de e-mail. Em seguida, clique em ‘Inscrever-se’.

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>Não inscrito. Toque para se inscrever.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>Inscrever-secama. Toque para cancelar a inscrição.</i>

Se você <b><i>tem uma conta</i></b>, você deve fazer login primeiro. Para fazer isso, clique em ‘Configurações’, clique em ‘Mais’ na caixa de senha e digite seu e-mail e senha. Em seguida, clique em ‘Login’.

Agora, na página de mensagens, toque e mude o ouvido para o ‘ouvido ouvinte’ verde para se inscrever naquele fórum.

Para <b>cancelar a inscrição</b> em um fórum inscrito, toque em e mude para o “ouvido que não escuta” vermelho na página de mensagens.

## Para configurar um grupo

Digite o nome do seu próprio grupo ou um nome de grupo público existente, onde diz “Digite um nome de grupo” em AtomJump.com e clique em <span class="notranslate">“AtomJump”</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

por exemplo. Digitar <span class="notranslate">“sailinglondon”</span> e tocar em <span class="notranslate">“AtomJump”</span> levará seu navegador ao seguinte endereço:

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

Compartilhe o link do seu fórum com seu grupo por e-mail, SMS, mensagem instantânea ou algum outro meio. Você pode encontrar esse link rapidamente em atomjump.com tocando no ícone ‘compartilhar’ no canto superior direito da página subjacente.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

Cada indivíduo pode então optar por se tornar um assinante do fórum para receber notificações contínuas.

<b>Dica avançada:</b> você pode opcionalmente adicionar <span class="notranslate">“/go/”</span> ao final do seu link da web, o que abrirá imediatamente o fórum ao vivo para seus convidados (para que eles não precisem tocar no grande botão azul 'ir').

<b>Endereços temporários</b>

Se você foi enviado para um endereço temporário (por exemplo, http://sailinglondon.clst4.atomjump.com) e foi informado que levaria de 2 a 48 horas para ficar totalmente ativo, você pode verificar o status da transferência do domínio em https: //dnschecker.org. Insira o endereço pretendido da sua página, por exemplo. "sailinglondon.atomjump.com" e pesquise. O que você precisa confirmar é que todos os endereços retornados são diferentes de "114.23.93.129" (que é o endereço residencial existente do atomjump.com). Quando for esse o caso, todos poderão ver o servidor correto e você estará seguro para convidar outras pessoas para o fórum. Caso contrário, existe a possibilidade de alguns usuários escreverem no fórum de mensagens, mas essas mensagens serão enviadas para o servidor errado e não ficarão visíveis para outros usuários.

## Para configurar uma sala privada

No AtomJump․com, você pode inserir o nome exclusivo da sua sala onde diz <span class="notranslate">“Digite um nome de grupo”</span>, por exemplo. <span class="notranslate">“fredfamily”</span> e toque na entrada inferior <span class="notranslate">‘Create a PRIVATE room’ (‘Criar uma sala PRIVADA’)</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

No AtomJump․com, isso custa NZ$ 15/ano (aproximadamente US$ 10/ano). Você também pode se inscrever diretamente <a href="http://atomjump.org/wp/introduction/">nesta página</a>. Em outros sites, você deve entrar em contato com o administrador do sistema para adicionar uma senha privada ao fórum.

Observação: a senha do fórum que você decidir ter é diferente da sua senha pessoal e deve ser uma que você possa enviar por e-mail para todos os membros da sala confortavelmente.

## Para receber notificações por SMS
Observe que no AtomJump.com isso foi substituído pelo aplicativo, que é gratuito e fornece pop-ups de mensagens gráficas. No entanto, estas instruções ainda podem ser aplicadas a outras instalações do servidor.

Clique em ‘Configurações’ no canto inferior esquerdo. Clique em ‘Mais’ e insira seu número de telefone internacional sem o sinal de adição ‘+’ e sem espaços, por exemplo. ‘+64 3 384 5613’ deve ser inserido como ‘6433845613’.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

Provavelmente haverá um custo do seu provedor (por exemplo, 16c por mensagem), portanto, verifique as instruções do seu provedor.

## Para receber notificações de aplicativos

As notificações de aplicativos, embora possíveis, não são muito perceptíveis no AtomJump Messaging. Geralmente, as pessoas por trás do AtomJump preferem uma vida tranquila, então é melhor tratar suas notificações de mensagens mais como trata o e-mail: você verifica novas mensagens quando deseja verificá-las, não quando todos querem que você as verifique. Em outras palavras, você está no controle do seu espaço e respeita o desejo geral das outras pessoas de silêncio no seu telefone.

Vá para o endereço web do fórum. Clique em ‘Configurações’.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Existem alguns links para ‘Obter notificações pop-up’ para Android e iOS. Se estiver em um servidor privado, você precisará copiar o endereço do servidor mencionado. Clique no link do aplicativo e, se ainda não tiver feito isso, “instale” o aplicativo seguindo as etapas do link ‘Instalar’ em seu Android, iPhone ou dispositivo desktop. Isso criará um ícone da página inicial que pode ser revisitado.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

Dentro do aplicativo, toque em <span class="notranslate">"Register on an AtomJump group or a Private Server" ("Registrar em um grupo AtomJump ou em um servidor privado")</span> einsira o nome do grupo da sua página xyz.atomjump.com (para xyz.atomjump.com você digitaria ‘xyz’) ou cole o URL mencionado em seu fórum privado, em ‘Configurações’.

Você será solicitado a fazer login e poderá criar uma conta ou inserir uma conta existente. Se isso for concluído, o navegador deverá mostrar que você associou seu aplicativo inicial ao seu endereço de e-mail nesse servidor, e o próprio aplicativo inicial deverá dizer <span class="notranslate">‘Listening for Messages’ ('Ouvindo mensagens')</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

Isso significa que todas as mensagens pessoais desta conta aparecerão quando o aplicativo for aberto e, em alguns dispositivos (por exemplo, desktop/Android), emitirão um som de ping se o aplicativo estiver sendo executado em uma guia em segundo plano. No entanto, em muitos telefones Android, o som do ping não será ouvido se o telefone entrar no modo de suspensão.

Toque nesta guia para obter uma versão completa da mensagem e, em seguida, toque em <span class="notranslate">‘Open Forum’ ('Abrir fórum')</span> para entrar no próprio fórum, que permite responder.

Para <b>interromper as notificações de mensagens</b> a qualquer momento, você pode 'Cancelar o registro' em um servidor específico. As notificações de mensagens serão feitas por e-mail enquanto você estiver desconectado. Registrar-se novamente iniciará as notificações novamente.

Você pode se inscrever em qualquer número de fóruns. Consulte <a href="#become-owner">'Para se tornar um assinante do fórum'</a> acima.


## Para salvar um atalho do fórum
Se você estiver no telefone, recomendamos instalar o aplicativo inicial AtomJump Messaging como um atalho na área de trabalho.

* https://app.atomjump.com

Toque em "Registrar-se em um grupo AtomJump ou em um servidor privado" e insira o nome do grupo da sua página xyz.atomjump.com (para xyz.atomjump.com você digitaria 'xyz') ou cole o URL mencionado em seu fórum privado , em ‘Configurações’.


## Para postar uma foto
Clique no ícone ‘upload’ no canto inferior esquerdo. Selecione uma foto .jpg da sua unidade e clique em ‘Upload’. Obs: você pode escolher mais de uma foto.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

Nota: se você estiver usando um dispositivo móvel, por ex. Android, para obter acesso à sua Galeria em vez da ‘Câmera’, selecione ‘Documentos’ e depois ‘Galeria’.

Você pode optar por ver uma visualização da foto com zoom tocando nas visualizações em miniatura que aparecem. Toque na foto novamente para fechar a visualização completa.

## Para postar um adesivo **
Clique no rosto sorridente no canto inferior esquerdo. Em seguida, clique no seu adesivo.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## Para baixar mensagens
Faça login com sua conta. Clique no ícone ‘Upload’ no canto inferior esquerdo. Clique no botão ‘Baixar’. Ele fará o download de um arquivo de planilha .xlsx, que pode ser aberto por qualquer software de planilha, por exemplo. Escritório aberto, Google Docs, Microsoft Office.

## Para alterar o idioma **
Clique em ‘Configurações’ no canto inferior esquerdo. Em seguida, clique em ‘Mais’ e selecione a caixa ‘Idioma’. Escolha seu idioma. Em seguida, clique em ‘Salvar configurações’. Pode ser necessário inserir sua senha novamente se estiver conectado.

Observação: a caixa cinza frontal com o ícone 'Configurações' só mudará o idioma se o site vizinho tiver implementado esse recurso e talvez seja necessário atualizar a página no seu navegador. A alteração do idioma alterará as mensagens de ‘guia’ na caixa central. Isso não afeta o texto inserido por outros usuários.

## Para ir para tela inteira **

No seu **telefone Android**, quando você estiver em uma página de mensagens frequentada do AtomJump, acesse as 'configurações' do navegador (geralmente um ícone de 3 barras próximo ao endereço na parte superior) e selecione a opção 'Adicionar à tela inicial' , ou algo semelhante.

No seu **iPhone**, quando você estiver em uma página de mensagens frequentada do AtomJump, toque no ícone "compartilhar" na parte inferior da página e, em seguida, no ícone "Adicionar à tela inicial" na parte inferior, ou algo semelhante.

Em seguida, volte para a tela inicial do seu telefone e toque no ícone. Isso usará o espaço da tela inteira durante as mensagens.

## Para excluir mensagens

Qualquer pessoa pode excluir qualquer mensagem no AtomJump.com, e nós permitimos isso para que os fóruns sejam em grande parte automoderados. Embora possa significar que sua mensagem foi excluída injustamente por alguém, significa que apenas as mensagens que todos aprovam permanecerão no fórum. Se você tiver problemas que não possam ser resolvidos com esta política, entre em contato conosco na página Privacidade.

Para excluir uma mensagem, toque ou clique na própria mensagem e você verá a mensagem com um ícone de lixeira abaixo. Toque no ícone da lixeira e a mensagem deverá ser removida em alguns segundos.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

Nota: Depois que uma mensagem for postada, você terá 20 minutos (para uma instalação padrão do AtomJump, e isso inclui AtomJump.com) para excluir a mensagem antes que outros feeds do robô tenham a chance de copiar a mensagem.

## Para parar de tremer imagens no Android

Em um telefone Android, se você estiver se conectando a um servidor de mensagens ‘http’ não seguro e o ‘Modo Lite’ da configuração do navegador do telefone estiver ativado, você provavelmente encontrará ios magos parecem piscar a cada 5 segundos ou mais. Recomendamos desligar o ‘Modo Lite’ para interromper a oscilação na página de configurações do navegador Chrome do seu telefone.

Alternativamente, se um domínio ‘https’ estiver associado ao servidor, o problema deverá desaparecer.
 
 

__** Este recurso está ativado por padrão em AtomJump.com, mas pode não estar se o seu provedor não tiver incluído esses plug-ins.__
