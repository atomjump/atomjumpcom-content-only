<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom コンテンツのみ
このリポジトリは、https://atomjump.com 上の重要なコンテンツの公開コピーです。
サーバーがダウンした場合に備えて。




# メッセージング ユーザー ガイド


＃＃ 導入
AtomJump メッセージングは、ライブ メッセージングとフォーラム コミュニケーションのための多層の「プログレッシブ Web アプリ」です。 その中心となるのは、単一のフォーラムを操作するメッセージング インターフェイスであり、このガイドではこのインターフェイスの操作方法を説明します。 Web サイト上の特定のリンクをクリックすると、ブラウザ内のポップアップ ボックスにフォーラムが開き、AtomJump フォーラムを見つけることができます。 どの Web サイトでもこれらのフォーラムを運営できます。AtomJump.com もその Web サイトの 1 つです。

オプションのスターター アプリ (ホームページ アイコンをタップした後にブラウザで実行されます) があり、これを使用すると、お気に入りのフォーラムを 1 か所に収集し、インターネット上の AtomJump メッセージング フォーラムからの新しいメッセージを確認できます。 これらのコンポーネントはすべてオープンソースで無料です。 AtomJump メッセージングは、「より安全なアプリ」として登録されています。

* https://atomjump.com/wp/safer-apps/

Progressive Web App であるため、第 1 世代のアプリストア メッセージング アプリに比べて次のような利点があります。

* 携帯電話で何かについて話し合っているときに、同じ会話のデスクトップ バージョンにすぐに切り替えることができ、場合によってはログインすることさえありません。
* Web リンクを送信するだけで、これまで AtomJump にサインインしたことがない人のグループをフォーラムに招待できます。 アカウントを作成する必要はありません。
* 新しい人が会話に参加する場合、電話上で大量のセキュリティ要求が発生するような、大規模なアプリをダウンロードするのが面倒なことはありません。
* それぞれの異なるフォーラムは異なる組織によって運営されており、そのフォーラムの機能は運営者によって選択され、基本的な構成要素 (「プラグイン」と呼ばれる) から組み立てられます。

まずはフォーラムを試してみてください。 https://atomjump.com

オプションの AtomJump メッセージング スターター アプリを入手するには (iPhone/Android/デスクトップ):

* https://app.atomjump.com

＃＃ メッセージを投稿します
AtomJump.com を使用している場合は、大きな青い「チャット」ボタンをクリックしてフォーラムに入ります。 AtomJump ソフトウェアを使用している他のサイトでは、特定のリンクをクリックするとメッセージ ポップアップが表示されます。

上部にある「コメントを入力してください」ボックスに入力を開始し、「送信」をクリックします。 それでおしまい！ アカウントは必要ありませんが、名前を付けていない場合は「Anon XX」と呼ばれます。


## メッセージを作成するには
さまざまな種類のメディアに共通のメッセージング ショートカットを入力できます。

__``Enter''__ を作成します

「http://link.com」 http://link.com

「link.com」 <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

「笑」 <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

「😜😀😊😇⚽️🎎🦊💭」😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

「5 ドル払ってください」 <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bXAlMmVj b20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY29kZSUzZF VTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0ZWRHDWVz dCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">5 ドル払ってください< /a> [Paypalへのリンク付き]

「3.30 ポンドをお支払いください。」 <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bXAl MmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW5jeV9j b2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYlMjUzYU 5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">3.30 ポンド払ってください< /a> [Paypalへのリンク付き]

「http://yoururl.com/yourvideo.mp4」 [クリックする灰色のビデオ アイコンが表示されます]
「https://atomjumpurl/api/images/im/upl8-32601688.mp4」 [クリックするとビデオの画像が表示されます]

``london@`` <a href="http://london.atomjump.com">london@</a> [http://london.atomjump.com へのリンク付き]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [http://london.atomjump.com へのリンク付き]

「http://a.very.long.link.with.lots.of.text.com」 <a href="http://a.very.long.link.with.lots.of.text. com">展開</a> [http://a.very.long.link.with.lots.of.text.com へのリンク]

「http://youru」rl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [画像を表示]

「http://yoururl.com/youraudio.mp3」 [クリックするための灰色の耳アイコンを表示]

「http://yoururl.com/yourdoc.pdf」 [クリックする PDF アイコンを表示]


** この機能は AtomJump.com ではデフォルトで有効になっていますが、プロバイダーにこれらのプラグインが含まれていない場合は無効になる可能性があります。

## ビデオ会議へ

フォーラムに参加している場合、[送信] ボタンの横にあるカメラのアイコンをクリックします。 これにより、ビデオ フォーラムに参加する前に確認画面が表示されます。 一部のブラウザでは、最初にブラウザの権限でカメラが受け入れられる必要があります。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

電話ユーザーは別のアプリをダウンロードすることを好む場合があります。

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

誰かに会うには、他の人に AtomJump フォーラムの URL (AtomJump.com では共有アイコンをクリックできます) を送り、カメラをクリックするように指示するのが最善です。 詳細については、Jitsi をご覧ください。 https://jitsi.org/user-faq/

注: これらのビデオ フォーラムは、プライベート フォーラムを使用していない限り公開されており、参加する前に新しいユーザーを受け入れる必要がありますが、参加後は他の人に表示する内容に注意する必要があります。 ここでプライバシーを確保したい場合は、1 回限りのセッションを開始するか、プライベート フォーラムを購入する必要があります。

代替方法: ほとんどのプラットフォームではクリックしてビデオ フォーラムにアクセスできるはずですが、一部のプラットフォームではビデオ フォーラムにアクセスできません。 特に iPad / Internet Explorer の場合は、デスクトップ / アプリを直接実行し、そこにフォーラムのアドレスをコピーすることもできます。 例えば。 AtomJump.com ページの場合、フォーラム アドレスは「https://jitsi0.atomjump.com/[一意のコード]ajps」の後にサブドメインが続きます。 紫色の「リンク」アイコンのアドレスを見てこれを見つけることもできます。


## 自分に名前を付けるには
左下隅にある「設定」をクリックします。 「あなたの名前」にあなたの名前を入力し、「設定を保存」をクリックします。 完全なアカウントは必要ありませんが、プライベート メッセージを受信したときに通知を受け取りたい場合は、アカウントが必要になります (下記を参照)。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/your-name.png">


## アカウントを設定するには

アカウントを設定すると、プライベート メッセージを受信できるようになります。 左下隅にある「設定」をクリックします。 「あなたの名前」にあなたの名前を、「あなたのメールアドレス」にあなたのメールアドレスを記入してください。

オプションで、アカウントにパスワードを追加できます (これにより、他の人があなたのプライベート メッセージを読むことができなくなります)。 これを追加したい場合は、[詳細] をクリックしてパスワードを入力してください。これを強くお勧めします。

「設定を保存」をクリックします。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

プライベート メッセージに関する電子メール通知が届きますが、当社がお客様の電子メール アドレスを他の目的に使用することはありません。

## 新しい PC または電話にログインするには
アカウントを設定したときに最初のデバイスで入力したのと同じ電子メールとパスワードを使用します。

公共の PC を使用している場合は、ログインの詳細が記憶されるため、PC から離れるときは必ずログアウトする必要があることに注意してください。

注: それぞれの異なる AtomJump Server インストールには、独自のプライベート ユーザー データベースがあります。 アカウントが AtomJump.com で設定されている場合、そのアカウントは他の AtomJump.com プロパティ間で共有される可能性がありますが、メッセージング ポップアップのすべてのバージョンで機能するとは限りません。これらは他の会社によって運営されているためです。 。

## 1 人にプライベート メッセージを書くには
メッセージの横にあるその人の名前をクリックまたはタップします。 次にメッセージを入力し、[[名前] に送信] をタップします。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

再び誰もが見ることができるメッセージの投稿を開始するには、[公開する] をクリックする必要があります。

## フォーラムの購読者にプライベート メッセージを書き込むには
「プライベートにする」をクリックします。 通常どおりメッセージを入力します。

公開フォーラムへの投稿に戻る準備ができたら、もう一度 [公開する] をクリックします。

## Twitter ユーザーに返信するには
メッセージの横にあるその人の名前をクリックまたはタップします。 次にメッセージを入力して「送信」をタップします。

Twitter の返信は常に公開されるため、Twitter アカウントは必要ないことに注意してください。 AtomJump アカウントから、現在参加しているフォーラムでメッセージを待っていることを知らせるメッセージが自動的に送信されます。 Twitter メッセージを読んで応答するかどうかは、ユーザー次第です。

## フォーラムの購読者になるには

<b><i>アカウントを持っておらず</i></b>、（サインインせずに）電子メールによる通知のみを受け取りたい場合は、タップして耳を緑色に切り替えることで購読できます。 メッセージ ウィンドウで「耳を傾ける」をクリックすると、電子メール アドレスが求められます。 次に「購読」をクリックします。

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>購読していません。 タップして購読します。</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>購読ベッド。 タップして購読を解除します。</i>

<b><i>アカウントをお持ち</i></b>の場合は、まずログインする必要があります。 これを行うには、[設定] をクリックし、パスワード ボックスの [詳細] をクリックして、電子メールとパスワードを入力します。 次に「ログイン」をクリックします。

次に、メッセージ ページで、耳をタップして緑色の「リスニング耳」に切り替えて、そのフォーラムに登録します。

登録済みのフォーラムの<b>登録を解除</b>するには、メッセージ ページの赤い「非聴取耳」をタップして切り替えます。

## グループを設定するには

AtomJump.com の「グループ名を入力してください」と表示されている場所に独自のグループ名、または既存の公開グループ名を入力し、<span class="notranslate">「AtomJump」</span> をクリックします。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

例えば <span class="notranslate">「sailinglondon」</span> と入力し、<span class="notranslate">「AtomJump」</span> をタップすると、ブラウザが次のアドレスに移動します。

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

電子メール、SMS、インスタント メッセージ、またはその他の手段を介して、フォーラムの Web リンクをグループと共有します。 このリンクは、atomjump.com で、下にあるページの右上隅にある「共有」アイコンをタップするとすぐに見つかります。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

その後、各個人がフォーラムの購読者になることを選択して、継続的な通知を受け取ることができます。

<b>高度なヒント:</b> 必要に応じて、ウェブ リンクの末尾に <span class="notranslate">「/go/」</span> を追加すると、ゲスト向けにライブ フォーラムがすぐに開かれます。 (大きな青い「go」ボタンをタップする必要がないようにします)。

<b>一時的な住所</b>

一時的なアドレス (例: http://sailinglondon.clst4.atomjump.com) に送信され、完全にアクティブになるまで 2 ～ 48 時間かかると言われた場合は、https: でドメイン移管のステータスを確認できます。 //dnschecker.org. ページの目的のアドレスを入力します。例: 「sailinglondon.atomjump.com」で検索してください。 確認する必要があるのは、返されたすべてのアドレスが「114.23.93.129」(既存の atomjump.com ホーム アドレス) とは異なることです。 これに該当すると、誰もが正しいサーバーを参照できるようになり、安全に他の人をフォーラムに招待できるようになります。 そうしないと、一部のユーザーがメッセージ フォーラムに書き込みをする可能性がありますが、それらのメッセージは間違ったサーバーに送信され、他のユーザーには表示されなくなります。

## プライベートルームをセットアップするには

AtomJump․com では、<span class="notranslate">「グループ名を入力してください」</span> と表示されている場所に、独自の一意のルーム名を入力できます。 <span class="notranslate">「fredfamily」</span> を選択し、一番下のエントリ <span class="notranslate">‘Create a PRIVATE room’「プライベート ルームの作成」</span> をタップします。

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

AtomJump․com では、年間 NZ$15 (約 US10/年) の費用がかかります。 <a href="http://atomjump.org/wp/introduction/">このページ</a>から直接サインアップすることもできます。 他のサイトでは、システム管理者に連絡してプライベート フォーラムのパスワードを追加する必要があります。

注: 決定したフォーラムのパスワードは個人のパスワードとは異なり、ルームのすべてのメンバーに快適に電子メールで送信できるものである必要があります。

## SMS 通知を受け取るには
AtomJump.com では、これは、グラフィカル メッセージ ポップアップを提供する無料のアプリに置き換えられていることに注意してください。 ただし、これらの手順はサーバーの他のインストールにも適用される場合があります。

左下隅にある「設定」をクリックします。 「詳細」をクリックし、先頭のプラス記号「+」やスペースを含まない国際電話番号を入力します。 「+64 3 384 5613」は「6433845613」と入力する必要があります。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

プロバイダーからの料金 (例: メッセージあたり 16 セント) が発生する可能性がありますので、プロバイダーの指示を確認してください。

## アプリの通知を受け取るには

アプリ通知は可能ですが、AtomJump メッセージングではあまり目立ちません。 一般に、AtomJump の背後にいる人々は静かな生活を好むため、メッセージ通知を電子メールと同じように扱うのが最善です。つまり、他の人がチェックしてほしいと思っているときではなく、自分がチェックしたいときに新しいメッセージをチェックします。 言い換えれば、あなたは自分の空間をコントロールしており、電話から沈黙したいという他の人の一般的な願望を尊重します。

フォーラムの Web アドレスにアクセスします。 「設定」をクリックします。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Android および iOS 用の「ポップアップ通知を取得」へのリンクがいくつかあります。 これがプライベート サーバー上にある場合は、記載されているサーバー アドレスをコピーする必要があります。 アプリのリンクをクリックし、まだ行っていない場合は、「インストール」リンクの手順に従って Android、iPhone、またはデスクトップ デバイスにアプリを「インストール」します。 これにより、再度アクセスできるホームページ アイコンが作成されます。

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

アプリ内で、<span class="notranslate">"Register on an AtomJump group or a Private Server" [AtomJump グループまたはプライベート サーバーに登録]</span> をタップして、xyz.atomjump.com ページのグループ名を入力するか (xyz.atomjump.com の場合は「xyz」と入力します)、プライベート フォーラムに記載されている URL を [設定] に貼り付けます。

サインインを求められるので、アカウントを作成するか、既存のアカウントを入力します。 これが完了すると、スターター アプリがそのサーバー上の電子メール アドレスに関連付けられたことがブラウザに表示され、スターター アプリ自体が<span class="notranslate">‘Listening for Messages’「メッセージをリッスンしています」</span> と表示されるはずです。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

これは、アプリが開いているときにこのアカウントの個人メッセージがポップアップ表示され、一部のデバイス (デスクトップ/Android など) では、アプリがバックグラウンド タブで実行されている場合にピン音が鳴ることを意味します。 ただし、多くの Android スマートフォンでは、スマートフォンがスリープ モードになると、ping 音は聞こえません。

このタブをタップしてメッセージの完全版を表示し、<span class="notranslate">‘Open Forum’ [フォーラムを開く]</span> をタップしてフォーラム自体に入り、返信できるようになります。

特定のサーバーの「登録を解除」 することで、 いつでも<b>メッセージ通知を停止</b>できます。 メッセージ通知は、ログアウト中に電子メールで行われます。 再度登録すると通知が再開されます。

フォーラムはいくつでも購読できます。 上記の<a href="#become-owner">「フォーラムの購読者になるには」</a> をご覧ください。


## フォーラムのショートカットを保存するには
携帯電話を使用している場合は、AtomJump Messaging スターター アプリをデスクトップ ショートカットとしてインストールすることをお勧めします。

* https://app.atomjump.com

[AtomJump グループまたはプライベート サーバーに登録] をタップし、xyz.atomjump.com ページのグループ名を入力するか (xyz.atomjump.com の場合は「xyz」と入力します)、プライベート フォーラムに記載されている URL を貼り付けます。 , [設定] の下にあります。


## 写真を投稿するには
左下隅にある「アップロード」アイコンをクリックします。 ドライブから .jpg 写真を選択し、「アップロード」をクリックします。 注: 複数の写真を選択できます。

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

注: モバイルデバイスを使用している場合、例: Android の場合、「カメラ」ではなくギャラリーにアクセスするには、「ドキュメント」を選択してから「ギャラリー」を選択します。

表示されるサムネイル プレビューをタップすると、写真のズーム可能なプレビューを表示するように選択できます。 写真をもう一度タップして、完全なプレビューを閉じます。

## ステッカーを投稿するには **
左下隅にあるスマイリーフェイスをクリックします。 次に、ステッカーをクリックします。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/Laugh1-150x150.jpg">

## メッセージをダウンロードするには
自分のアカウントでログインします。 左下隅にある「アップロード」アイコンをクリックします。 「ダウンロード」ボタンをクリックします。 .xlsx スプレッドシート ファイルがダウンロードされます。このファイルは、任意のスプレッドシート ソフトウェアで開くことができます。 Office、Google ドキュメント、Microsoft Office を開きます。

## 言語を変更するには **
左下隅にある「設定」をクリックします。 次に、「詳細」をクリックし、「言語」ボックスをドロップダウンします。 言語を選んでください。 次に「設定を保存」をクリックします。 ログインしている場合は、パスワードを再度入力する必要がある場合があります。

注: 「設定」アイコンが付いた前面の灰色のボックスは、周囲のサイトがこの機能を実装している場合にのみ言語を変更するため、ブラウザでページを更新する必要がある場合があります。 言語を変更すると、中央ボックスの「ガイド」メッセージが変更されます。 他のユーザーが入力したテキストには影響しません。

## 全画面表示にするには **

**Android スマートフォン**で、頻繁に使用する AtomJump メッセージング ページを表示しているときに、ブラウザの「設定」(多くの場合、上部のアドレス近くにある 3 本の棒のアイコン) に移動し、「ホーム画面に追加」オプションを選択します。 、または同様のもの。

**iPhone** で、頻繁に使用する AtomJump メッセージング ページを表示しているときに、ページの下部にある [共有] アイコンをタップし、次に下部にある [ホーム画面に追加] アイコンなどをタップします。

次に、携帯電話のホーム画面に戻り、アイコンをタップします。 これにより、メッセージング中に全画面領域が使用されます。

## メッセージを削除するには

AtomJump.com 上のメッセージは誰でも削除できます。フォーラムがほぼ自己管理されるように、これを許可しています。 これは、あなたのメッセージが誰かによって不当に削除されたことを意味するかもしれませんが、全員が承認したメッセージだけがフォーラムに残ることを意味します。 このポリシーで解決できない問題がある場合は、プライバシー ページからお問い合わせください。

メッセージを削除するには、メッセージ自体をタップまたはクリックすると、下にゴミ箱アイコンが付いたメッセージが表示されます。 ゴミ箱アイコンをタップすると、メッセージは数秒以内に削除されます。

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

注: メッセージが投稿された後、他のロボット フィードがメッセージをコピーする前に、20 分以内にメッセージを削除することができます (AtomJump のデフォルト インストールの場合、これには AtomJump.com も含まれます)。

## Android で画像のちらつきを止めるには

Android 携帯電話で、安全でない「http」メッセージング サーバーに接続しており、携帯電話のブラウザ設定の「ライト モード」がオンになっている場合、次のようなメッセージが表示される可能性があります。魔術師は 5 秒ごとにちらつくように見えます。 携帯電話の Chrome ブラウザ設定ページのちらつきを防ぐには、「ライト モード」をオフにすることをお勧めします。

あるいは、「https」ドメインがサーバーに関連付けられている場合、問題は解消されるはずです。
 
 

__** この機能は AtomJump.com ではデフォルトで有効になっていますが、プロバイダーにこれらのプラグインが含まれていない場合は無効になる可能性があります。__
