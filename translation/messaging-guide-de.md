<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom-content-only
Dieses Repository ist eine öffentliche Kopie der wichtigen Inhalte auf https://atomjump.com.
für den Fall, dass die Server ausfallen sollten.




# Messaging-Benutzerhandbuch


## Einführung
AtomJump Messaging ist eine mehrschichtige „Progressive Web App“ für Live-Messaging und Forum-Kommunikation. Im Kern handelt es sich um eine Messaging-Schnittstelle, die ein einzelnes Forum betreibt. In diesem Handbuch erfahren Sie, wie Sie diese Schnittstelle bedienen. Sie können AtomJump-Foren finden, indem Sie auf bestimmte Links auf einer Website klicken. Dadurch wird das Forum in einem Popup-Fenster in Ihrem Browser geöffnet. Jede Website kann diese Foren betreiben, und AtomJump.com ist eine dieser Websites.

Es gibt eine optionale Starter-App (die in einem Browser ausgeführt wird, nachdem Sie auf ein Homepage-Symbol getippt haben), mit der Sie Ihre Lieblingsforen an einem Ort sammeln und nach neuen Nachrichten von jedem AtomJump Messaging-Forum im Internet suchen können. Alle diese Komponenten sind Open Source und kostenlos. AtomJump Messaging ist eine registrierte „Safer App“.

* https://atomjump.com/wp/safer-apps/

Da es sich um eine progressive Web-App handelt, gibt es bestimmte Vorteile gegenüber Messaging-Apps aus App-Stores der ersten Generation:

* Sie können etwas auf Ihrem Telefon besprechen und sofort zu einer Desktop-Version derselben Konversation wechseln, möglicherweise ohne sich anzumelden.
* Sie können eine Gruppe von Personen zu einem Forum einladen, die sich noch nicht bei AtomJump angemeldet haben, indem Sie einfach einen Weblink senden. Sie müssen kein Konto erstellen.
* Damit eine neue Person an der Konversation teilnehmen kann, muss kein umständlicher Download einer großen App mit vielen Sicherheitsabfragen am Telefon erfolgen.
* Jedes einzelne Forum kann von einer anderen Organisation betrieben werden, und die Funktionalität dieses Forums wird vom Betreiber ausgewählt und aus Grundbausteinen (sogenannten „Plugins“) zusammengestellt.

Um zu beginnen, können Sie ein Forum auf z.B. https://atomjump.com

So erhalten Sie die optionale AtomJump Messaging-Starter-App (iPhone/Android/Desktop):

* https://app.atomjump.com

## Sende eine Nachricht
Wenn Sie auf AtomJump.com sind, klicken Sie auf die große blaue Schaltfläche „Chat“, um ein Forum zu betreten. Andere Websites, die die AtomJump-Software verwenden, zeigen das Nachrichten-Popup an, nachdem Sie auf einen bestimmten Link geklickt haben.

Beginnen Sie mit der Eingabe in das Feld „Kommentar eingeben“ oben und klicken Sie auf „Senden“. Das ist es! Sie benötigen kein Konto, allerdings werden Sie „Anon XX“ genannt, wenn Sie keinen Namen angegeben haben.


## So verfassen Sie eine Nachricht
Sie können einige gängige Nachrichtenverknüpfungen für verschiedene Medientypen eingeben.

__``Enter``__ Erstellt

„http://link.com“ http://link.com

„link.com“ <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

„lol“ <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

„😜😀😊😇⚽️🎎🦊💭“ 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

/a> [mit Link zu Paypal]

„Bitte zahlen Sie mir 3,30 Pfund.“ p1bXAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW5 jeV9jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYlMj UzYU5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">Bitte zahlen Sie 3,30 Pfund an mich< /a> [mit Link zu Paypal]

„http://yoururl.com/yourvideo.mp4“ [zeigt ein graues Videosymbol, das angeklickt werden kann]
„https://atomjumpurl/api/images/im/upl8-32601688.mp4“ [zeigt ein Bild des Videos, das angeklickt werden kann]

„london@“ <a href="http://london.atomjump.com">london@</a> [mit Link zu http://london.atomjump.com]

„london.atomjump.com“ <a href="http://london.atomjump.com">london@</a> [mit Link zu http://london.atomjump.com]

„http://a.very.long.link.with.lots.of.text.com“ <a href="http://a.very.long.link.with.lots.of.text. com">Erweitern</a> [mit Link zu http://a.very.long.link.with.lots.of.text.com]

„http://yoururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [zeigt Bild]

„http://yoururl.com/youaudio.mp3“ [zeigt ein graues Ohrsymbol, das angeklickt werden kann]

„http://yoururl.com/yourdoc.pdf“ [zeigt ein PDF-Symbol, das angeklickt werden kann]


** Diese Funktion ist bei AtomJump.com standardmäßig aktiviert. Dies ist jedoch möglicherweise nicht der Fall, wenn Ihr Anbieter diese Plugins nicht integriert hat.

## Zur Videokonferenz

Wenn Sie sich in einem Forum befinden, klicken Sie auf das Kamerasymbol neben der Schaltfläche „Senden“. Dadurch wird vor dem Betreten des Videoforums ein Bestätigungsbildschirm angezeigt. Bei einigen Browsern muss die Kamera zunächst in Ihren Browserberechtigungen akzeptiert werden.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

Telefonbenutzer ziehen es möglicherweise vor, eine separate App herunterzuladen.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

Um jemanden kennenzulernen, senden Sie den anderen am besten eine URL des AtomJump-Forums (auf AtomJump.com können Sie auf das Teilen-Symbol klicken) und fordern Sie sie auf, auf die Kamera zu klicken. Weitere Anleitungen finden Sie bei Jitsi. https://jitsi.org/user-faq/

Hinweis: Diese Videoforen sind öffentlich, es sei denn, Sie befinden sich in einem privaten Forum. Sie müssen zwar neue Benutzer akzeptieren, bevor sie beitreten, danach sollten Sie jedoch vorsichtig sein, was Sie anderen Personen zeigen. Wenn Sie hier Privatsphäre wünschen, sollten Sie eine einmalige Sitzung starten oder ein privates Forum erwerben.

Alternative Methode: Während Sie auf den meisten Plattformen in der Lage sein sollten, durch das Videoforum zu klicken, gibt es einige Plattformen, z. Insbesondere bei iPads/Internet Explorer können Sie alternativ den Desktop/die App direkt ausführen und die Forumsadresse hinein kopieren. Z.B. Für AtomJump.com-Seiten lautet die Forumsadresse „https://jitsi0.atomjump.com/[ein eindeutiger Code]ajps“, gefolgt von der Subdomain. Sie können dies auch finden, indem Sie sich die Adresse mit dem violetten „Link“-Symbol ansehen.


## Um sich selbst einen Namen zu geben
Klicken Sie unten links auf „Einstellungen“. Geben Sie Ihren Namen in „Ihr Name“ ein und klicken Sie auf „Einstellungen speichern“. Sie benötigen kein vollständiges Konto, aber wenn Sie Benachrichtigungen erhalten möchten, wenn Sie eine private Nachricht erhalten, benötigen Sie eines (siehe unten).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/your-name.png">


## So richten Sie ein Konto ein

Wenn Sie ein Konto einrichten, können Sie private Nachrichten empfangen. Klicken Sie unten links auf „Einstellungen“. Geben Sie Ihren Namen in „Ihr Name“ und Ihre E-Mail-Adresse in „Ihre E-Mail“ ein.

Optional können Sie dem Konto ein Passwort hinzufügen (das verhindert, dass jemand anderes Ihre privaten Nachrichten lesen kann). Wenn Sie dies hinzufügen möchten und dies dringend empfohlen wird, klicken Sie auf „Mehr“ und geben Sie Ihr Passwort ein.

Klicken Sie auf „Einstellungen speichern“.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

Sie erhalten E-Mail-Benachrichtigungen über private Nachrichten, wir verwenden Ihre E-Mail-Adresse jedoch nicht für andere Zwecke.

## So melden Sie sich an einem neuen PC oder Telefon an
Verwenden Sie dieselbe E-Mail-Adresse und dasselbe Passwort, die Sie bei der Einrichtung des Kontos auf Ihrem ersten Gerät eingegeben haben.

Denken Sie daran: Wenn Sie sich an einem öffentlichen PC befinden, sollten Sie sich jedes Mal abmelden, wenn Sie den PC verlassen, da Ihre Anmeldedaten gespeichert werden.

Hinweis: Jede einzelne AtomJump Server-Installation verfügt über eine eigene Datenbank für private Benutzer. Wenn Ihr Konto auf AtomJump.com eingerichtet wurde, kann das Konto von anderen AtomJump.com-Eigenschaften gemeinsam genutzt werden, es funktioniert jedoch nicht unbedingt in jeder Version des Messaging-Popups, die Sie möglicherweise finden, da diese von anderen Unternehmen betrieben werden .

## Um einer Person eine private Nachricht zu schreiben
Klicken oder tippen Sie auf den Namen der Person neben einer ihrer Nachrichten. Geben Sie dann eine Nachricht ein und tippen Sie auf „An [ihren Namen] senden“.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

Sie müssen auf „Öffentlich gehen“ klicken, um mit dem Posten von Nachrichten zu beginnen, damit jeder sie wieder sehen kann.

## So schreiben Sie eine private Nachricht an die Forum-Abonnenten:
Klicken Sie auf „Privat gehen“. Geben Sie Nachrichten wie gewohnt ein.

Sobald Sie bereit sind, wieder im öffentlichen Forum zu posten, klicken Sie erneut auf „Öffentlich gehen“.

## Um einem Twitter-Benutzer zu antworten
Klicken oder tippen Sie auf den Namen der Person neben einer ihrer Nachrichten. Geben Sie dann eine Nachricht ein und tippen Sie auf „Senden“.

Beachten Sie, dass eine Twitter-Antwort immer öffentlich ist und Sie kein Twitter-Konto benötigen. Von unserem AtomJump-Konto wird automatisch eine Nachricht gesendet, dass in dem Forum, in dem Sie sich gerade befinden, eine Nachricht auf sie wartet. Es liegt an ihnen, ob sie ihre Twitter-Nachrichten lesen und antworten.

## Um Forum-Abonnent zu werden

Wenn Sie <b><i>kein Konto haben</i></b> und nur E-Mail-Benachrichtigungen erhalten möchten (ohne sich anmelden zu müssen), können Sie sich anmelden, indem Sie auf das Ohr tippen und es auf das grüne Symbol stellen Klicken Sie im Nachrichtenfenster auf „Zuhören“, um nach Ihrer E-Mail-Adresse zu fragen. Klicken Sie dann auf „Abonnieren“.

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>Nicht abonniert. Zum Abonnieren tippen.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>AbonnierenBett. Tippen Sie hier, um sich abzumelden.</i>

Wenn Sie <b><i>ein Konto haben</i></b>, sollten Sie sich zuerst anmelden. Klicken Sie dazu auf „Einstellungen“, klicken Sie auf „Mehr“ für das Passwortfeld und geben Sie Ihre E-Mail-Adresse und Ihr Passwort ein. Klicken Sie dann auf „Anmelden“.

Tippen Sie nun auf der Nachrichtenseite auf und schalten Sie das Ohr auf das grüne „zuhörende Ohr“, um dieses eine Forum zu abonnieren.

Um sich von einem abonnierten Forum <b>abzumelden</b>, tippen Sie auf und wechseln Sie zum roten „nicht zuhörenden Ohr“ auf der Nachrichtenseite.

## So richten Sie eine Gruppe ein

Geben Sie Ihren eigenen Gruppennamen oder einen vorhandenen öffentlichen Gruppennamen mit der Aufschrift „Geben Sie einen Gruppennamen ein“ auf AtomJump.com ein und klicken Sie auf <span class="notranslate">„AtomJump“</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

z.B. Wenn Sie <span class="notranslate">„sailinglondon“</span> eingeben und auf <span class="notranslate">„AtomJump“</span> tippen, wird Ihr Browser zur folgenden Adresse weitergeleitet:

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

Teilen Sie den Weblink Ihres Forums per E-Mail, SMS, Sofortnachricht oder auf andere Weise mit Ihrer Gruppe. Sie können diesen Link schnell auf atomjump.com finden, indem Sie auf das „Teilen“-Symbol in der oberen rechten Ecke der zugrunde liegenden Seite tippen.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

Jeder Einzelne kann sich dann dafür entscheiden, Forum-Abonnent zu werden, um fortlaufende Benachrichtigungen zu erhalten.

<b>Tipp für Fortgeschrittene:</b> Sie können optional <span class="notranslate">“/go/“</span> am Ende Ihres Weblinks hinzufügen, wodurch das Live-Forum für Ihre Gäste sofort geöffnet wird (damit sie nicht auf den großen blauen „Los“-Button tippen müssen).

<b>Temporäre Adressen</b>

Wenn Sie an eine temporäre Adresse (z. B. http://sailinglondon.clst4.atomjump.com) weitergeleitet wurden und Ihnen mitgeteilt wurde, dass es 2 bis 48 Stunden dauern würde, bis sie vollständig aktiv ist, können Sie den Status der Domainübertragung unter https überprüfen: //dnschecker.org. Geben Sie die gewünschte Adresse Ihrer Seite ein, z. B. „sailinglondon.atomjump.com“ und suchen Sie. Sie müssen bestätigen, dass sich alle zurückgegebenen Adressen von „114.23.93.129“ (der vorhandenen Privatadresse von atomjump.com) unterscheiden. Sobald dies der Fall ist, kann jeder den richtigen Server sehen und Sie können andere Personen sicher in das Forum einladen. Wenn nicht, besteht die Möglichkeit, dass einige Benutzer in das Nachrichtenforum schreiben, diese Nachrichten jedoch an den falschen Server gesendet werden und für andere Benutzer nicht sichtbar sind.

## So richten Sie einen privaten Raum ein

Auf AtomJump․com können Sie Ihren eigenen eindeutigen Raumnamen eingeben, wo <span class="notranslate">„Geben Sie einen Gruppennamen ein“</span> steht, z. B. <span class="notranslate">„fredfamily“</span> und tippen Sie auf den unteren Eintrag <span class="notranslate">‘Create a PRIVATE room’ („Privaten Raum erstellen“)</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

Auf AtomJump․com kostet dies 15 NZ$/Jahr (ca. 10 US-Dollar/Jahr). Sie können sich auch direkt auf <a href="http://atomjump.org/wp/introduction/">dieser Seite</a> anmelden. Auf anderen Websites sollten Sie sich an Ihren Systemadministrator wenden, um ein privates Forum-Passwort hinzuzufügen.

Hinweis: Das Forum-Passwort, für das Sie sich entscheiden, unterscheidet sich von Ihrem persönlichen Passwort und sollte eines sein, das Sie bequem per E-Mail an jedes Mitglied im Raum senden können.

## Um SMS-Benachrichtigungen zu erhalten
Beachten Sie, dass dies auf AtomJump.com durch die App ersetzt wurde, die kostenlos ist und grafische Popups für Nachrichten bereitstellt. Diese Anweisungen gelten jedoch möglicherweise weiterhin für andere Installationen des Servers.

Klicken Sie unten links auf „Einstellungen“. Klicken Sie auf „Mehr“ und geben Sie Ihre internationale Telefonnummer ohne das führende Pluszeichen „+“ und ohne Leerzeichen ein, z. B. „+64 3 384 5613“ sollte als „6433845613“ eingegeben werden.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

Es werden wahrscheinlich Kosten von Ihrem Anbieter anfallen (z. B. 16 Cent pro Nachricht). Überprüfen Sie daher bitte die Anweisungen Ihres Anbieters.

## Um App-Benachrichtigungen zu erhalten

App-Benachrichtigungen sind zwar möglich, fallen bei AtomJump Messaging jedoch kaum auf. Im Allgemeinen bevorzugen die Leute hinter AtomJump ein ruhiges Leben, daher ist es am besten, Ihre Nachrichtenbenachrichtigungen eher so zu behandeln, wie Sie E-Mails behandeln: Sie suchen nach neuen Nachrichten, wenn Sie nach ihnen suchen möchten, und nicht, wenn alle anderen möchten, dass Sie nach ihnen suchen. Mit anderen Worten: Sie haben die Kontrolle über Ihren Raum und respektieren den allgemeinen Wunsch anderer Menschen nach Ruhe auf Ihrem Telefon.

Gehen Sie zur Webadresse des Forums. Klicken Sie auf „Einstellungen“.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Es gibt einige Links zu „Popup-Benachrichtigungen erhalten“ für Android und iOS. Wenn es sich um einen privaten Server handelt, müssen Sie die genannte Serveradresse kopieren. Klicken Sie auf den App-Link und dann, falls Sie dies noch nicht getan haben, „installieren“ Sie die App mit den Schritten im Link „Installieren“ auf Ihrem Android-Gerät, iPhone oder Desktop-Gerät. Dadurch wird ein Homepage-Symbol erstellt, das erneut aufgerufen werden kann.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

Tippen Sie in der App auf <span class="notranslate">"Register on an AtomJump group or a Private Server" ("Bei einer AtomJump-Gruppe oder einem privaten Server registrieren")</span> undGeben Sie den Gruppennamen Ihrer xyz.atomjump.com-Seite ein (für xyz.atomjump.com würden Sie „xyz“ eingeben) oder fügen Sie die in Ihrem privaten Forum angegebene URL unter „Einstellungen“ ein.

Sie sollten aufgefordert werden, sich anzumelden, und Sie können ein Konto erstellen oder ein bestehendes Konto eingeben. Wenn dies abgeschlossen ist, sollte der Browser anzeigen, dass Sie Ihre Starter-App mit Ihrer E-Mail-Adresse auf diesem Server verknüpft haben, und die Starter-App selbst sollte <span class="notranslate">„Listening for Messages“</span> anzeigen.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

Dies bedeutet, dass alle persönlichen Nachrichten für dieses Konto angezeigt werden, wenn die App geöffnet ist, und auf einigen Geräten (z. B. Desktop/Android) ein Ping-Ton erzeugt wird, wenn die App in einem Hintergrundtab ausgeführt wird. Bei vielen Android-Telefonen ist der Ping-Ton jedoch nicht zu hören, wenn das Telefon in den Ruhemodus wechselt.

Tippen Sie auf diese Registerkarte, um eine vollständige Version der Nachricht anzuzeigen, und tippen Sie dann auf <span class="notranslate">‘Open Forum’ („Forum öffnen“)</span>, um das Forum selbst aufzurufen, in dem Sie antworten können.

Um <b>Nachrichtenbenachrichtigungen zu stoppen</b>, können Sie sich jederzeit von einem bestimmten Server abmelden. Nachrichtenbenachrichtigungen werden per E-Mail verschickt, während Sie abgemeldet sind. Bei einer erneuten Registrierung werden die Benachrichtigungen erneut gestartet.

Sie können beliebig viele Foren abonnieren. Siehe <a href="#become-owner">„Forum-Abonnent werden“</a> oben.


## So speichern Sie eine Forum-Verknüpfung
Wenn Sie Ihr Telefon verwenden, empfehlen wir Ihnen, die AtomJump Messaging-Starter-App als Desktop-Verknüpfung zu installieren.

* https://app.atomjump.com

Tippen Sie auf „Auf einer AtomJump-Gruppe oder einem privaten Server registrieren“ und geben Sie den Gruppennamen Ihrer xyz.atomjump.com-Seite ein (für xyz.atomjump.com würden Sie „xyz“ eingeben) oder fügen Sie die in Ihrem privaten Forum angegebene URL ein , unter „Einstellungen“.


## Um ein Foto zu posten
Klicken Sie unten links auf das Symbol „Hochladen“. Wählen Sie ein JPG-Foto von Ihrem Laufwerk aus und klicken Sie auf „Hochladen“. Hinweis: Sie können mehr als ein Foto auswählen.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

Hinweis: Wenn Sie ein mobiles Gerät verwenden, z. Android: Um Zugriff auf Ihre Galerie statt auf die „Kamera“ zu erhalten, wählen Sie „Dokumente“ und dann „Galerie“.

Sie können eine zoombare Vorschau des Fotos anzeigen, indem Sie auf die angezeigten Miniaturvorschauen tippen. Tippen Sie erneut auf das Foto, um die vollständige Vorschau zu schließen.

## So posten Sie einen Aufkleber **
Klicken Sie auf das Smiley-Gesicht in der unteren linken Ecke. Klicken Sie dann auf Ihren Aufkleber.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## Zum Herunterladen von Nachrichten
Melden Sie sich mit Ihrem Konto an. Klicken Sie unten links auf das Symbol „Hochladen“. Klicken Sie auf die Schaltfläche „Herunterladen“. Es wird eine .xlsx-Tabellenkalkulationsdatei heruntergeladen, die mit jeder Tabellenkalkulationssoftware geöffnet werden kann, z. B. Open Office, Google Docs, Microsoft Office.

## Um die Sprache zu ändern **
Klicken Sie unten links auf „Einstellungen“. Klicken Sie dann auf „Mehr“ und öffnen Sie das Feld „Sprache“. Wähle deine Sprache. Klicken Sie dann auf „Einstellungen speichern“. Möglicherweise müssen Sie Ihr Passwort erneut eingeben, wenn Sie angemeldet sind.

Hinweis: Das vordere graue Feld mit dem Symbol „Einstellungen“ ändert die Sprache nur, wenn die umgebende Website diese Funktion implementiert hat. Möglicherweise müssen Sie die Seite in Ihrem Browser aktualisieren. Wenn Sie die Sprache ändern, ändern sich die „Leitfaden“-Meldungen im zentralen Feld. Es hat keinen Einfluss auf den von anderen Benutzern eingegebenen Text.

## Zum Vollbildmodus **

Wenn Sie sich auf Ihrem **Android-Telefon** auf einer häufig genutzten Nachrichtenseite von AtomJump befinden, rufen Sie die „Einstellungen“ für den Browser auf (häufig ein 3-Balken-Symbol neben der Adresse oben) und wählen Sie die Option „Zum Startbildschirm hinzufügen“. , oder etwas ähnliches.

Wenn Sie sich auf Ihrem **iPhone** auf einer häufig genutzten AtomJump-Nachrichtenseite befinden, tippen Sie unten auf der Seite auf das Symbol „Teilen“ und dann unten auf das Symbol „Zum Startbildschirm hinzufügen“ oder etwas Ähnliches.

Gehen Sie dann zurück zum Startbildschirm Ihres Telefons und tippen Sie auf das Symbol. Dadurch wird dann während der Nachrichtenübermittlung die Vollbildfläche genutzt.

## So löschen Sie Nachrichten

Jeder kann jede Nachricht auf AtomJump.com löschen, und wir erlauben dies, sodass die Foren größtenteils selbst moderieren. Das kann zwar bedeuten, dass Ihre Nachricht von jemandem zu Unrecht gelöscht wird, bedeutet aber, dass nur die Nachrichten im Forum verbleiben, mit denen alle einverstanden sind. Wenn Sie Probleme haben, die mit dieser Richtlinie nicht gelöst werden können, kontaktieren Sie uns bitte auf der Seite Datenschutz.

Um eine Nachricht zu löschen, tippen oder klicken Sie auf die Nachricht selbst. Daraufhin wird die Nachricht mit einem Papierkorbsymbol darunter angezeigt. Tippen Sie auf das Mülleimer-Symbol. Die Nachricht sollte in wenigen Sekunden entfernt werden.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

Hinweis: Nachdem eine Nachricht gepostet wurde, haben Sie 20 Minuten Zeit (bei einer Standardinstallation von AtomJump, einschließlich AtomJump.com), die Nachricht zu löschen, bevor andere Roboter-Feeds die Möglichkeit haben, die Nachricht zu kopieren.

## Um flackernde Bilder auf Android zu stoppen

Wenn Sie auf einem Android-Telefon eine Verbindung zu einem nicht sicheren „http“-Messaging-Server herstellen und in der Browsereinstellung Ihres Telefons der „Lite-Modus“ aktiviert ist, werden Sie wahrscheinlich iMagier scheinen etwa alle 5 Sekunden zu flackern. Wir empfehlen, den „Lite-Modus“ auszuschalten, um das Flackern auf der Chrome-Browser-Einstellungsseite Ihres Telefons zu stoppen.

Alternativ sollte das Problem verschwinden, wenn dem Server eine „https“-Domäne zugeordnet ist.
 
 

__** Diese Funktion ist bei AtomJump.com standardmäßig aktiviert, möglicherweise jedoch nicht, wenn Ihr Anbieter diese Plugins nicht integriert hat.__
