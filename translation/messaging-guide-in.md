<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom-konten-saja
Repositori ini adalah salinan publik dari konten penting di https://atomjump.com,
kalau-kalau server sedang down.




# Panduan Pengguna Pesan


## Perkenalan
AtomJump Messaging adalah 'Aplikasi Web Progresif' berlapis-lapis, untuk pengiriman pesan langsung dan komunikasi forum. Intinya adalah antarmuka perpesanan yang mengoperasikan satu forum, dan panduan ini akan mengajarkan Anda cara mengoperasikan antarmuka ini. Anda dapat menemukan forum AtomJump dengan mengklik link tertentu di situs web, yang akan membuka forum tersebut di kotak pop-up di browser Anda. Situs web mana pun dapat mengoperasikan forum ini, dan AtomJump.com adalah salah satu situs web tersebut.

Ada aplikasi starter opsional (yang berjalan di browser setelah mengetuk ikon beranda), yang memungkinkan Anda mengumpulkan forum favorit Anda di satu tempat dan memeriksa pesan baru dari forum AtomJump Messaging mana pun di internet. Semua komponen ini bersifat open source dan gratis. AtomJump Messaging adalah 'Aplikasi Lebih Aman' yang terdaftar.

* https://atomjump.com/wp/safer-apps/

Menjadi Aplikasi Web Progresif, ada beberapa keunggulan dibandingkan aplikasi perpesanan toko aplikasi generasi pertama:

* Anda dapat mendiskusikan sesuatu di ponsel Anda dan langsung beralih ke versi desktop dari percakapan yang sama, bahkan mungkin tanpa login.
* Anda dapat mengundang sekelompok orang ke forum, yang belum pernah masuk ke AtomJump sebelumnya, cukup dengan mengirimkan tautan web. Mereka tidak perlu membuat akun.
* Bagi orang baru untuk bergabung dalam percakapan, tidak ada pengunduhan aplikasi besar yang sulit, dengan banyak permintaan keamanan di telepon.
* Setiap forum berbeda dapat dioperasikan oleh organisasi berbeda, dan fungsi forum tersebut dipilih oleh operator, yang disusun berdasarkan blok bangunan dasar (disebut ‘plugin’).

Untuk memulai, Anda dapat mencoba forum di mis. https://atomjump.com

Untuk mendapatkan aplikasi starter AtomJump Messaging opsional (iPhone/Android/Desktop):

* https://app.atomjump.com

## Posting Pesan
Jika Anda berada di AtomJump.com, klik tombol 'obrolan' besar berwarna biru untuk masuk ke forum. Situs lain yang menggunakan perangkat lunak AtomJump akan menampilkan pop-up pesan setelah Anda mengklik link tertentu.

Mulailah mengetik di kotak 'Masukkan komentar Anda' di bagian atas dan klik 'kirim'. Itu dia! Anda tidak memerlukan akun, meskipun Anda akan dipanggil 'Anon XX' jika Anda belum menyebutkan nama diri Anda.


## Untuk Menulis Pesan
Anda dapat memasukkan beberapa pintasan perpesanan umum untuk berbagai jenis media.

__``Masuk``__ Membuat

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``lol`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭`` 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``Harap bayar 5 dolar kepada saya`` <a href="https://www.paypal.com/webapps/xorouter/Paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bXAl MmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDULMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY29kZSUzZ FVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0ZWRHdWVz dCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">Harap bayar 5 dolar kepada saya< /a> [dengan tautan ke Paypal]

``Harap bayar 3,30 pound kepada saya`` <a href="https://www.paypal.com/webapps/xorouter/Paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1 bXAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW5jeV9 jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYlMjUzYU 5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">Harap bayar 3,30 pound kepada saya< /a> [dengan tautan ke Paypal]

``http://yoururl.com/yourvideo.mp4`` [menampilkan ikon video abu-abu, untuk diklik]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [menampilkan gambar video, untuk diklik]

``london@`` <a href="http://london.atomjump.com">london@</a> [dengan tautan ke http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [dengan tautan ke http://london.atomjump.com]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text. com">Perluas</a> [dengan tautan ke http://a.very.long.link.with.lots.of.text.com]

``http://yoururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [menunjukkan gambar]

``http://yoururl.com/youraudio.mp3`` [menampilkan ikon telinga abu-abu untuk diklik]

``http://yoururl.com/yourdoc.pdf`` [menampilkan ikon PDF untuk diklik]


** Fitur ini diaktifkan secara default di AtomJump.com, namun mungkin tidak aktif jika penyedia Anda tidak menyertakan plugin ini.

## Ke Konferensi Video

Saat Anda berada di forum, klik ikon kamera di sebelah tombol 'Kirim'. Ini akan memberikan layar konfirmasi sebelum memasuki forum video. Beberapa browser memerlukan kamera untuk diterima di izin browser Anda terlebih dahulu.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

Pengguna telepon mungkin lebih suka mengunduh aplikasi terpisah.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

Untuk bertemu seseorang, Anda sebaiknya mengirimkan URL forum AtomJump kepada orang lain (di AtomJump.com Anda dapat mengklik ikon bagikan) dan meminta mereka untuk mengklik kamera. Anda akan menemukan petunjuk lebih lanjut di Jitsi. https://jitsi.org/user-faq/

Catatan: Forum video ini bersifat publik, kecuali Anda berada di forum pribadi, dan meskipun Anda harus menerima pengguna baru sebelum mereka bergabung, setelah ini Anda harus berhati-hati dengan apa yang Anda tunjukkan kepada orang lain. Jika Anda menginginkan privasi di sini, Anda harus memulai sesi satu kali, atau membeli forum pribadi.

Metode Alternatif: Meskipun Anda dapat mengeklik ke forum video di sebagian besar platform, beberapa platform misalnya. khususnya iPad/Internet Explorer, Anda juga dapat menjalankan desktop/aplikasi secara langsung, dan menyalin alamat forum ke dalamnya. Misalnya. Untuk halaman AtomJump.com alamat forumnya adalah 'https://jitsi0.atomjump.com/[kode unik]ajps' diikuti dengan subdomain. Anda juga dapat menemukannya dengan melihat alamat ikon "tautan" berwarna ungu.


## Untuk Menamai Diri Anda
Klik 'Pengaturan' di sudut kiri bawah. Tulis nama Anda di 'Nama Anda' dan klik 'Simpan Pengaturan'. Anda tidak memerlukan akun lengkap, tetapi jika Anda ingin menerima notifikasi saat menerima pesan pribadi, Anda memerlukannya (lihat di bawah).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/nama-anda.png">


## Untuk Menyiapkan Akun

Menyiapkan akun berarti Anda dapat menerima pesan pribadi. Klik 'Pengaturan' di sudut kiri bawah. Tulis nama Anda di 'Nama Anda', dan alamat email Anda di 'Email Anda'.

Secara opsional, Anda dapat menambahkan kata sandi pada akun tersebut (yang mencegah orang lain dapat membaca pesan pribadi Anda). Jika Anda ingin menambahkan ini, dan sangat disarankan, klik 'Lainnya' dan masukkan kata sandi Anda.

Klik 'Simpan Pengaturan'.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

Anda akan mendapatkan pemberitahuan email pada pesan pribadi, namun kami tidak menggunakan alamat email Anda untuk tujuan lain apa pun.

## Untuk Masuk di PC atau ponsel baru
Gunakan email dan kata sandi yang sama dengan yang Anda masukkan pada perangkat pertama saat menyiapkan akun.

Ingat, jika Anda menggunakan PC umum, Anda harus logout setiap kali Anda meninggalkan PC, karena detail login Anda akan diingat.

Catatan: setiap instalasi AtomJump Server yang berbeda memiliki database pengguna pribadinya sendiri. Jika akun Anda diatur di AtomJump.com, maka akun tersebut dapat dibagikan ke seluruh properti AtomJump.com lainnya, namun, akun tersebut belum tentu berfungsi di setiap versi popup perpesanan yang mungkin Anda temukan, karena ini dijalankan oleh perusahaan lain .

## Untuk Menulis Pesan Pribadi ke Satu Orang
Klik atau ketuk nama orang tersebut di samping salah satu pesannya. Kemudian masukkan pesan dan ketuk 'Kirim ke [nama mereka]'.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

Anda perlu mengklik 'Go Public' untuk mulai memposting pesan agar semua orang dapat melihatnya lagi.

## Untuk Menulis Pesan Pribadi ke Pelanggan Forum
Klik 'Jadikan Pribadi'. Masukkan pesan seperti biasa.

Setelah Anda siap untuk kembali memposting ke forum publik, klik 'Go Public' lagi.

## Untuk Membalas Pengguna Twitter
Klik atau ketuk nama orang tersebut di samping salah satu pesannya. Kemudian masukkan pesan dan ketuk 'Kirim'.

Catatan, balasan Twitter akan selalu bersifat publik, dan Anda tidak memerlukan akun Twitter. Sebuah pesan akan otomatis terkirim dari akun AtomJump kami yang menyatakan bahwa ada pesan menunggu mereka di forum tempat Anda berada saat ini. Terserah mereka apakah mereka akan membaca pesan Twitter mereka dan meresponsnya.

## Untuk Menjadi Pelanggan Forum

Jika Anda <b><i>tidak memiliki akun</i></b>, dan hanya ingin menerima notifikasi email (tanpa harus masuk), Anda dapat berlangganan dengan mengetuk dan mengalihkan telinga ke warna hijau 'mendengarkan telinga' di jendela pesan, yang akan menanyakan alamat email Anda. Kemudian klik 'Berlangganan'.

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>Tidak berlangganan. Ketuk untuk berlangganan.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>Berlangganantempat tidur. Ketuk untuk berhenti berlangganan.</i>

Jika Anda <b><i>memiliki akun</i></b>, Anda harus login terlebih dahulu. Untuk melakukan ini, klik 'Pengaturan', klik 'Lainnya' untuk kotak kata sandi, dan masukkan email dan kata sandi Anda. Kemudian klik 'Masuk'.

Sekarang di halaman perpesanan, ketuk dan alihkan telinga ke 'telinga mendengarkan' berwarna hijau untuk berlangganan satu forum itu.

Untuk <b>berhenti berlangganan</b> dari forum berlangganan, ketuk dan alihkan ke 'telinga yang tidak mendengarkan' berwarna merah di halaman perpesanan.

## Untuk Menyiapkan Grup

Masukkan nama grup Anda sendiri, atau nama grup publik yang sudah ada, yang bertuliskan “Masukkan nama grup” di AtomJump.com, dan klik <span class="notranslate">“AtomJump”</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

misalnya Memasukkan <span class="notranslate">“sailinglondon”</span> dan mengetuk <span class="notranslate">“AtomJump”</span> akan membawa browser Anda ke alamat berikut:

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

Bagikan tautan web forum Anda dengan grup Anda melalui email, SMS, pesan instan, atau cara lain. Anda dapat menemukan tautan ini dengan cepat di atomjump.com dengan mengetuk ikon 'bagikan' di sudut kanan atas halaman utama.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

Setiap individu kemudian dapat memilih untuk menjadi pelanggan forum untuk mendapatkan notifikasi berkelanjutan.

<b>Tips Lanjutan:</b> Anda dapat menambahkan <span class="notranslate">“/go/”</span> secara opsional di akhir tautan web Anda, yang akan segera membuka forum langsung untuk tamu Anda (sehingga mereka tidak perlu mengetuk tombol 'pergi' berwarna biru yang besar).

<b>Alamat Sementara</b>

Jika Anda dikirim ke alamat sementara (misalnya http://sailinglondon.clst4.atomjump.com), dan diberi tahu bahwa diperlukan waktu 2-48 jam untuk aktif sepenuhnya, Anda dapat memeriksa status transfer domain di https: //dnschecker.org. Masukkan alamat halaman yang Anda tuju, mis. "sailinglondon.atomjump.com" dan cari. Yang perlu Anda konfirmasi adalah bahwa semua alamat yang dikembalikan berbeda dengan "114.23.93.129" (yang merupakan alamat rumah atomjump.com yang ada). Jika ini masalahnya, semua orang dapat melihat server yang benar dan Anda aman mengundang orang lain ke forum. Jika tidak, ada kemungkinan beberapa pengguna akan menulis ke forum pesan, namun pesan tersebut akan dikirim ke server yang salah, dan tidak akan terlihat oleh pengguna lain.

## Untuk Menyiapkan Kamar Pribadi

Di AtomJump․com, Anda dapat memasukkan nama ruangan unik Anda sendiri yang bertuliskan <span class="notranslate">“Masukkan nama grup”</span>, misalnya. <span class="notranslate">“fredfamily”</span>, dan ketuk entri paling bawah <span class="notranslate">‘Create a PRIVATE room’ ('Buat ruang PRIBADI')</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

Di AtomJump․com, biayanya NZ$ 15 / tahun (kira-kira $US 10 / tahun). Anda juga dapat mendaftar langsung di <a href="http://atomjump.org/wp/introduction/">halaman ini</a>. Di situs lain, Anda harus menghubungi administrator sistem Anda untuk menambahkan kata sandi forum pribadi.

Catatan: kata sandi forum yang Anda putuskan berbeda dengan kata sandi pribadi Anda dan harus dapat Anda kirim melalui email ke setiap anggota ruangan dengan nyaman.

## Untuk Mendapatkan Notifikasi SMS
Catatan, di AtomJump.com hal ini telah digantikan oleh aplikasi, yang gratis, dan menyediakan popup pesan grafis. Namun, instruksi ini mungkin masih berlaku untuk instalasi server lainnya.

Klik 'Pengaturan' di sudut kiri bawah. Klik 'Lainnya', dan masukkan nomor telepon internasional Anda tanpa tanda tambah '+' dan tanpa spasi, misalnya. '+64 3 384 5613' harus dimasukkan sebagai '6433845613'.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

Kemungkinan besar akan ada biaya dari penyedia Anda (misalnya 16c per pesan), jadi harap periksa instruksi dari penyedia Anda.

## Untuk Mendapatkan Notifikasi Aplikasi

Notifikasi Aplikasi, meskipun memungkinkan, tidak terlalu terlihat di AtomJump Messaging. Umumnya, orang-orang di balik AtomJump lebih menyukai kehidupan yang tenang, jadi sebaiknya perlakukan notifikasi pesan Anda seperti Anda memperlakukan email: Anda memeriksa pesan baru saat Anda ingin memeriksanya, bukan saat orang lain ingin Anda memeriksanya. Dengan kata lain, Anda mengendalikan ruang Anda dan menghormati keinginan umum orang lain untuk diam dari ponsel Anda.

Pergi ke alamat web forum. Klik 'Pengaturan'.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Ada beberapa tautan ke 'Dapatkan pemberitahuan popup' untuk Android dan iOS. Jika ini di server pribadi, Anda perlu menyalin alamat server yang disebutkan. Klik tautan aplikasi, lalu, jika Anda belum melakukannya, "instal" aplikasi tersebut dengan langkah-langkah di tautan 'Instal' di perangkat Android, iPhone, atau desktop Anda. Ini akan membuat ikon beranda yang dapat dikunjungi kembali.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

Di dalam aplikasi, ketuk <span class="notranslate">"Register on an AtomJump group or a Private Server" ("Daftar di grup AtomJump atau Server Pribadi")</span> danmasukkan nama grup halaman xyz.atomjump.com Anda (untuk xyz.atomjump.com Anda akan mengetik 'xyz'), atau tempelkan URL yang disebutkan di forum pribadi Anda, di bawah 'Pengaturan'.

Anda akan diminta untuk masuk, dan Anda dapat membuat akun, atau memasukkan akun yang sudah ada. Jika ini selesai, browser akan menunjukkan bahwa Anda telah mengaitkan aplikasi pemula dengan alamat email Anda di server tersebut, dan aplikasi awal itu sendiri akan bertuliskan <span class="notranslate">‘Listening for Messages’ ('Mendengarkan Pesan')</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

Artinya, pesan pribadi apa pun untuk akun ini akan muncul saat aplikasi dibuka, dan di beberapa perangkat (yaitu desktop/Android) akan mengeluarkan bunyi ping jika aplikasi berjalan di tab latar belakang. Namun pada banyak ponsel Android, suara ping tidak akan terdengar jika ponsel sudah masuk ke mode sleep.

Ketuk tab ini untuk melihat versi lengkap pesan tersebut, lalu ketuk <span class="notranslate">‘Open Forum’ ('Buka Forum')</span> untuk masuk ke forum itu sendiri, sehingga Anda dapat merespons.

Untuk <b>menghentikan notifikasi pesan</b> kapan saja Anda dapat 'Membatalkan pendaftaran' ke server tertentu. Pemberitahuan pesan akan dilakukan melalui email saat Anda logout. Mendaftar lagi akan memulai notifikasi lagi.

Anda dapat berlangganan ke sejumlah forum mana pun. Lihat <a href="#become-owner">‘Menjadi Pelanggan Forum’</a> di atas.


## Untuk Menyimpan Pintasan Forum
Jika Anda menggunakan ponsel, kami sarankan untuk menginstal aplikasi starter AtomJump Messaging sebagai pintasan desktop.

* https://app.atomjump.com

Ketuk "Daftar di grup AtomJump atau Server Pribadi" dan masukkan nama grup halaman xyz.atomjump.com Anda (untuk xyz.atomjump.com Anda akan mengetik 'xyz'), atau tempelkan di URL yang disebutkan di forum pribadi Anda , di bawah 'Pengaturan'.


## Untuk Memposting Foto
Klik ikon 'unggah' di pojok kiri bawah. Pilih foto .jpg dari drive Anda dan klik 'Unggah'. Catatan: Anda dapat memilih lebih dari satu foto.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

Catatan: jika Anda menggunakan perangkat seluler, mis. Android, untuk mendapatkan akses ke Galeri Anda daripada 'Kamera', pilih 'Dokumen' dan kemudian 'Galeri'.

Anda dapat memilih untuk melihat pratinjau foto yang dapat diperbesar dengan mengetuk pratinjau gambar mini yang muncul. Ketuk foto itu lagi untuk menutup pratinjau penuh.

## Untuk Memposting Stiker **
Klik wajah smiley di pojok kiri bawah. Lalu klik stiker Anda.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## Untuk Mengunduh Pesan
Masuk dengan akun Anda. Klik ikon 'Unggah' di sudut kiri bawah. Klik tombol 'Unduh'. Ini akan mengunduh file spreadsheet .xlsx, yang dapat dibuka oleh perangkat lunak spreadsheet apa pun, misalnya. Buka Office, Google Dokumen, Microsoft Office.

## Untuk Mengubah Bahasa **
Klik 'Pengaturan' di sudut kiri bawah. Kemudian klik 'Lainnya' dan tarik ke bawah kotak 'Bahasa'. Pilih bahasamu. Kemudian klik 'Simpan Pengaturan'. Anda mungkin perlu memasukkan kata sandi lagi jika Anda masuk.

Catatan: kotak abu-abu depan dengan ikon 'Pengaturan' hanya akan mengubah bahasa jika situs di sekitarnya telah menerapkan fitur ini, dan Anda mungkin perlu menyegarkan halaman di browser Anda. Mengubah bahasa akan mengubah pesan 'panduan' di kotak tengah. Itu tidak mempengaruhi teks yang dimasukkan oleh pengguna lain.

## Untuk Menjadi Layar Penuh **

Di **ponsel Android** Anda, ketika Anda berada di halaman perpesanan AtomJump yang sering dikunjungi, masuklah ke 'pengaturan' untuk browser (biasanya berupa ikon 3 batang di dekat alamat di bagian atas), dan pilih opsi 'Tambahkan ke layar Utama' , atau yang serupa.

Di **iPhone** Anda, saat Anda berada di halaman perpesanan AtomJump yang sering dikunjungi, ketuk ikon 'bagikan' di bagian bawah halaman, lalu ikon 'Tambahkan ke layar Utama' di bagian bawah, atau yang serupa.

Kemudian kembali ke layar beranda ponsel Anda dan ketuk ikonnya. Ini kemudian akan menggunakan real estat layar penuh selama pengiriman pesan.

## Untuk Menghapus Pesan

Siapa pun dapat menghapus pesan apa pun di AtomJump.com, dan kami mengizinkannya sehingga sebagian besar forum dapat melakukan moderasi mandiri. Meskipun ini mungkin berarti bahwa pesan Anda dihapus secara tidak adil oleh seseorang, itu berarti hanya pesan yang disetujui semua orang yang akan tetap berada di forum. Jika Anda memiliki masalah yang tidak dapat diselesaikan dengan kebijakan ini, silakan hubungi kami di halaman Privasi.

Untuk menghapus pesan, ketuk atau klik pesan itu sendiri, dan Anda akan diperlihatkan pesan dengan ikon tempat sampah di bawahnya. Ketuk ikon tempat sampah, dan pesan tersebut akan dihapus dalam beberapa detik.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

Catatan: Setelah pesan diposting, Anda memiliki waktu 20 menit (untuk instalasi default AtomJump, dan itu termasuk AtomJump.com) untuk menghapus pesan tersebut sebelum feed robot lain memiliki kesempatan untuk menyalin pesan tersebut.

## Untuk Menghentikan Gambar Berkedip di Android

Pada ponsel Android, jika Anda tersambung ke server Pesan 'http' yang tidak aman, dan 'Mode Ringan' pada pengaturan browser ponsel Anda diaktifkan, kemungkinan besar Anda akan menemukan ipenyihir tampaknya berkedip setiap 5 detik atau lebih. Kami sarankan untuk menonaktifkan 'Mode Ringan' untuk menghentikan kedipan di laman Setelan Browser Chrome ponsel Anda.

Alternatifnya, jika domain 'https' dikaitkan dengan server, masalahnya akan hilang.
 
 

__** Fitur ini diaktifkan secara default di AtomJump.com, namun mungkin tidak aktif jika penyedia Anda belum menyertakan plugin ini.__
