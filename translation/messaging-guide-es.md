<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom-solo contenido
Este repositorio es una copia pública del contenido importante en https://atomjump.com,
en caso de que los servidores estén caídos.




# Guía del usuario de mensajería


## Introducción
AtomJump Messaging es una 'aplicación web progresiva' de múltiples capas, para mensajería en vivo y comunicación en foros. En esencia, hay una interfaz de mensajería que opera un solo foro, y esta guía le enseñará cómo operar esta interfaz. Puede encontrar foros de AtomJump haciendo clic en enlaces particulares en un sitio web, lo que abrirá el foro en un cuadro emergente dentro de su navegador. Cualquier sitio web puede operar estos foros y AtomJump.com es uno de estos sitios web.

Hay una aplicación de inicio opcional (que se ejecuta en un navegador después de tocar el ícono de la página de inicio), que le permite recopilar sus foros favoritos en un solo lugar y buscar mensajes nuevos de cualquier foro de AtomJump Messaging en Internet. Todos estos componentes son de código abierto y gratuitos. AtomJump Messaging es una "aplicación más segura" registrada.

* https://atomjump.com/wp/safer-apps/

Al ser una aplicación web progresiva, existen ciertas ventajas sobre las aplicaciones de mensajería de la tienda de aplicaciones de primera generación:

* Puedes estar discutiendo algo en tu teléfono y cambiar a una versión de escritorio de la misma conversación, inmediatamente, potencialmente sin siquiera iniciar sesión.
* Puedes invitar a un grupo de personas a un foro, que no hayan iniciado sesión antes en AtomJump, simplemente enviando un enlace web. No es necesario que creen una cuenta.
* Para que una nueva persona se una a la conversación, no es necesario descargar una aplicación grande y difícil de manejar, con muchas solicitudes de seguridad en el teléfono.
* Cada foro diferente puede ser operado por una organización diferente, y la funcionalidad de ese foro la elige el operador, y se construye a partir de bloques de construcción básicos (llamados "complementos").

Para comenzar, puede probar un foro en, p. https://atomjump.com

Para obtener la aplicación de inicio opcional AtomJump Messaging (iPhone/Android/Desktop):

* https://app.atomjump.com

## Deja un mensaje
Si estás en AtomJump.com, haz clic en el botón azul grande de "chat" para ingresar a un foro. Otros sitios que utilizan el software AtomJump mostrarán la ventana emergente de mensajes después de hacer clic en un enlace en particular.

Comience a escribir en el cuadro "Ingrese su comentario" en la parte superior y haga clic en "enviar". ¡Eso es todo! No necesitas una cuenta, aunque te llamarán "Anon XX" si no te has nombrado.


## Para redactar un mensaje
Puede ingresar algunos atajos de mensajería comunes para varios tipos de medios.

__``Intro``__ Crea

``http://enlace.com`` http://enlace.com

``enlace.com`` <a href="http://link.com">enlace.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``jajaja`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭`` 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``Por favor, págame 5 dólares`` <a href="https://www.paypal.com/webapps/xorouter/paidstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1b XAlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY2 9kZSUzZFVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0Z WRHdWVzdCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">Por favor, págame 5 dólares< /a> [con enlace a Paypal]

``Por favor, págame 3,30 libras`` <a href="https://www.paypal.com/webapps/xorouter/paidstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbW p1bXAlMmVjdXJyZW 5jeV9jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYl MjUzYU5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">Por favor, págame 3,30 libras< /a> [con enlace a Paypal]

``http://yoururl.com/yourvideo.mp4`` [muestra un ícono de video gris en el que se puede hacer clic]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [muestra una imagen del vídeo, en la que se puede hacer clic]

``london@`` <a href="http://london.atomjump.com">london@</a> [con enlace a http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [con enlace a http://london.atomjump.com]

``http://un.enlace.muy.largo.con.mucho.texto.com`` <a href="http://un.enlace.muy.largo.con.mucho.texto. com">Expandir</a> [con enlace a http://a.very.long.link.with.lots.of.text.com]

``http://tururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [muestra imagen]

``http://yoururl.com/youraudio.mp3`` [muestra un ícono de oreja gris en el que se puede hacer clic]

``http://yoururl.com/yourdoc.pdf`` [muestra un icono de PDF en el que se puede hacer clic]


** Esta función está activada de forma predeterminada en AtomJump.com, pero es posible que no lo esté si su proveedor no ha incluido estos complementos.

## A videoconferencia

Cuando estés en un foro, haz clic en el ícono de la cámara al lado del botón "Enviar". Esto proporcionará una pantalla de confirmación antes de ingresar al foro de video. Algunos navegadores necesitan que la cámara sea aceptada primero en los permisos de su navegador.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

Es posible que los usuarios de teléfonos prefieran descargar una aplicación separada.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

Para conocer a alguien, lo mejor es enviar a otras personas una URL del foro AtomJump (en AtomJump.com puedes hacer clic en el ícono de compartir) y decirles que hagan clic en la cámara. Encontrarás más instrucciones en Jitsi. https://jitsi.org/user-faq/

Nota: Estos foros de vídeo son públicos, a menos que estés en un foro privado, y si bien debes aceptar nuevos usuarios antes de que se unan, después de esto debes tener cuidado con lo que les muestras a otras personas. Si desea privacidad aquí, debe iniciar una sesión única o comprar un foro privado.

Método alternativo: si bien debería poder hacer clic en el foro de vídeo en la mayoría de las plataformas, algunas plataformas, por ejemplo. particularmente iPads/Internet Explorer, también puedes ejecutar el escritorio/aplicación directamente y copiar la dirección del foro en él. P.ej. Para las páginas de AtomJump.com, la dirección del foro será 'https://jitsi0.atomjump.com/[un código único]ajps' seguida del subdominio. También puedes encontrarlo mirando la dirección del ícono violeta de "enlace".


## Para ponerte un nombre
Haga clic en "Configuración" en la esquina inferior izquierda. Escriba su nombre en "Su nombre" y haga clic en "Guardar configuración". No necesita una cuenta completa, pero si desea recibir notificaciones cuando reciba un mensaje privado, necesitará una (ver más abajo).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/tu-nombre.png">


## Para configurar una cuenta

Configurar una cuenta significa que puedes recibir mensajes privados. Haga clic en "Configuración" en la esquina inferior izquierda. Escriba su nombre en "Su nombre" y su dirección de correo electrónico en "Su correo electrónico".

Opcionalmente, puedes agregar una contraseña a la cuenta (lo que evita que otra persona pueda leer tus mensajes privados). Si desea agregar esto, y es muy recomendable, haga clic en "Más" e ingrese su contraseña.

Haga clic en "Guardar configuración".

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

Recibirá notificaciones por correo electrónico en mensajes privados, pero no utilizamos su dirección de correo electrónico para ningún otro propósito.

## Para iniciar sesión en una nueva PC o teléfono
Utilice el mismo correo electrónico y contraseña que ingresó en su primer dispositivo cuando configuró la cuenta.

Recuerde, si está en una PC pública, debe cerrar sesión cada vez que salga de la PC, ya que se recordarán sus datos de inicio de sesión.

Nota: cada instalación diferente de AtomJump Server tiene su propia base de datos de usuarios privados. Si su cuenta se configuró en AtomJump.com, entonces la cuenta puede compartirse con otras propiedades de AtomJump.com, pero no necesariamente funcionará en todas las versiones de la ventana emergente de mensajería que pueda encontrar, porque están administradas por otras compañías. .

## Para escribir un mensaje privado a una persona
Haga clic o toque el nombre de la persona junto a uno de sus mensajes. Luego ingrese un mensaje y toque 'Enviar a [su nombre]'.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

Deberá hacer clic en "Hacer público" para comenzar a publicar mensajes para que todos puedan verlos nuevamente.

## Para escribir un mensaje privado a los suscriptores del foro
Haga clic en "Pasar a privado". Ingrese mensajes como de costumbre.

Una vez que esté listo para volver a publicar en el foro público, haga clic en "Hacer público" nuevamente.

## Para responder a un usuario de Twitter
Haga clic o toque el nombre de la persona junto a uno de sus mensajes. Luego ingrese un mensaje y toque 'Enviar'.

Tenga en cuenta que una respuesta de Twitter siempre será pública y no necesita una cuenta de Twitter. Se enviará automáticamente un mensaje desde nuestra cuenta AtomJump diciendo que hay un mensaje esperándolos en el foro en el que se encuentra actualmente. Depende de ellos si leerán sus mensajes de Twitter y responderán.

## Para convertirse en suscriptor del foro

Si <b><i>no tienes una cuenta</i></b> y solo quieres recibir notificaciones por correo electrónico (sin tener que iniciar sesión), puedes suscribirte tocando y cambiando la oreja a verde. 'oído atento' en la ventana de mensajería, que le pedirá su dirección de correo electrónico. Luego haga clic en 'Suscribirse'.

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>No suscrito. Toca para suscribirte.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>Suscribircama. Toca para cancelar la suscripción.</i>

Si <b><i>tienes una cuenta</i></b>, primero debes iniciar sesión. Para hacer esto, haga clic en "Configuración", haga clic en "Más" para el cuadro de contraseña e ingrese su correo electrónico y contraseña. Luego haga clic en 'Iniciar sesión'.

Ahora, en la página de mensajes, toque y cambie el oído al "oído que escucha" verde para suscribirse a ese foro.

Para <b>cancelar la suscripción</b> de un foro suscrito, toque y cambie al "oído que no escucha" rojo en la página de mensajería.

## Para configurar un grupo

Ingrese su propio nombre de grupo, o un nombre de grupo público existente, donde dice "Ingrese un nombre de grupo" en AtomJump.com, y haga clic en <span class="notranslate">“AtomJump”</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

p.ej. Al ingresar <span class="notranslate">“sailinglondon”</span> y tocar <span class="notranslate">“AtomJump”</span>, su navegador accederá a la siguiente dirección:

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

Comparta el enlace web de su foro con su grupo por correo electrónico, SMS, mensajes instantáneos o algún otro medio. Puede encontrar este enlace rápidamente en atomjump.com tocando el ícono "compartir" en la esquina superior derecha de la página subyacente.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

Luego, cada individuo puede optar por suscribirse al foro para recibir notificaciones continuas.

<b>Consejo avanzado:</b> opcionalmente, puedes agregar <span class="notranslate">“/go/”</span> al final de tu enlace web, lo que abrirá el foro en vivo para tus invitados inmediatamente. (para que no tengan que tocar el gran botón azul "ir").

<b>Direcciones temporales</b>

Si lo enviaron a una dirección temporal (por ejemplo, http://sailinglondon.clst4.atomjump.com) y le dijeron que tardaría entre 2 y 48 horas en estar completamente activo, puede verificar el estado de la transferencia de dominio en https: //dnschecker.org. Ingrese la dirección deseada de su página, p. "sailinglondon.atomjump.com" y busque. Lo que necesita confirmar es que todas las direcciones devueltas son diferentes a "114.23.93.129" (que es la dirección particular existente de atomjump.com). Una vez que este sea el caso, todos podrán ver el servidor correcto y podrás invitar a otras personas al foro con seguridad. De lo contrario, existe la posibilidad de que algunos usuarios escriban en el foro de mensajes, pero esos mensajes se enviarán al servidor equivocado y no serán visibles para otros usuarios.

## Para configurar una sala privada

En AtomJump․com, puedes ingresar el nombre de tu sala única donde dice <span class="notranslate">“Ingresa un nombre de grupo”</span>, p.e. <span class="notranslate">“fredfamily”</span> y toca la entrada inferior <span class="notranslate">‘Create a PRIVATE room’ (‘Crear una sala PRIVADA’)</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

En AtomJump․com, esto cuesta NZ$ 15 al año (aproximadamente 10 dólares estadounidenses al año). También puedes registrarte directamente en <a href="http://atomjump.org/wp/introduction/">esta página</a>. En otros sitios, debe comunicarse con el administrador de su sistema para agregar una contraseña privada al foro.

Nota: la contraseña del foro que decidas tener es diferente de tu contraseña personal y debe ser una que puedas enviar por correo electrónico a todos los miembros de la sala cómodamente.

## Para recibir notificaciones por SMS
Tenga en cuenta que en AtomJump.com esto ha sido reemplazado por la aplicación, que es gratuita y proporciona mensajes emergentes gráficos. Sin embargo, es posible que estas instrucciones aún se apliquen a otras instalaciones del servidor.

Haga clic en "Configuración" en la esquina inferior izquierda. Haga clic en "Más" e ingrese su número de teléfono internacional sin el signo más inicial "+" y sin espacios, p. "+64 3 384 5613" debe ingresarse como "6433845613".

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

Es probable que su proveedor le cobre un costo (por ejemplo, 16 centavos por mensaje), así que consulte las instrucciones de su proveedor.

## Para recibir notificaciones de aplicaciones

Las notificaciones de aplicaciones, si bien son posibles, no se notan mucho en AtomJump Messaging. Generalmente, las personas detrás de AtomJump prefieren una vida tranquila, por lo que es mejor tratar las notificaciones de mensajes como trata el correo electrónico: busca mensajes nuevos cuando quiere, no cuando todos los demás quieren que los revise. En otras palabras, tienes el control de tu espacio y respetas el deseo general de otras personas de guardar silencio desde tu teléfono.

Ir a la dirección web del foro. Haga clic en "Configuración".

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Hay algunos enlaces para "Recibir notificaciones emergentes" para Android e iOS. Si se trata de un servidor privado, deberá copiar la dirección del servidor mencionada. Haga clic en el enlace de la aplicación y luego, si aún no lo ha hecho, "instale" la aplicación siguiendo los pasos del enlace "Instalar" en su dispositivo Android, iPhone o de escritorio. Esto creará un ícono en la página de inicio que se podrá volver a visitar.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

Dentro de la aplicación, toque <span class="notranslate">"Register on an AtomJump group or a Private Server" ("Registrarse en un grupo AtomJump o en un servidor privado")</span> yingresa el nombre del grupo de tu página xyz.atomjump.com (para xyz.atomjump.com escribirías 'xyz'), o pega la URL mencionada en tu foro privado, en 'Configuración'.

Se le pedirá que inicie sesión y podrá crear una cuenta o ingresar a una cuenta existente. Si esto se completa, el navegador debería mostrar que ha asociado su aplicación de inicio con su dirección de correo electrónico en ese servidor, y la propia aplicación de inicio debería decir <span class="notranslate">‘Listening for Messages’ (‘Escuchando mensajes’)</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

Esto significa que cualquier mensaje personal para esta cuenta aparecerá cuando la aplicación esté abierta y, en algunos dispositivos (es decir, escritorio/Android), emitirá un sonido de ping si la aplicación se ejecuta en una pestaña en segundo plano. Sin embargo, en muchos teléfonos Android, el sonido de ping no se escuchará si el teléfono entra en modo de suspensión.

Toque esta pestaña para obtener una versión completa del mensaje y luego toque <span class="notranslate">‘Open Forum’ ('Abrir foro')</span> para ingresar al foro, lo que le permitirá responder.

Para <b>detener las notificaciones de mensajes</b> en cualquier momento, puede "Anular el registro" en un servidor en particular. Las notificaciones de mensajes se realizarán por correo electrónico mientras esté desconectado. Al registrarse nuevamente, se iniciarán las notificaciones nuevamente.

Puede suscribirse a cualquier número de foros. Consulte <a href="#become-owner">‘Para convertirse en suscriptor del foro’</a> más arriba.


## Para guardar un acceso directo al foro
Si está en su teléfono, le recomendamos instalar la aplicación de inicio AtomJump Messaging como acceso directo en el escritorio.

* https://app.atomjump.com

Toque "Registrarse en un grupo AtomJump o en un servidor privado" e ingrese el nombre del grupo de su página xyz.atomjump.com (para xyz.atomjump.com escribiría 'xyz') o pegue la URL mencionada en su foro privado. , en 'Configuración'.


## Para publicar una foto
Haga clic en el icono "cargar" en la esquina inferior izquierda. Seleccione una foto .jpg de su disco y haga clic en "Cargar". Nota: puedes elegir más de una foto.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

Nota: si está utilizando un dispositivo móvil, p. Android, para obtener acceso a su Galería en lugar de a la "Cámara", seleccione "Documentos" y luego "Galería".

Puede optar por ver una vista previa de la foto con capacidad de zoom tocando las vistas previas en miniatura que aparecen. Toque la foto nuevamente para cerrar la vista previa completa.

## Para publicar una pegatina **
Haz clic en la cara sonriente en la esquina inferior izquierda. Luego haz clic en tu pegatina.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## Para descargar mensajes
Inicie sesión con su cuenta. Haga clic en el icono "Cargar" en la esquina inferior izquierda. Haga clic en el botón "Descargar". Descargará un archivo de hoja de cálculo .xlsx, que se puede abrir con cualquier software de hoja de cálculo, p. Abra Office, Google Docs, Microsoft Office.

## Para cambiar el idioma **
Haga clic en "Configuración" en la esquina inferior izquierda. Luego haga clic en "Más" y despliegue el cuadro "Idioma". Elige tu idioma. Luego haga clic en "Guardar configuración". Es posible que deba ingresar su contraseña nuevamente si ya inició sesión.

Nota: el cuadro gris frontal con el ícono 'Configuración' solo cambiará de idioma si el sitio circundante ha implementado esta función y es posible que deba actualizar la página en su navegador. Al cambiar de idioma se cambiarán los mensajes 'guía' del cuadro central. No afecta el texto ingresado por otros usuarios.

## Para ir a pantalla completa **

En su **teléfono Android**, cuando esté en una página de mensajería de AtomJump frecuentada, vaya a la "configuración" del navegador (a menudo un ícono de tres barras cerca de la dirección en la parte superior) y seleccione la opción "Agregar a la pantalla de inicio". , o algo similar.

En tu **iPhone**, cuando estés en una página de mensajería de AtomJump frecuentada, toca el ícono "compartir" en la parte inferior de la página y luego el ícono "Agregar a la pantalla de inicio" en la parte inferior, o algo similar.

Luego regrese a la pantalla de inicio de su teléfono y toque el ícono. Esto luego utilizará el espacio de pantalla completa durante la mensajería.

## Para eliminar mensajes

Cualquiera puede eliminar cualquier mensaje en AtomJump.com y lo permitimos para que los foros se automoderen en gran medida. Si bien puede significar que alguien elimina injustamente tu mensaje, sí significa que solo los mensajes que todos aprueben permanecerán en el foro. Si tiene problemas que no se pueden resolver con esta política, contáctenos en la página de Privacidad.

Para eliminar un mensaje, toque o haga clic en el mensaje en sí y se le mostrará el mensaje con un icono de papelera debajo. Toque el icono de la papelera y el mensaje debería eliminarse en unos segundos.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

Nota: Después de publicar un mensaje, tiene 20 minutos (para una instalación predeterminada de AtomJump, y eso incluye AtomJump.com) para eliminar el mensaje antes de que otros feeds de robots tengan la oportunidad de copiarlo.

## Para detener las imágenes parpadeantes en Android

En un teléfono Android, si se está conectando a un servidor de mensajería "http" no seguro y la configuración del "Modo Lite" del navegador de su teléfono está activada, probablemente encontrará iLos magos parecen parpadear cada 5 segundos aproximadamente. Recomendamos desactivar el "Modo Lite" para detener el parpadeo en la página de configuración del navegador Chrome de su teléfono.

Alternativamente, si un dominio "https" está asociado con el servidor, el problema debería desaparecer.
 
 

__** Esta función está activada de forma predeterminada en AtomJump.com, pero es posible que no lo esté si su proveedor no ha incluido estos complementos.__
