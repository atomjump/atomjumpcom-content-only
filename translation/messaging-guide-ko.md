<img src="http://atomjump.org/images/logo80.png">

#atomjumpcom-콘텐츠 전용
이 저장소는 https://atomjump.com에 있는 중요한 콘텐츠의 공개 복사본입니다.
서버가 다운될 경우를 대비해.




# 메시징 사용자 가이드


## 소개
AtomJump Messaging은 라이브 메시징 및 포럼 커뮤니케이션을 위한 다계층 '프로그레시브 웹 앱'입니다. 그 핵심에는 단일 포럼을 운영하는 메시징 인터페이스가 있으며, 이 가이드에서는 이 인터페이스를 작동하는 방법을 알려줍니다. 웹사이트의 특정 링크를 클릭하면 AtomJump 포럼을 찾을 수 있습니다. 그러면 브라우저 내 팝업 상자에서 포럼이 열립니다. 모든 웹사이트에서 이러한 포럼을 운영할 수 있으며 AtomJump.com은 이러한 웹사이트 중 하나입니다.

선택적인 시작 앱(홈페이지 아이콘을 탭한 후 브라우저에서 실행됨)이 있어 즐겨찾는 포럼을 한 곳에서 수집하고 인터넷을 통해 AtomJump Messaging 포럼의 새 메시지를 확인할 수 있습니다. 이러한 구성 요소는 모두 오픈 소스이며 무료입니다. AtomJump Messaging은 '안전한 앱'으로 등록되었습니다.

* https://atomjump.com/wp/safer-apps/

프로그레시브 웹 앱이기 때문에 1세대 앱 스토어 메시징 앱에 비해 다음과 같은 장점이 있습니다.

* 휴대전화로 무언가에 대해 논의하고 있을 때 로그인하지 않고도 즉시 동일한 대화의 데스크톱 버전으로 전환할 수 있습니다.
* 웹 링크를 전송하여 이전에 AtomJump에 로그인한 적이 없는 사람들을 포럼에 초대할 수 있습니다. 계정을 만들 필요가 없습니다.
* 새로운 사람이 대화에 참여하기 위해 전화로 많은 보안 요청이 있는 대형 앱을 다루기 힘들게 다운로드할 필요가 없습니다.
* 각각의 서로 다른 포럼은 서로 다른 조직에서 운영할 수 있으며 해당 포럼의 기능은 운영자가 선택하여 기본 빌딩 블록('플러그인'이라고 함)으로 구성됩니다.

시작하려면 예를 들어 포럼을 시도해 볼 수 있습니다. https://atomjump.com

선택적 AtomJump Messaging 시작 앱을 받으려면(iPhone/Android/데스크톱):

* https://app.atomjump.com

## 메세지를 보내다
AtomJump.com에 있는 경우 커다란 파란색 '채팅' 버튼을 클릭하여 포럼에 입장하세요. AtomJump 소프트웨어를 사용하는 다른 사이트에서는 특정 링크를 클릭하면 메시지 팝업이 표시됩니다.

상단의 '댓글 입력' 상자에 내용을 입력한 후 '보내기'를 클릭하세요. 그게 다야! 계정이 필요하지 않지만 자신의 이름을 지정하지 않으면 'Anon XX'라고 불립니다.


## 메시지를 작성하려면
다양한 유형의 미디어에 대한 몇 가지 일반적인 메시징 바로가기를 입력할 수 있습니다.

__``Enter``__ 생성

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``ㅋㅋㅋ`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭`` 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``저에게 5달러를 지불해주세요`` <a href="https://www.paypal.com/webapps/xorouter/paystandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bXAlMmV jb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY29kZSUzZ FVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0ZWRHdWV zdCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">5달러를 저에게 지불해주세요< /a> [페이팔 링크 포함]

``3.30파운드를 저에게 지불해주세요`` <a href="https://www.paypal.com/webapps/xorouter/paystandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bXAl MmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW5jeV9jb 2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYlMjUzYU5 vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">3.30파운드를 저에게 지불해주세요< /a> [페이팔 링크 포함]

``http://yoururl.com/yourvideo.mp4`` [클릭할 수 있는 회색 비디오 아이콘 표시]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [클릭할 수 있는 동영상 사진 표시]

``london@`` <a href="http://london.atomjump.com">london@</a> [http://london.atomjump.com 링크 포함]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [http://london.atomjump.com 링크 포함]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text. com">확장</a> [http://a.very.long.link.with.lots.of.text.com 링크 포함]

``http://yoururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [사진 표시]

``http://yoururl.com/youraudio.mp3`` [클릭할 수 있는 회색 귀 아이콘이 표시됨]

``http://yoururl.com/yourdoc.pdf`` [클릭할 수 있는 PDF 아이콘 표시]


** 이 기능은 AtomJump.com에서 기본적으로 켜져 있지만, 공급자가 이러한 플러그인을 포함하지 않은 경우 그렇지 않을 수도 있습니다.

## 화상회의로

포럼에 있을 때 '보내기' 버튼 옆에 있는 카메라 아이콘을 클릭하세요. 그러면 비디오 포럼에 입장하기 전에 확인 화면이 제공됩니다. 일부 브라우저에서는 먼저 브라우저 권한에서 카메라를 허용해야 합니다.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

휴대폰 사용자는 별도의 앱을 다운로드하는 것을 선호할 수 있습니다.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

누군가를 만나려면 다른 사람들에게 AtomJump 포럼의 URL(AtomJump.com에서는 공유 아이콘을 클릭할 수 있음)을 보내고 카메라를 클릭하라고 말하는 것이 가장 좋습니다. Jitsi에서 더 많은 지침을 찾을 수 있습니다. https://jitsi.org/user-faq/

참고: 이러한 비디오 포럼은 비공개 포럼이 아닌 한 공개되며, 새로운 사용자가 가입하기 전에 수락해야 하지만 이후에는 다른 사람들에게 무엇을 보여줄지 주의해야 합니다. 여기에서 개인정보 보호를 원하시면 일회성 세션을 시작하거나 비공개 포럼을 구매하셔야 합니다.

대체 방법: 대부분의 플랫폼에서 클릭하면 비디오 포럼으로 이동할 수 있지만 일부 플랫폼에서는 다음과 같습니다. 특히 iPad/Internet Explorer의 경우 데스크톱/앱을 직접 실행하고 포럼 주소를 복사할 수도 있습니다. 예: AtomJump.com 페이지의 경우 포럼 주소는 'https://jitsi0.atomjump.com/[고유 코드]ajps'이고 그 뒤에 하위 도메인이 옵니다. 보라색 "링크" 아이콘의 주소를 통해서도 찾을 수 있습니다.


## 자신의 이름을 지정하려면
왼쪽 하단의 '설정'을 클릭하세요. '이름'에 이름을 적고 '설정 저장'을 클릭하세요. 전체 계정이 필요하지는 않지만 비공개 메시지를 받았을 때 알림을 받으려면 계정이 필요합니다(아래 참조).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/your-name.png">


## 계정을 설정하려면

계정을 설정하면 비공개 메시지를 받을 수 있습니다. 왼쪽 하단의 '설정'을 클릭하세요. '이름'에는 이름을, '이메일'에는 이메일 주소를 기재하세요.

선택적으로 계정에 비밀번호를 추가할 수 있습니다(다른 사람이 귀하의 비공개 메시지를 읽을 수 없도록 방지). 이것을 추가하고 싶다면 '더보기'를 클릭하고 비밀번호를 입력하세요.

'설정 저장'을 클릭하세요.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

귀하는 비공개 메시지에 대한 이메일 알림을 받게 되지만 당사는 귀하의 이메일 주소를 다른 목적으로 사용하지 않습니다.

## 새 PC나 휴대폰에서 로그인하려면
계정을 설정할 때 첫 번째 기기에 입력한 것과 동일한 이메일 및 비밀번호를 사용하세요.

공용 PC를 사용하는 경우 로그인 정보가 기억되므로 PC를 떠날 때마다 로그아웃해야 합니다.

참고: 각기 다른 AtomJump 서버 설치에는 자체 개인 사용자 데이터베이스가 있습니다. 귀하의 계정이 AtomJump.com에 설정된 경우 해당 계정은 다른 AtomJump.com 속성에서 공유될 수 있지만, 다른 회사에서 운영하기 때문에 찾을 수 있는 메시징 팝업의 모든 버전에서 반드시 작동하지는 않습니다. .

## 한 사람에게 비공개 메시지를 쓰려면
메시지 옆에 있는 사람의 이름을 클릭하거나 탭하세요. 그런 다음 메시지를 입력하고 '[그 사람 이름]에게 보내기'를 탭하세요.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

모든 사람이 다시 볼 수 있도록 메시지 게시를 시작하려면 '공개'를 클릭해야 합니다.

## 포럼 구독자에게 비공개 메시지를 작성하려면
'비공개로 전환'을 클릭하세요. 정상적으로 메시지를 입력하세요.

공개 포럼에 게시할 준비가 되면 다시 '공개'를 클릭하세요.

## 트위터 사용자에게 답장하려면
메시지 옆에 있는 사람의 이름을 클릭하거나 탭하세요. 그런 다음 메시지를 입력하고 '보내기'를 탭하세요.

Twitter 답글은 항상 공개되며 Twitter 계정이 필요하지 않습니다. 귀하가 현재 참여 중인 포럼에 대기 중인 메시지가 있다는 메시지가 AtomJump 계정에서 자동으로 전송됩니다. 트위터 메시지를 읽고 응답할지 여부는 그들에게 달려 있습니다.

## 포럼 구독자가 되려면

<b><i>계정이 없고</i></b> 로그인할 필요 없이 이메일 알림만 받고 싶다면 귀를 탭하고 녹색으로 전환하여 구독할 수 있습니다. 귀하의 이메일 주소를 묻는 메시지 창에서 '듣는 귀'. 그런 다음 '구독'을 클릭하세요.

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>구독되지 않았습니다. 구독하려면 탭하세요.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>구독침대. 구독을 취소하려면 탭하세요.</i>

<b><i>계정이 있는</i></b> 경우 먼저 로그인해야 합니다. 이렇게 하려면 '설정'을 클릭하고 비밀번호 상자에서 '더 보기'를 클릭한 후 이메일과 비밀번호를 입력하세요. 그런 다음 '로그인'을 클릭하세요.

이제 메시지 페이지에서 귀를 탭하고 녹색 '듣는 귀'로 전환하여 해당 포럼을 구독하세요.

구독 중인 포럼을 <b>구독 취소</b>하려면 아이콘을 탭하고 메시지 페이지에서 빨간색 '듣지 않는 귀'로 전환하세요.

## 그룹을 설정하려면

자신의 그룹 이름 또는 AtomJump.com에서 "그룹 이름을 입력하세요"라고 표시된 기존 공개 그룹 이름을 입력하고 <span class="notranslate">"AtomJump"</span>를 클릭하세요.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

예를 들어 <span class="notranslate">“sailinglondon”</span>을 입력하고 <span class="notranslate">“AtomJump”</span>를 탭하면 브라우저가 다음 주소로 이동합니다.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

이메일, SMS, 인스턴트 메시지 또는 기타 수단을 통해 포럼의 웹 링크를 그룹과 공유하세요. 기본 페이지의 오른쪽 상단에 있는 '공유' 아이콘을 탭하면atomjump.com에서 이 링크를 빠르게 찾을 수 있습니다.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

그러면 각 개인은 포럼 구독자가 되어 지속적인 알림을 받을 수 있습니다.

<b>고급 팁:</b> 선택적으로 웹 링크 끝에 <span class="notranslate">"/go/"</span>를 추가하면 손님을 위한 실시간 포럼이 즉시 열립니다. (큰 파란색 '이동' 버튼을 탭할 필요가 없도록 하기 위해).

<b>임시 주소</b>

임시 주소(예: http://sailinglondon.clst4.atomjump.com)로 전송되었고 완전히 활성화되려면 2~48시간이 걸린다는 메시지를 받은 경우 https에서 도메인 이전 상태를 확인할 수 있습니다. //dnschecker.org. 페이지의 원하는 주소를 입력하세요. "sailinglondon.atomjump.com"을 검색해 보세요. 확인해야 할 것은 반환된 모든 주소가 "114.23.93.129"(기존atomjump.com 집 주소)와 다르다는 것입니다. 이 경우 모든 사람이 올바른 서버를 볼 수 있으며 다른 사람을 포럼에 안전하게 초대할 수 있습니다. 그렇지 않은 경우 일부 사용자가 메시지 포럼에 글을 쓸 가능성이 있지만 해당 메시지는 잘못된 서버로 전송되어 다른 사용자에게는 표시되지 않습니다.

## 개인실을 설정하려면

AtomJump․com에서는 <span class="notranslate">“그룹 이름을 입력하세요”</span>라고 표시된 고유한 방 이름을 입력할 수 있습니다. 예: <span class="notranslate">'fredfamily'</span>를 탭하고 하단 항목 <span class="notranslate">‘Create a PRIVATE room’ ('개인 방 만들기')</span>를 탭하세요.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

AtomJump․com에서는 연간 NZ$ 15(연간 약 $US 10)의 비용이 듭니다. <a href="http://atomjump.org/wp/introduction/">이 페이지</a>에서 직접 가입할 수도 있습니다. 다른 사이트에서는 시스템 관리자에게 문의하여 비공개 포럼 비밀번호를 추가해야 합니다.

참고: 귀하가 결정한 포럼 비밀번호는 개인 비밀번호와 다르며 방의 모든 구성원에게 편안하게 이메일을 보낼 수 있는 비밀번호여야 합니다.

## SMS 알림을 받으려면
AtomJump.com에서는 이 기능이 무료이며 그래픽 메시지 팝업을 제공하는 앱으로 대체되었습니다. 그러나 이 지침은 서버의 다른 설치에도 여전히 적용될 수 있습니다.

왼쪽 하단의 '설정'을 클릭하세요. '더 보기'를 클릭하고 앞에 더하기 기호 '+'나 공백 없이 국제 전화번호를 입력하세요. '+64 3 384 5613'은 '6433845613'으로 입력해야 합니다.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

제공업체로부터 비용이 발생할 수 있으므로(예: 메시지당 16c) 제공업체의 지침을 확인하세요.

## 앱 알림을 받으려면

앱 알림은 가능하지만 AtomJump Messaging에서는 눈에 띄지 않습니다. 일반적으로 AtomJump 뒤에 있는 사람들은 조용한 생활을 선호하므로 이메일을 처리하는 것처럼 메시지 알림을 처리하는 것이 가장 좋습니다. 다른 사람이 확인하기를 원할 때가 아니라 확인하고 싶을 때 새 메시지를 확인합니다. 즉, 귀하는 귀하의 공간을 통제할 수 있으며 귀하의 전화기에서 침묵을 바라는 다른 사람들의 일반적인 바람을 존중합니다.

포럼의 웹 주소로 이동합니다. '설정'을 클릭하세요.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Android 및 iOS용 '팝업 알림 받기' 링크가 있습니다. 개인 서버에 있는 경우 언급된 서버 주소를 복사해야 합니다. 앱 링크를 클릭한 다음, 아직 설치하지 않았다면 Android, iPhone 또는 데스크톱 장치에 '설치' 링크의 단계에 따라 앱을 "설치"하세요. 이렇게 하면 다시 방문할 수 있는 홈페이지 아이콘이 생성됩니다.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

앱 내에서 <span class="notranslate">"Register on an AtomJump group or a Private Server" ("AtomJump 그룹 또는 개인 서버에 등록")</span>을 탭하고xyz.atomjump.com 페이지의 그룹 이름을 입력하거나(xyz.atomjump.com의 경우 'xyz' 입력) 비공개 포럼에 언급된 URL을 '설정' 아래에 붙여넣습니다.

로그인하라는 메시지가 표시되며 계정을 만들거나 기존 계정을 입력할 수 있습니다. 이 작업이 완료되면 시작 앱을 해당 서버의 이메일 주소와 연결했다는 내용이 브라우저에 표시되고 시작 앱 자체에 <span class="notranslate">‘Listening for Messages’ ('메시지 듣기')</span>가 표시됩니다.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

이는 앱이 열릴 때 이 계정에 대한 모든 개인 메시지가 팝업되고, 일부 기기(예: 데스크톱/안드로이드)에서는 앱이 백그라운드 탭에서 실행 중인 경우 핑 소리를 낸다는 의미입니다. 그러나 많은 Android 휴대폰에서는 휴대폰이 절전 모드로 전환되면 핑 소리가 들리지 않습니다.

메시지의 전체 버전을 보려면 이 탭을 탭한 다음 <span class="notranslate">‘Open Forum’ ('포럼 열기')</span>를 탭하여 포럼에 들어가 응답할 수 있습니다.

언제든지 <b>메시지 알림을 중지</b>하려면 특정 서버에 대한 '등록을 취소'하면 됩니다. 로그아웃된 동안 메시지 알림은 이메일로 전달됩니다. 다시 등록하면 알림이 다시 시작됩니다.

원하는 만큼의 포럼을 구독할 수 있습니다. 위의 <a href="#become-owner">'포럼 구독자가 되려면'</a>을 참조하세요.


## 포럼 바로가기를 저장하려면
휴대폰을 사용하는 경우 AtomJump Messaging 시작 앱을 바탕화면 바로가기로 설치하는 것이 좋습니다.

* https://app.atomjump.com

"AtomJump 그룹 또는 개인 서버에 등록"을 누르고 xyz.atomjump.com 페이지의 그룹 이름을 입력하거나(xyz.atomjump.com의 경우 'xyz' 입력) 비공개 포럼에 언급된 URL을 붙여넣습니다. , '설정'에서.


## 사진을 게시하려면
왼쪽 하단에 있는 '업로드' 아이콘을 클릭하세요. 드라이브에서 .jpg 사진을 선택하고 '업로드'를 클릭하세요. 참고: 두 장 이상의 사진을 선택할 수 있습니다.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

참고: 모바일 장치를 사용하는 경우(예: Android의 경우 '카메라'가 아닌 갤러리에 액세스하려면 '문서'를 선택한 다음 '갤러리'를 선택하세요.

표시되는 썸네일 미리보기를 탭하여 확대/축소 가능한 사진 미리보기를 보도록 선택할 수 있습니다. 전체 미리보기를 닫으려면 사진을 다시 탭하세요.

## 스티커를 게시하려면 **
왼쪽 하단에 있는 웃는 얼굴을 클릭하세요. 그런 다음 스티커를 클릭하세요.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## 메시지를 다운로드하려면
귀하의 계정으로 로그인하십시오. 왼쪽 하단에 있는 '업로드' 아이콘을 클릭하세요. '다운로드' 버튼을 클릭하세요. .xlsx 스프레드시트 파일을 다운로드합니다. 이 파일은 스프레드시트 소프트웨어에서 열 수 있습니다. Office, Google Docs, Microsoft Office를 엽니다.

## 언어를 변경하려면 **
왼쪽 하단의 '설정'을 클릭하세요. 그런 다음 '더보기'를 클릭하고 '언어' 상자를 드롭다운하세요. 당신의 언어를 고르시 오. 그런 다음 '설정 저장'을 클릭하세요. 로그인한 경우 비밀번호를 다시 입력해야 할 수도 있습니다.

참고: '설정' 아이콘이 있는 전면 회색 상자는 주변 사이트에서 이 기능을 구현한 경우에만 언어를 변경하며 브라우저에서 페이지를 새로 고쳐야 할 수도 있습니다. 언어를 변경하면 중앙 상자의 '안내' 메시지가 변경됩니다. 다른 사용자가 입력한 텍스트에는 영향을 주지 않습니다.

## 전체 화면으로 이동하려면 **

**Android 휴대폰**에서 자주 방문하는 AtomJump 메시지 페이지에 있을 때 브라우저의 '설정'(주로 상단 주소 근처에 3개의 막대 아이콘이 있음)으로 이동하여 '홈 화면에 추가' 옵션을 선택하세요. , 또는 이와 유사한 것.

**iPhone**에서 자주 방문하는 AtomJump 메시징 페이지에 있을 때 페이지 하단에 있는 '공유' 아이콘을 탭한 다음 하단에 있는 '홈 화면에 추가' 아이콘이나 이와 유사한 아이콘을 탭하세요.

그런 다음 휴대전화의 홈 화면으로 돌아가서 아이콘을 탭하세요. 그러면 메시징 중에 전체 화면 공간이 사용됩니다.

## 메시지를 삭제하려면

누구나 AtomJump.com의 메시지를 삭제할 수 있으며 포럼이 대부분 자체 조정되도록 이를 허용합니다. 이는 귀하의 메시지가 누군가에 의해 부당하게 삭제되었음을 의미할 수도 있지만, 모든 사람이 승인한 메시지만 포럼에 남게 된다는 의미입니다. 본 정책으로 해결할 수 없는 문제가 있는 경우 개인정보 보호 페이지를 통해 문의해 주세요.

메시지를 삭제하려면 메시지 자체를 탭하거나 클릭하면 아래에 쓰레기통 아이콘이 있는 메시지가 표시됩니다. 쓰레기통 아이콘을 누르면 메시지가 몇 초 안에 제거됩니다.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

참고: 메시지가 게시된 후 다른 로봇 피드가 메시지를 복사할 기회를 갖기 전에 20분(AtomJump의 기본 설치, AtomJump.com 포함) 동안 메시지를 삭제할 수 있습니다.

## 안드로이드에서 이미지 깜박임을 멈추려면

Android 휴대폰에서 비보안 'http' 메시징 서버에 연결하고 휴대폰 브라우저 설정의 '라이트 모드'가 켜져 있는 경우 다음을 찾을 수 있습니다.마법사는 5초마다 깜박이는 것 같습니다. 휴대전화의 Chrome 브라우저 설정 페이지에서 깜박임을 멈추려면 '라이트 모드'를 끄는 것이 좋습니다.

또는 'https' 도메인이 서버와 연결되어 있으면 문제가 사라져야 합니다.
 
 

__** 이 기능은 AtomJump.com에서 기본적으로 켜져 있지만, 공급자가 이러한 플러그인을 포함하지 않은 경우 그렇지 않을 수도 있습니다.__
