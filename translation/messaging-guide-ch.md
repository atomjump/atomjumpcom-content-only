<img src="http://atomjump.org/images/logo80.png">

#atomjumpcom-仅限内容
该存储库是 https://atomjump.com 上重要内容的公共副本，
以防服务器宕机。




# 消息传递用户指南


＃＃ 介绍
AtomJump Messaging 是一个多层“渐进式 Web 应用程序”，用于实时消息传递和论坛通信。 它的核心是一个操作单个论坛的消息传递界面，本指南将教您如何操作该界面。 您可以通过单击网站上的特定链接来查找 AtomJump 论坛，这将在浏览器的弹出框中打开该论坛。 任何网站都可以运营这些论坛，AtomJump.com 就是其中之一。

有一个可选的入门应用程序（点击主页图标后在浏览器中运行），它允许您在一个地方收集您最喜欢的论坛，并检查来自互联网上任何 AtomJump Messaging 论坛的新消息。 所有这些组件都是开源且免费的。 AtomJump Messaging 是一个注册的“更安全的应用程序”。

* https://atomjump.com/wp/safer-apps/

作为渐进式 Web 应用程序，与第一代应用程序商店消息应用程序相比，具有某些优势：

* 您可以在手机上讨论某些内容，然后立即切换到同一对话的桌面版本，甚至可能无需登录。
* 只需发送一个网络链接，您就可以邀请一群之前没有登录过 AtomJump 的人加入论坛。 他们不必创建帐户。
* 对于加入对话的新人来说，无需下载大型应用程序，手机上有大量安全请求。
* 每个不同的论坛可以由不同的组织运营，并且该论坛的功能由运营商选择，并由基本构建块（称为“插件”）组合在一起。

首先，您可以尝试论坛，例如 https://atomjump.com

要获取可选的 AtomJump Messaging 入门应用程序（iPhone/Android/桌面）：

* https://app.atomjump.com

## 发表留言
如果您访问 AtomJump.com，请单击蓝色大“聊天”按钮进入论坛。 其他使用 AtomJump 软件的网站将在您单击特定链接后显示消息弹出窗口。

开始在顶部的“输入您的评论”框中输入内容，然后单击“发送”。 就是这样！ 您不需要帐户，但如果您没有透露自己的姓名，您将被称为“Anon XX”。


## 撰写消息
您可以为各种类型的媒体输入一些常见的消息传递快捷方式。

__``输入``__ 创建

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``哈哈`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭``😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``请支付 5 美元给我`` <a href="https://www.paypal.com/webapps/xorouter/ paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bXAlMm VJB20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY29kZSUz ZFVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhynRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0ZWRHdWV zdCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">请支付 5 美元给我< /a> [带有 Paypal 链接]

``请支付 3.30 英镑给我`` <a href="https://www.paypal.com/webapps/xorouter/ paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bX AlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW5jeV9 jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGULM2RzZXJ2aWNlcyUyNm5vX25vdGULM2QwJTI2Ym4lM2RQUCUyZEJ1EU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYlMjuz YU5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">请支付 3.30 英镑给我< /a> [带有 Paypal 链接]

``http://yoururl.com/yourvideo.mp4`` [显示一个灰色视频图标，点击]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [显示视频图片，点击]

``london@`` <a href="http://london.atomjump.com">london@</a> [链接到 http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [链接到 http://london.atomjump.com]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text. com">展开</a> [包含指向 http://a.very.long.link.with.lots.of.text.com 的链接]

``http://yoururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [显示图片]

``http://yoururl.com/youraudio.mp3`` [显示要单击的灰色耳朵图标]

``http://yoururl.com/yourdoc.pdf`` [显示要单击的 PDF 图标]


** 此功能在 AtomJump.com 上默认启用，但如果您的提供商未包含这些插件，则可能不会启用。

## 参加视频会议

当您在论坛中时，单击“发送”按钮旁边的相机图标。 这将在进入视频论坛之前提供确认屏幕。 某些浏览器需要首先在您的浏览器权限中接受相机。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

手机用户可能更喜欢下载单独的应用程序。

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

要认识某人，您最好向其他人发送 AtomJump 论坛的 URL（在 AtomJump.com 上您可以单击共享图标）并告诉他们单击相机。 您可以在 Jitsi 中找到更多说明。 https://jitsi.org/user-faq/

注意：这些视频论坛是公开的，除非您在私人论坛上，虽然您必须在新用户加入之前接受他们，但在此之后您应该谨慎对待向其他人展示的内容。 如果您想在这里获得隐私，您应该启动一次性会话，或购买私人论坛。

替代方法：虽然您应该能够在大多数平台上点击进入视频论坛，但某些平台例如 特别是iPad / Internet Explorer，您也可以直接运行桌面/应用程序，并将论坛地址复制到其中。 例如。 对于 AtomJump.com 页面，论坛地址将为“https://jitsi0.atomjump.com/[唯一代码]ajps”，后跟子域。 您还可以通过查看紫色“链接”图标的地址来找到它。


## 给自己命名
单击左下角的“设置”。 将您的名字写入“您的名字”，然后单击“保存设置”。 您不需要完整的帐户，但如果您想在收到私人消息时收到通知，则需要一个帐户（见下文）。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/your-name.png">


## 设置帐户

设置帐户意味着您可以接收私人消息。 单击左下角的“设置”。 将您的姓名写入“您的姓名”，将您的电子邮件地址写入“您的电子邮件”。

或者，您可以为帐户添加密码（这可以防止其他人读取您的私人消息）。 如果您想添加此功能（强烈建议您这样做），请单击“更多”并输入您的密码。

单击“保存设置”。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

您将收到有关私人消息的电子邮件通知，但我们不会将您的电子邮件地址用于任何其他目的。

## 在新电脑或手机上登录
使用您在设置帐户时在第一台设备上输入的相同电子邮件地址和密码。

请记住，如果您使用的是公共电脑，则每次离开电脑时都应该注销，因为系统会记住您的登录详细信息。

注意：每个不同的 AtomJump 服务器安装都有其自己的私有用户数据库。 如果您的帐户是在 AtomJump.com 上设置的，则该帐户可能会在其他 AtomJump.com 属性之间共享，但是，它不一定适用于您可能找到的消息弹出窗口的每个版本，因为这些版本是由其他公司运营的 。

## 给一个人写一封私人消息
单击或点击其中一条消息旁边的人员姓名。 然后输入消息并点击“发送至[他们的名字]”。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

您需要单击“公开”才能开始发布消息以供所有人再次查看。

## 给论坛订阅者写私人消息
单击“转为私有”。 正常输入消息。

一旦您准备好返回公共论坛发帖，请再次单击“公开”。

## 回复 Twitter 用户
单击或点击其中一条消息旁边的人员姓名。 然后输入消息并点击“发送”。

请注意，Twitter 回复将始终是公开的，并且您不需要 Twitter 帐户。 我们的 AtomJump 帐户将自动发送一条消息，说明您当前所在的论坛上有一条消息正在等待他们。 是否会阅读 Twitter 消息并做出回应取决于他们。

## 成为论坛订阅者

如果您<b><i>没有帐户</i></b>，并且只想接收电子邮件通知（无需登录），则可以通过点击并将耳朵切换为绿色来订阅 消息窗口中的“倾听的耳朵”，它会询问您的电子邮件地址。 然后点击“订阅”。

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>未订阅。 点击即可订阅。</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>订阅床。 点击即可取消订阅。</i>

如果您<b><i>有帐户</i></b>，则应先登录。 为此，请单击“设置”，单击密码框的“更多”，然后输入您的电子邮件和密码。 然后点击“登录”。

现在，在消息传递页面中，点击并将耳朵切换到绿色的“聆听耳朵”以订阅该论坛。

要<b>取消订阅</b>已订阅的论坛，请点击并切换到消息传递页面上的红色“非监听耳朵”。

## 设置群组

输入您自己的群组名称或现有的公共群组名称（AtomJump.com 上显示“输入群组名称”），然后单击<span class="notranslate">“AtomJump”</span>。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

例如 输入<span class="notranslate">“sailinglondon”</span>并点击<span class="notranslate">“AtomJump”</span>会将您的浏览器带到以下地址：

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

通过电子邮件、短信、即时消息或其他方式与您的群组共享论坛的网络链接。 您可以通过点击底层页面右上角的“共享”图标在atomjump.com上快速找到此链接。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

然后，每个人都可以选择成为论坛订阅者以获取持续的通知。

<b>高级提示：</b>您可以选择将<span class="notranslate">“/go/”</span>添加到网络链接的末尾，这将立即为您的客人打开实时论坛 （这样他们就不必点击蓝色的大“开始”按钮）。

<b>临时地址</b>

如果您被发送到临时地址（例如 http://sailinglondon.clst4.atomjump.com），并被告知需要 2-48 小时才能完全激活，您可以在 https 上检查域名转移的状态： //dnschecker.org。 输入您页面的预期地址，例如 “sailinglondon.atomjump.com”并搜索。 您需要确认的是，所有返回的地址都不同于“114.23.93.129”（这是现有的atomjump.com家庭地址）。 一旦出现这种情况，每个人都可以看到正确的服务器，并且您可以安全地邀请其他人加入论坛。 如果没有，某些用户可能会在消息论坛上写入消息，但这些消息将被发送到错误的服务器，并且其他用户将看不到。

## 设置私人房间

在 AtomJump․com 上，您可以输入自己独特的房间名称，其中显示<span class="notranslate">“输入群组名称”</span>，例如 <span class="notranslate">“fredfamily”</span>，然后点击底部条目<span class="notranslate">‘Create a PRIVATE room’ (“创建私人房间”)</span>。

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

在 AtomJump․com 上，费用为 15 新西兰元/年（约 10 美元/年）。 您也可以直接在<a href="http://atomjump.org/wp/introduction/">此页面</a>上注册。 在其他站点上，您应该联系系统管理员来添加私人论坛密码。

注意：您决定使用的论坛密码与您的个人密码不同，并且应该是您可以轻松通过电子邮件发送给房间中每个成员的密码。

## 获取短信通知
请注意，在 AtomJump.com 上，它已被免费的应用程序取代，并提供图形消息弹出窗口。 但是，这些说明可能仍然适用于服务器的其他安装。

单击左下角的“设置”。 单击“更多”，然后输入您的国际电话号码，不带前导加号“+”且不带空格，例如 “+64 3 384 5613”应输入为“6433845613”。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

您的提供商可能会收取费用（例如每条消息 16c），因此请检查提供商的说明。

## 获取应用程序通知

应用程序通知虽然可能，但在 AtomJump Messaging 上并不是很明显。 一般来说，AtomJump 背后的人更喜欢安静的生活，所以你最好像对待电子邮件一样对待你的消息通知：当你想要检查新消息时，你检查新消息，而不是当其他人希望你检查它们时。 换句话说，您可以控制自己的空间，并且尊重其他人希望手机保持安静的普遍愿望。

转到论坛的网址。 单击“设置”。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

有一些适用于 Android 和 iOS 的“获取弹出通知”链接。 如果这是在私人服务器上，您将需要复制提到的服务器地址。 单击应用程序链接，如果您尚未执行此操作，请按照 Android、iPhone 或桌面设备上“安装”链接中的步骤“安装”该应用程序。 这将创建一个可以重新访问的主页图标。

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

在应用程序中，点按<span class="notranslate">"Register on an AtomJump group or a Private Server" (“在 AtomJump 群组或私人服务器上注册”)</span>，然后输入您的 xyz.atomjump.com 页面的群组名称（对于 xyz.atomjump.com，您可以输入“xyz”），或粘贴到您的私人论坛上“设置”下提到的 URL。

系统应该要求您登录，您可以创建一个帐户，或输入现有帐户。 如果完成，浏览器应显示您已将入门应用程序与该服务器上的电子邮件地址关联起来，并且入门应用程序本身应显示<span class="notranslate">‘Listening for Messages’ (“监听消息”)</span>。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

这意味着当应用程序打开时，该帐户的任何个人消息都会弹出，并且在某些设备（即桌面/Android）上，如果应用程序在后台选项卡中运行，则会发出 ping 声音。 但是，在许多 Android 手机上，如果手机进入睡眠模式，则不会听到 ping 声音。

点击此选项卡可查看该消息的完整版本，然后点击<span class="notranslate">‘Open Forum’ (“开放论坛”)</span>进入论坛本身，您可以在其中进行回复。

要随时<b>停止消息通知</b>，您可以“取消注册”到特定服务器。 当您注销时，将通过电子邮件发送消息通知。 再次注册将再次开始通知。

您可以订阅任意数量的论坛。 请参阅上面的<a href="#become-owner">“成为论坛订阅者”</a>。


## 保存论坛快捷方式
如果您使用手机，我们建议您将 AtomJump Messaging 入门应用程序安装为桌面快捷方式。

* https://app.atomjump.com

点击“在 AtomJump 群组或私人服务器上注册”，然后输入您的 xyz.atomjump.com 页面的群组名称（对于 xyz.atomjump.com，您需要输入“xyz”），或粘贴您的私人论坛上提到的 URL ，在“设置”下。


## 发布照片
单击左下角的“上传”图标。 从驱动器中选择一张 .jpg 照片，然后单击“上传”。 注意：您可以选择多张照片。

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

注意：如果您使用的是移动设备，例如 Android，要访问图库而不是“相机”，请选择“文档”，然后选择“图库”。

您可以通过点击出现的缩略图预览来选择查看照片的可缩放预览。 再次点击照片可关闭完整预览。

## 发布贴纸 **
单击左下角的笑脸。 然后单击您的贴纸。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## 下载消息
使用您的帐户登录。 单击左下角的“上传”图标。 单击“下载”按钮。 它将下载一个 .xlsx 电子表格文件，该文件可以通过任何电子表格软件打开，例如 打开 Office、Google 文档、Microsoft Office。

## 更改语言 **
单击左下角的“设置”。 然后单击“更多”并下拉“语言”框。 选择你的语言。 然后单击“保存设置”。 如果您已登录，可能需要再次输入密码。

注意：前面带有“设置”图标的灰色框只有在周围站点实现了此功能的情况下才会更改语言，并且您可能需要在浏览器中刷新页面。 更改语言将更改中央框中的“指南”消息。 它不会影响其他用户输入的文本。

## 全屏显示 **

在您的 **Android 手机** 上，当您访问经常访问的 AtomJump 消息页面时，请进入浏览器的“设置”（通常是顶部地址附近的 3 条形图标），然后选择“添加到主屏幕”选项 ，或类似的东西。

在您的 **iPhone** 上，当您访问经常访问的 AtomJump 消息传递页面时，请点击页面底部的“共享”图标，然后点击底部的“添加到主屏幕”图标或类似图标。

然后返回手机的主屏幕并点击该图标。 然后，这将在消息传递期间使用全屏空间。

## 删除消息

任何人都可以删除 AtomJump.com 上的任何消息，我们允许这样做，以便论坛在很大程度上进行自我调节。 虽然这可能意味着您的消息被某人不公平地删除，但这确实意味着只有每个人都认可的消息才会保留在论坛上。 如果您遇到本政策无法解决的问题，请通过隐私页面联系我们。

要删除消息，请点击或单击消息本身，您将看到该消息，其下方带有垃圾箱图标。 点击垃圾箱图标，几秒钟后该消息就会被删除。

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

注意：发布消息后，您有 20 分钟的时间（对于 AtomJump 的默认安装，包括 AtomJump.com）删除该消息，然后其他机器人提要才有机会复制该消息。

## 在 Android 上停止闪烁的图像

在 Android 手机上，如果您连接到不安全的“http”消息服务器，并且手机浏览器设置的“精简模式”已打开，您可能会发现法师似乎每 5 秒左右就会闪烁一次。 我们建议关闭“精简模式”，以停止手机 Chrome 浏览器设置页面中的闪烁。

或者，如果“https”域与服务器关联，问题就会消失。
 
 

__** 此功能在 AtomJump.com 上默认启用，但如果您的提供商未包含这些插件，则可能不会启用。__
