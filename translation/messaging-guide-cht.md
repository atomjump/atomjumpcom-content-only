<img src="http://atomjump.org/images/logo80.png">

#atomjumpcom-僅限內容
該儲存庫是 https://atomjump.com 上重要內容的公開副本，
以防伺服器宕機。




# 訊息傳遞使用者指南


＃＃ 介紹
AtomJump Messaging 是一個多層“漸進式 Web 應用程式”，用於即時訊息和論壇通訊。 它的核心是一個操作單一論壇的訊息傳遞介面，本指南將教您如何操作該介面。 您可以透過點擊網站上的特定連結來尋找 AtomJump 論壇，這將在瀏覽器的彈出框中開啟該論壇。 任何網站都可以經營這些論壇，AtomJump.com 就是其中之一。

有一個可選的入門應用程式（點擊主頁圖示後在瀏覽器中運行），它允許您在一個地方收集您最喜歡的論壇，並檢查來自互聯網上任何 AtomJump Messaging 論壇的新訊息。 所有這些元件都是開源且免費的。 AtomJump Messaging 是一個註冊的「更安全的應用程式」。

* https://atomjump.com/wp/safer-apps/

作為漸進式 Web 應用程序，與第一代應用程式商店訊息應用程式相比，具有某些優勢：

* 您可以在手機上討論某些內容，然後立即切換到相同對話的桌面版本，甚至可能無需登入。
* 只需發送一個網絡鏈接，您就可以邀請一群之前沒有登入過 AtomJump 的人加入論壇。 他們不必創建帳戶。
* 對於加入對話的新人來說，無需下載大型應用程序，手機上有大量安全請求。
* 每個不同的論壇可以由不同的組織運營，並且該論壇的功能由運營商選擇，並由基本構建塊（稱為“插件”）組合在一起。

首先，您可以嘗試論壇，例如 https://atomjump.com

要取得可選的 AtomJump Messaging 入門應用程式（iPhone/Android/桌面）：

* https://app.atomjump.com

## 留言
如果您訪問 AtomJump.com，請點擊藍色大「聊天」按鈕進入論壇。 其他使用 AtomJump 軟體的網站將在您點擊特定連結後顯示訊息彈出視窗。

開始在頂部的「輸入您的評論」方塊中輸入內容，然後按一下「傳送」。 就是這樣！ 您不需要帳戶，但如果您沒有透露自己的姓名，您將被稱為「Anon XX」。


## 撰寫訊息
您可以為各種類型的媒體輸入一些常見的訊息傳遞捷徑。

__``輸入``__ 創建

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``哈哈`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭``😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

『請付5 美元給我`` <a href="https://www.paypal.com/webapps/xorouter/ paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWN m VJB20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjMmUlMmUlM TRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhynRuX2J1eW5vdNDXlOb3dCRiUyNTNhynRuX2J1eW5vdND UyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">請付5 美元給我< /a> [帶有 Paypal 連結]

``請付3.30 英鎊給我`` <a href="https://www.paypal.com/webapps/xorouter/ paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsa​​iwvRwFzZHRydWUlMjZjbWQlM2RfeGN0 X AlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhW91TI1MjBwYXklMmUlMmUlMmUlMjZhW91Nb lJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGULM2RzZXJ2aWNlcyUyNm5vX25vdGULM2QwJTI2Ym4lM2RQUCUyZEJ1EU5vd0JGJTI1M2Fid530YndvY lZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">請支付3.30 英鎊給我< /a> [帶有 Paypal 連結]

``http://yoururl.com/yourvideo.mp4`` [顯示一個灰色視訊圖標，點擊]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [顯示影片圖片，點擊]

``london@`` <a href="http://london.atomjump.com">london@</a> [連結至 http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [連結至 http://london.atomjump.com]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text. com">展開</a> [包含指向http://a.very.long.link.with.lots.of.text.com 的連結]

``http://yoururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [顯示圖片]

``http://yoururl.com/youraudio.mp3`` [顯示要點擊的灰色耳朵圖示]

``http://yoururl.com/yourdoc.pdf`` [顯示要點選的 PDF 圖示]


** 此功能在 AtomJump.com 上預設為啟用，但如果您的提供者未包含這些插件，則可能不會啟用。

## 參加視訊會議

當您在論壇中時，請按一下「傳送」按鈕旁的相機圖示。 這將在進入視訊論壇之前提供確認畫面。 某些瀏覽器需要先在您的瀏覽器權限中接受相機。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

手機用戶可能更喜歡下載單獨的應用程式。

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

要認識某人，您最好向其他人發送 AtomJump 論壇的 URL（在 AtomJump.com 上您可以單擊共享圖標）並告訴他們單擊相機。 您可以在 Jitsi 中找到更多說明。 https://jitsi.org/user-faq/

注意：這些影片論壇是公開的，除非您在私人論壇上，雖然您必須在新用戶加入之前接受他們，但在此之後您應該謹慎對待向其他人展示的內容。 如果您想在這裡獲得隱私，您應該啟動一次性會話，或購買私人論壇。

替代方法：雖然您應該能夠在大多數平台上點擊進入視訊論壇，但某些平台例如 特別是iPad / Internet Explorer，您也可以直接運行桌面/應用程序，並將論壇地址複製到其中。 例如。 對於 AtomJump.com 頁面，論壇地址將為“https://jitsi0.atomjump.com/[唯一程式碼]ajps”，後面跟著子網域。 您也可以透過查看紫色「連結」圖示的地址來找到它。


## 為自己命名
點選左下角的“設定”。 將您的名字寫入“您的名字”，然後按一下“儲存設定”。 您不需要完整的帳戶，但如果您想在收到私人訊息時收到通知，則需要帳戶（請參閱下文）。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/your-name.png">


## 設定帳戶

設定帳戶意味著您可以接收私人訊息。 點選左下角的“設定”。 將您的姓名寫入“您的姓名”，將您的電子郵件地址寫入“您的電子郵件”。

或者，您可以為帳戶添加密碼（這可以防止其他人讀取您的私人訊息）。 如果您想添加此功能（強烈建議您這樣做），請按一下「更多」並輸入您的密碼。

點選“儲存設定”。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

您將收到私人訊息的電子郵件通知，但我們不會將您的電子郵件地址用於任何其他目的。

## 在新電腦或手機上登錄
使用您在設定帳戶時在第一台裝置上輸入的相同電子郵件地址和密碼。

請記住，如果您使用的是公共電腦，則每次離開電腦時都應該登出，因為系統會記住您的登入詳細資訊。

注意：每個不同的 AtomJump 伺服器安裝都有自己的私人使用者資料庫。 如果您的帳戶是在AtomJump.com 上設定的，則該帳戶可能會在其他AtomJump.com 屬性之間共用，但是，它不一定適用於您可能找到的訊息彈出視窗的每個版本，因為這些版本是由其他公司經營的。

## 給一個人寫一封私人訊息
點擊或點擊其中一條訊息旁邊的人員姓名。 然後輸入訊息並點擊“發送至[他們的名字]”。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

您需要點擊“公開”才能開始發布訊息以供所有人再次查看。

## 寫私人訊息給論壇訂閱者
按一下“轉為私有”。 正常輸入訊息。

一旦您準備好返回公共論壇發帖，請再次點擊「公開」。

## 回覆 Twitter 用戶
點擊或點擊其中一條訊息旁邊的人員姓名。 然後輸入訊息並點擊“發送”。

請注意，Twitter 回覆將始終是公開的，並且您不需要 Twitter 帳戶。 我們的 AtomJump 帳戶將自動發送一條訊息，說明您目前所在的論壇上有一條訊息正在等待他們。 是否會閱讀 Twitter 訊息並回應取決於他們。

## 成為論壇訂閱者

如果您<b><i>沒有帳戶</i></b>，並且只想接收電子郵件通知（無需登入），則可以透過點擊並將耳朵切換為綠色來訂閱訊息視窗中的「傾聽的耳朵”，它會詢問您的電子郵件地址。 然後點選“訂閱”。

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>未訂閱。 點擊即可訂閱。</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>訂閱床。 點擊即可取消訂閱。</i>

如果您<b><i>有帳戶</i></b>，則應先登入。 為此，請按一下“設定”，按一下密碼框的“更多”，然後輸入您的電子郵件和密碼。 然後點選「登入」。

現在，在訊息傳遞頁面中，點擊並將耳朵切換到綠色的「聆聽耳朵」以訂閱該論壇。

若要<b>取消訂閱</b>已訂閱的論壇，請點擊並切換到訊息傳遞頁面上的紅色「非監聽耳朵」。

## 設定群組

輸入您自己的群組名稱或現有的公共群組名稱（AtomJump.com 上顯示「輸入群組名稱」），然後按一下<span class="notranslate">「AtomJump」</span>。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

例如 輸入<span class="notranslate">「sailinglondon」</span>並點選<span class="notranslate">「AtomJump」</span>會將您的瀏覽器帶到以下地址：

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

透過電子郵件、簡訊、即時訊息或其他方式與您的群組分享論壇的網路連結。 您可以透過點擊底層頁面右上角的「分享」圖示在atomjump.com上快速找到此連結。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

然後，每個人都可以選擇成為論壇訂閱者以獲取持續的通知。

<b>高級提示：</b>您可以選擇將<span class="notranslate">“/go/”</span>添加到網絡鏈接的末尾，這將立即為您的客人打開實時論壇（這樣他們就不必點擊藍色的大「開始」按鈕）。

<b>臨時位址</b>

如果您被發送到臨時地址（例如 http://sailinglondon.clst4.atomjump.com），並被告知需要 2-48 小時才能完全激活，您可以在 https 上檢查域名轉移的狀態： //dnschecker.org 。 輸入您頁面的預期地址，例如 “sailinglondon.atomjump.com”並搜尋。 您需要確認的是，所有回傳的地址都不同於「114.23.93.129」（這是現有的atomjump.com家庭地址）。 一旦發生這種情況，每個人都可以看到正確的伺服器，並且您可以安全地邀請其他人加入論壇。 如果沒有，某些用戶可能會在訊息論壇上寫入訊息，但這些訊息將被發送到錯誤的伺服器，並且其他用戶將看不到。

## 設置私人房間

在 AtomJump․com 上，您可以輸入自己獨特的房間名稱，其中顯示<span class="notranslate">“Enter a group name”「輸入群組名稱」</span>，例如 <span class="notranslate">「fredfamily」</span>，然後點選底部條目<span class="notranslate">‘Create a PRIVATE room’「建立私人房間」</span>。

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

在 AtomJump․com 上，費用為 15 紐西蘭元/年（約 10 美元/年）。 您也可以直接在<a href="http://atomjump.org/wp/introduction/">此頁面</a>上註冊。 在其他網站上，您應該聯絡系統管理員以新增私人論壇密碼。

注意：您決定使用的論壇密碼與您的個人密碼不同，並且應該是您可以輕鬆透過電子郵件發送給房間中每個成員的密碼。

## 取得簡訊通知
請注意，在 AtomJump.com 上，它已被免費的應用程式取代，並提供圖形訊息彈出視窗。 但是，這些說明可能仍然適用於伺服器的其他安裝。

點選左下角的“設定”。 點擊“更多”，然後輸入您的國際電話號碼，不帶前導加號“+”且不帶空格，例如 “+64 3 384 5613”應輸入為“6433845613”。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

您的提供者可能會收取費用（例如每個訊息 16c），因此請檢查提供者的說明。

## 取得應用程式通知

應用程式通知雖然可能，但在 AtomJump Messaging 上並不是很明顯。 一般來說，AtomJump 背後的人更喜歡安靜的生活，所以你最好像對待電子郵件一樣對待你的訊息通知：當你想要檢查新訊息時，你檢查新訊息，而不是當其他人希望你檢查它們時。 換句話說，您可以控制自己的空間，並尊重其他人希望手機保持安靜的普遍願望。

轉到論壇的網址。 點選“設定”。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

有一些適用於 Android 和 iOS 的「獲取彈出通知」連結。 如果這是在私人伺服器上，您將需要複製提到的伺服器位址。 點擊應用程式鏈接，如果您尚未執行此操作，請按照 Android、iPhone 或桌面裝置上「安裝」連結中的步驟「安裝」該應用程式。 這將創建一個可以重新訪問的主頁圖示。

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

在應用程式中，點選<span class="notranslate">"Register on an AtomJump group or a Private Server"「在 AtomJump 群組或私人伺服器上註冊」</span>，然後輸入您的 xyz.atomjump.com 頁面的群組名稱（對於 xyz.atomjump.com，您可以輸入“xyz”），或貼上到您的私人論壇上“設定”下提到的 URL。

系統應該要求您登錄，您可以建立帳戶，或輸入現有帳戶。 如果完成，瀏覽器應顯示您已將入門應用程式與該伺服器上的電子郵件地址關聯起來，並且入門應用程式本身應顯示<span class="notranslate">‘Listening for Messages’「監聽訊息」</span>。

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

這表示當應用程式開啟時，該帳戶的任何個人訊息都會彈出，並且在某些裝置（即桌面/Android）上，如果應用程式在背景標籤中運行，則會發出 ping 聲音。 但是，在許多 Android 手機上，如果手機進入睡眠模式，則不會聽到 ping 聲音。

點擊此標籤以查看該訊息的完整版本，然後點擊<span class="notranslate">‘Open Forum’「開放論壇」</span>進入論壇本身，您可以在其中回覆。

若要隨時<b>停止訊息通知</b>，您可以「取消註冊」到特定伺服器。 當您登出時，將透過電子郵件發送訊息通知。 再次註冊將再次開始通知。

您可以訂閱任意數量的論壇。 請參閱上面的<a href="#become-owner">“成為論壇訂閱者”</a>。


## 儲存論壇快捷方式
如果您使用手機，我們建議您將 AtomJump Messaging 入門應用程式安裝為桌面捷徑。

* https://app.atomjump.com

點擊“在AtomJump 群組或私人伺服器上註冊”，然後輸入您的xyz.atomjump.com 頁面的群組名稱（對於xyz.atomjump.com，您需要輸入“xyz”），或貼上您的私人論壇上提到的URL ，在「設定」下。


## 發布照片
點擊左下角的“上傳”圖示。 從磁碟機中選擇一張 .jpg 照片，然後按一下「上傳」。 注意：您可以選擇多張照片。

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

注意：如果您使用的是行動設備，例如 Android，要存取圖庫而不是“相機”，請選擇“文件”，然後選擇“圖庫”。

您可以點擊出現的縮圖預覽來選擇查看照片的可縮放預覽。 再次點選照片可關閉完整預覽。

## 發佈貼圖 **
點選左下角的笑臉。 然後點擊您的貼紙。

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## 下載訊息
使用您的帳號登入。 點擊左下角的“上傳”圖示。 點選“下載”按鈕。 它將下載一個 .xlsx 電子表格文件，該文件可以透過任何電子表格軟體打開，例如 開啟 Office、Google 文件、Microsoft Office。

## 更改語言 **
點選左下角的“設定”。 然後點擊“更多”並下拉“語言”框。 選擇你的語言。 然後點擊“儲存設定”。 如果您已登入，可能需要再次輸入密碼。

注意：前面帶有「設定」圖示的灰色方塊只有在周圍網站實現了此功能的情況下才會變更語言，並且您可能需要在瀏覽器中重新整理頁面。 更改語言將更改中央框中的“指南”訊息。 它不會影響其他使用者輸入的文字。

## 全螢幕顯示 **

在您的**Android 手機** 上，當您訪問經常訪問的AtomJump 訊息頁面時，請進入瀏覽器的「設定」（通常是頂部地址附近的3 條形圖示），然後選擇「新增至主螢幕”選項，或類似的東西。

在您的 **iPhone** 上，當您造訪經常造訪的 AtomJump 訊息頁面時，請點擊頁面底部的「分享」圖標，然後點擊底部的「新增至主畫面」圖示或類似圖示。

然後返回手機的主畫面並點擊該圖示。 然後，這將在訊息傳遞期間使用全螢幕空間。

## 刪除訊息

任何人都可以刪除 AtomJump.com 上的任何訊息，我們允許這樣做，以便論壇在很大程度上進行自我調節。 雖然這可能意味著您的訊息被某人不公平地刪除，但這確實意味著只有每個人都認可的訊息才會保留在論壇上。 如果您遇到本政策無法解決的問題，請透過隱私權頁面與我們聯絡。

要刪除訊息，請點擊或點擊訊息本身，您將看到該訊息，其下方帶有垃圾箱圖示。 點擊垃圾箱圖標，幾秒鐘後該訊息就會被刪除。

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

注意：發布訊息後，您有 20 分鐘的時間（對於 AtomJump 的預設安裝，包括 AtomJump.com）刪除該訊息，然後其他機器人提要才有機會複製該訊息。

## 在 Android 上停止閃爍的圖像

在 Android 手機上，如果您連接到不安全的「http」訊息伺服器，而手機瀏覽器設定的「精簡模式」已打開，您可能會發現法師似乎每 5 秒左右就會閃爍一次。 我們建議關閉“精簡模式”，以停止手機 Chrome 瀏覽器設定頁面中的閃爍。

或者，如果「https」網域與伺服器關聯，問題就會消失。
 
 

__** 此功能在 AtomJump.com 上預設啟用，但如果您的提供者未包含這些插件，則可能不會啟用。__
