<img src="http://atomjump.org/images/logo80.png">

# atomjumpcom-solo-contenuto
Questo repository è una copia pubblica dei contenuti importanti su https://atomjump.com,
nel caso in cui i server dovessero essere inattivi.




# Guida per l'utente della messaggistica


## Introduzione
AtomJump Messaging è un'app Web progressiva multilivello per la messaggistica live e la comunicazione nei forum. Al centro c'è un'interfaccia di messaggistica che gestisce un singolo forum e questa guida ti insegnerà come utilizzare questa interfaccia. Puoi trovare i forum AtomJump facendo clic su collegamenti particolari su un sito Web, che aprirà il forum in una finestra pop-up nel tuo browser. Qualsiasi sito Web può gestire questi forum e AtomJump.com è uno di questi siti Web.

Esiste un'app iniziale opzionale (che viene eseguita in un browser dopo aver toccato l'icona della home page), che ti consente di raccogliere i tuoi forum preferiti in un unico posto e controllare la presenza di nuovi messaggi da qualsiasi forum di messaggistica AtomJump su Internet. Tutti questi componenti sono open source e gratuiti. AtomJump Messaging è una "Safer App" registrata.

* https://atomjump.com/wp/safer-apps/

Essendo un'app Web progressiva, ci sono alcuni vantaggi rispetto alle app di messaggistica dell'app store di prima generazione:

* Puoi discutere di qualcosa sul tuo telefono e passare immediatamente alla versione desktop della stessa conversazione, potenzialmente senza nemmeno effettuare l'accesso.
* Puoi invitare a un forum un gruppo di persone che non hanno mai effettuato l'accesso ad AtomJump, semplicemente inviando un collegamento web. Non devono creare un account.
* Per consentire a una nuova persona di unirsi alla conversazione, non è necessario scaricare ingombranti app di grandi dimensioni, con molte richieste di sicurezza sul telefono.
* Ogni forum diverso può essere gestito da un'organizzazione diversa e la funzionalità di quel forum viene scelta dall'operatore, essendo composta da elementi costitutivi di base (chiamati "plugin").

Per iniziare puoi provare un forum su es. https://atomjump.com

Per ottenere l'app iniziale AtomJump Messaging opzionale (iPhone/Android/Desktop):

* https://app.atomjump.com

## Pubblica un messaggio
Se sei su AtomJump.com, fai clic sul grande pulsante blu "chat" per accedere a un forum. Altri siti che utilizzano il software AtomJump mostreranno il messaggio pop-up dopo aver fatto clic su un collegamento particolare.

Inizia a digitare nella casella "Inserisci il tuo commento" in alto e fai clic su "invia". Questo è tutto! Non hai bisogno di un account, anche se ti chiamerai "Anon XX" se non ti sei nominato.


## Per comporre un messaggio
È possibile inserire alcune scorciatoie di messaggistica comuni per vari tipi di media.

__``Invio``__ Crea

``http://link.com`` http://link.com

``link.com`` <a href="http://link.com">link.com</a>

``:)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/smiley.png"> **

``:(`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sad.png"> **

``;)`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/wink.png"> **

``lol`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/lol.png"> **

``😜😀😊😇⚽️🎎🦊💭`` 😜😀😊😇⚽️🎎🦊💭
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/iphone-emoji.jpg">
<img src="https://atomjump.com/wp/wp-content/uploads/2023/08/android-emoji.jpg">

``Per favore pagami 5 dollari" Vjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwVHJ5JTI1MjBwYXklMjUyMDUlMmUlMmUlMmUlMjZhbW91bnQlM2Q1JTI2Y3VycmVuY3lfY29kZSUz ZFVTRCUyNmJ1dHRvbl9zdWJ0eXBlJTNkc2VydmljZXMlMjZub19ub3RlJTNkMCUyNmJuJTNkUFAlMmRCdXlOb3dCRiUyNTNhYnRuX2J1eW5vd0NDX0xHJTJlZ2lmJTI1M2FOb25Ib3N0ZWRH dWVzdCUyNiUyNndhX3R5cGUlM2RCdXlOb3clMjY&flowlogging_id=e9fa8a7c2f07f#/checkout/login">Per favore pagami 5 dollari< /a> [con collegamento a Paypal]

``Per favore pagami 3,30 sterline`` <a href="https://www.paypal.com/webapps/xorouter/paymentstandard?xclick_params=JTI2QXV0b1JldHVybiUzZHRydWUlMjZjbWQlM2RfeGNsaWNrJTI2YnVzaW5lc3MlM2RwZXRlciUyNTQwYXRvbWp1bX AlMmVjb20lMjZsYyUzZFVTJTI2aXRlbV9uYW1lJTNkQXRvbUp1bXAlMjUzYSUyNTIwUGxlYXNlJTI1MjBwYXklMmUlMmUlMmUlMjZhbW91bnQlM2QzJTJlMzAlMjZjdXJyZW5jeV9 jb2RlJTNkR0JQJTI2YnV0dG9uX3N1YnR5cGUlM2RzZXJ2aWNlcyUyNm5vX25vdGUlM2QwJTI2Ym4lM2RQUCUyZEJ1eU5vd0JGJTI1M2FidG5fYnV5bm93Q0NfTEclMmVnaWYlMjUz YU5vbkhvc3RlZEd1ZXN0JTI2JTI2d2FfdHlwZSUzZEJ1eU5vdyUyNg&flowlogging_id=2edf7cf13321e#/checkout/login">Per favore pagami 3,30 sterline< /a> [con collegamento a Paypal]

``http://yoururl.com/yourvideo.mp4`` [mostra un'icona video grigia, su cui cliccare]
``https://atomjumpurl/api/images/im/upl8-32601688.mp4`` [mostra un'immagine del video, su cui cliccare]

``london@`` <a href="http://london.atomjump.com">london@</a> [con collegamento a http://london.atomjump.com]

``london.atomjump.com`` <a href="http://london.atomjump.com">london@</a> [con collegamento a http://london.atomjump.com]

``http://a.very.long.link.with.lots.of.text.com`` <a href="http://a.very.long.link.with.lots.of.text. com">Espandi</a> [con collegamento a http://a.very.long.link.with.lots.of.text.com]

"http://yoururl.com/yourpic.jpg`` <img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sunset_nelson-150x150.jpg"> [mostra l'immagine]

``http://yoururl.com/youraudio.mp3`` [mostra un'icona a forma di orecchio grigia su cui fare clic]

``http://yoururl.com/yourdoc.pdf`` [mostra un'icona PDF su cui cliccare]


** Questa funzionalità è attiva per impostazione predefinita su AtomJump.com, ma potrebbe non esserlo se il tuo provider non ha incluso questi plug-in.

## Alla videoconferenza

Quando sei in un forum, fai clic sull'icona della fotocamera accanto al pulsante "Invia". Ciò fornirà una schermata di conferma prima di accedere al forum video. Alcuni browser richiedono prima che la fotocamera venga accettata nelle autorizzazioni del browser.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/video-cam.png">

Gli utenti del telefono potrebbero preferire scaricare un'app separata.

* https://itunes.apple.com/us/app/jitsi-meet/id1165103905
* https://play.google.com/store/apps/details?id=org.jitsi.meet
* https://jitsi.org/downloads/

Per incontrare qualcuno, è meglio inviare ad altre persone un URL del forum AtomJump (su AtomJump.com puoi fare clic sull'icona di condivisione) e dire loro di fare clic sulla fotocamera. Troverai ulteriori istruzioni su Jitsi. https://jitsi.org/user-faq/

Nota: questi forum video sono pubblici, a meno che tu non sia su un forum privato, e anche se devi accettare nuovi utenti prima che si uniscano, dopodiché dovresti essere cauto con ciò che mostri ad altre persone. Se desideri privacy qui, dovresti avviare una sessione una tantum o acquistare un forum privato.

Metodo alternativo: anche se dovresti essere in grado di accedere al forum video sulla maggior parte delle piattaforme, alcune piattaforme, ad es. in particolare iPad/Internet Explorer, in alternativa puoi eseguire direttamente il desktop/l'app e copiarvi l'indirizzo del forum. Per esempio. Per le pagine AtomJump.com l'indirizzo del forum sarà "https://jitsi0.atomjump.com/[un codice univoco]ajps" seguito dal sottodominio. Puoi trovarlo anche guardando l'indirizzo dell'icona viola "link".


## Per darti un nome
Fai clic su "Impostazioni" nell'angolo in basso a sinistra. Scrivi il tuo nome in "Il tuo nome" e fai clic su "Salva impostazioni". Non hai bisogno di un account completo, ma se vuoi ricevere notifiche quando ricevi un messaggio privato, ne avrai bisogno (vedi sotto).

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/tuo-nome.png">


## Per creare un account

Configurare un account significa che puoi ricevere messaggi privati. Fai clic su "Impostazioni" nell'angolo in basso a sinistra. Scrivi il tuo nome in "Il tuo nome" e il tuo indirizzo email in "La tua email".

Facoltativamente, puoi aggiungere una password all'account (che impedisce a qualcun altro di leggere i tuoi messaggi privati). Se desideri aggiungerlo, ed è altamente raccomandato, fai clic su "Altro" e inserisci la password.

Fai clic su "Salva impostazioni".

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/settings.png">

Riceverai notifiche via email sui messaggi privati, ma non utilizziamo il tuo indirizzo email per nessun altro scopo.

## Per accedere su un nuovo PC o telefono
Utilizza la stessa email e password che hai inserito sul tuo primo dispositivo quando hai configurato l'account.

Ricorda, se ti trovi su un PC pubblico, dovresti disconnetterti ogni volta che lasci il PC, poiché i tuoi dati di accesso verranno ricordati.

Nota: ogni diversa installazione di AtomJump Server ha il proprio database di utenti privati. Se il tuo account è stato configurato su AtomJump.com, l'account potrebbe essere condiviso tra altre proprietà AtomJump.com, ma non funzionerà necessariamente con tutte le versioni del popup di messaggistica che potresti trovare, perché queste sono gestite da altre società .

## Per scrivere un messaggio privato a una persona
Fai clic o tocca il nome della persona accanto a uno dei suoi messaggi. Quindi inserisci un messaggio e tocca "Invia a [loro nome]".

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/send-privately-288x300.png">

Dovrai fare clic su "Rendi pubblico" per iniziare a pubblicare messaggi affinché tutti possano vederli di nuovo.

## Per scrivere un messaggio privato agli iscritti al forum
Fai clic su "Vai privato". Inserisci i messaggi normalmente.

Una volta che sei pronto per tornare a postare nel forum pubblico, fai nuovamente clic su "Go Public".

## Per rispondere a un utente Twitter
Fai clic o tocca il nome della persona accanto a uno dei suoi messaggi. Quindi inserisci un messaggio e tocca "Invia".

Tieni presente che una risposta Twitter sarà sempre pubblica e non è necessario un account Twitter. Un messaggio verrà inviato automaticamente dal nostro account AtomJump dicendo che c'è un messaggio che li attende sul forum in cui ti trovi attualmente. Spetta a loro decidere se leggere i loro messaggi Twitter e rispondere.

## Per diventare un iscritto al forum

Se <b><i>non hai un account</i></b> e desideri solo ricevere notifiche via email (senza dover accedere), puoi iscriverti toccando e spostando l'orecchio sul verde 'orecchio in ascolto' nella finestra di messaggistica, che ti chiederà il tuo indirizzo email. Quindi fare clic su "Iscriviti".

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/not-listening.png">
<i>Non iscritto. Tocca per iscriverti.</i>

* <img src="http://atomjump.org/wp/wp-content/uploads/2021/01/listening.png">
<i>Iscrivitiletto. Tocca per annullare l'iscrizione.</i>

Se <b><i>hai un account</i></b>, devi prima effettuare l'accesso. Per fare ciò, fai clic su "Impostazioni", fai clic su "Altro" per la casella della password e inserisci la tua email e la password. Quindi fare clic su "Accedi".

Ora nella pagina dei messaggi, tocca e sposta l'orecchio sull'"orecchio in ascolto" verde per iscriverti a quel forum.

Per <b>annullare l'iscrizione</b> a un forum a cui sei iscritto, tocca e passa all'"orecchio che non ascolta" rosso nella pagina dei messaggi.

## Per impostare un gruppo

Inserisci il nome del tuo gruppo o il nome di un gruppo pubblico esistente, dove è indicato "Inserisci un nome di gruppo" su AtomJump.com e fai clic su <span class="notranslate">"AtomJump"</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/group-name-290x300.png">

per esempio. Entrando <span class="notranslate">"sailinglondon"</span> e toccando <span class="notranslate">"AtomJump"</span> porterai il tuo browser al seguente indirizzo:

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/sailinglondon.png">

Condividi il collegamento web del tuo forum con il tuo gruppo tramite e-mail, SMS, messaggio istantaneo o altri mezzi. Puoi trovare rapidamente questo collegamento su atomjump.com toccando l'icona "condividi" nell'angolo in alto a destra della pagina sottostante.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/share-300x192.png">

Ciascun individuo può quindi scegliere di iscriversi al forum per ricevere notifiche continue.

<b>Suggerimento avanzato:</b> puoi facoltativamente aggiungere <span class="notranslate">“/go/”</span> alla fine del tuo collegamento web, che aprirà immediatamente il forum live per i tuoi ospiti (in modo che non debbano toccare il grande pulsante blu "vai").

<b>Indirizzi temporanei</b>

Se vieni inviato a un indirizzo temporaneo (ad esempio http://sailinglondon.clst4.atomjump.com) e ti viene detto che ci vorranno dalle 2 alle 48 ore per essere completamente attivo, puoi verificare lo stato del trasferimento del dominio su https: //dnschecker.org. Inserisci l'indirizzo previsto per la tua pagina, ad es. "sailinglondon.atomjump.com" e cerca. Ciò che devi confermare è che tutti gli indirizzi restituiti siano diversi da "114.23.93.129" (che è l'indirizzo di casa esistente di atomjump.com). Una volta fatto questo, tutti potranno vedere il server corretto e sarai sicuro di invitare altre persone al forum. In caso contrario, c'è la possibilità che alcuni utenti scrivano nel forum dei messaggi, ma quei messaggi verranno inviati al server sbagliato e non saranno visibili agli altri utenti.

## Per allestire una stanza privata

Su AtomJump․com, puoi inserire il nome della tua stanza univoca dove dice <span class="notranslate">"Inserisci un nome di gruppo"</span>, ad es. <span class="notranslate">"fredfamily"</span> e tocca la voce in basso <span class="notranslate">‘Create a PRIVATE room’ ("Crea una stanza PRIVATA")</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2021/01/fredfamily-208x300.png">

Su AtomJump․com, questo costa NZ $ 15 / anno (circa $ US 10 / anno). Puoi anche iscriverti direttamente su <a href="http://atomjump.org/wp/introduction/">questa pagina</a>. Su altri siti, dovresti contattare l'amministratore di sistema per aggiungere una password per il forum privato.

Nota: la password del forum che decidi di avere è diversa dalla tua password personale e dovrebbe essere quella che puoi inviare comodamente via email a ogni membro della stanza.

## Per ricevere notifiche SMS
Nota: su AtomJump.com questo è stato sostituito dall'app, che è gratuita e fornisce popup di messaggi grafici. Tuttavia, queste istruzioni potrebbero essere valide anche per altre installazioni del server.

Fai clic su "Impostazioni" nell'angolo in basso a sinistra. Fai clic su "Altro" e inserisci il tuo numero di telefono internazionale senza il segno più iniziale "+" e senza spazi, ad es. "+64 3 384 5613" deve essere inserito come "6433845613".

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/sms-phone.png">

Probabilmente ci sarà un costo da parte del tuo provider (ad esempio 16 centesimi per messaggio), quindi controlla le istruzioni del tuo provider.

## Per ricevere le notifiche delle app

Le notifiche delle app, sebbene possibili, non sono molto evidenti su AtomJump Messaging. In generale, le persone dietro AtomJump preferiscono una vita tranquilla, quindi è meglio trattare le notifiche dei messaggi più come tratti la posta elettronica: controlli la presenza di nuovi messaggi quando vuoi controllarli, non quando tutti gli altri vogliono che tu li controlli. In altre parole, hai il controllo del tuo spazio e rispetti il desiderio generale di silenzio degli altri dal tuo telefono.

Vai all'indirizzo web del forum. Fai clic su "Impostazioni".

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/settings.png">

Sono disponibili alcuni collegamenti a "Ricevi notifiche popup" per Android e iOS. Se si trova su un server privato, dovrai copiare l'indirizzo del server menzionato. Fai clic sul collegamento dell'app e quindi, se non l'hai già fatto, "installa" l'app seguendo i passaggi indicati nel collegamento "Installa" sul tuo dispositivo Android, iPhone o desktop. Questo creerà un'icona della home page che può essere rivisitata.

<img src="http://atomjump.org/wp/wp-content/uploads/2023/09/register-atomjump-app-224x300.png">

All'interno dell'app, tocca <span class="notranslate">"Register on an AtomJump group or a Private Server" ("Registrati su un gruppo AtomJump o un server privato")</span> einserisci il nome del gruppo della tua pagina xyz.atomjump.com (per xyz.atomjump.com dovresti digitare "xyz") o incolla l'URL menzionato nel tuo forum privato, sotto "Impostazioni".

Ti verrà chiesto di accedere e potrai creare un account o inserire un account esistente. Se questa operazione viene completata, il browser dovrebbe mostrare che hai associato la tua app iniziale al tuo indirizzo email su quel server e l'app iniziale stessa dovrebbe dire <span class="notranslate">‘Listening for Messages’ ("Ascolto messaggi")</span>.

<img src="http://atomjump.org/wp/wp-content/uploads/2017/01/messaging-pair-2-2.jpg">

Ciò significa che tutti i messaggi personali per questo account verranno visualizzati quando l'app è aperta e su alcuni dispositivi (ad esempio desktop/Android) verrà emesso un suono ping se l'app è in esecuzione in una scheda in background. Tuttavia, su molti telefoni Android, il suono del ping non verrà emesso se il telefono è entrato in modalità di sospensione.

Tocca questa scheda per una versione completa del messaggio, quindi tocca <span class="notranslate">‘Open Forum’ ("Apri forum")</span> per accedere al forum stesso, che ti consente di rispondere.

Per <b>interrompere le notifiche dei messaggi</b> in qualsiasi momento puoi "Annulla registrazione" su un server particolare. Le notifiche dei messaggi verranno inviate via e-mail mentre sei disconnesso. La nuova registrazione avvierà nuovamente le notifiche.

Puoi iscriverti a qualsiasi numero di forum. Vedi <a href="#become-owner">"Diventare un iscritto al forum"</a> sopra.


## Per salvare un collegamento al forum
Se utilizzi il telefono, ti consigliamo di installare l'app AtomJump Messaging Starter come collegamento sul desktop.

* https://app.atomjump.com

Tocca "Registrati su un gruppo AtomJump o un server privato" e inserisci il nome del gruppo della tua pagina xyz.atomjump.com (per xyz.atomjump.com dovresti digitare 'xyz'), oppure incolla l'URL menzionato nel tuo forum privato , in "Impostazioni".


## Per pubblicare una foto
Fai clic sull'icona "carica" nell'angolo in basso a sinistra. Seleziona una foto .jpg dal tuo disco e fai clic su "Carica". Nota: puoi scegliere più di una foto.

<img src="http://atomjump.com/wp/wp-content/uploads/2020/10/upload.png">

Nota: se stai utilizzando un dispositivo mobile, ad es. Android, per accedere alla tua Galleria anziché alla "Fotocamera", seleziona "Documenti" e poi "Galleria".

Puoi scegliere di visualizzare un'anteprima zoomabile della foto toccando le anteprime in miniatura visualizzate. Tocca nuovamente la foto per chiudere l'anteprima completa.

## Per pubblicare un adesivo **
Fai clic sulla faccina sorridente nell'angolo in basso a sinistra. Quindi fai clic sull'adesivo.

<img src="http://atomjump.com/wp/wp-content/uploads/2017/01/laugh1-150x150.jpg">

## Per scaricare i messaggi
Accedi con il tuo account. Fai clic sull'icona "Carica" nell'angolo in basso a sinistra. Fare clic sul pulsante "Scarica". Verrà scaricato un file di foglio di calcolo .xlsx, che può essere aperto da qualsiasi software per fogli di calcolo, ad es. Open Office, Google Documenti, Microsoft Office.

## Per cambiare lingua **
Fai clic su "Impostazioni" nell'angolo in basso a sinistra. Quindi fare clic su "Altro" e aprire la casella "Lingua". Scegli la tua LINGUA. Quindi fare clic su "Salva impostazioni". Potrebbe essere necessario inserire nuovamente la password se hai effettuato l'accesso.

Nota: la casella grigia anteriore con l'icona "Impostazioni" cambierà lingua solo se il sito circostante ha implementato questa funzionalità e potrebbe essere necessario aggiornare la pagina nel browser. Cambiando la lingua cambieranno i messaggi 'guida' nel riquadro centrale. Non influisce sul testo inserito da altri utenti.

## Per passare a schermo intero **

Sul tuo **telefono Android**, quando ti trovi su una pagina di messaggistica AtomJump frequentata, vai nelle "impostazioni" del browser (spesso un'icona a 3 barre vicino all'indirizzo in alto) e seleziona l'opzione "Aggiungi alla schermata Home" , o qualcosa di simile.

Sul tuo **iPhone**, quando ti trovi su una pagina di messaggistica AtomJump frequentata, tocca l'icona "Condividi" nella parte inferiore della pagina, quindi l'icona "Aggiungi alla schermata Home" in basso o qualcosa di simile.

Quindi torna alla schermata iniziale del telefono e tocca l'icona. Questo utilizzerà quindi lo spazio a schermo intero durante la messaggistica.

## Per eliminare i messaggi

Chiunque può eliminare qualsiasi messaggio su AtomJump.com e noi lo consentiamo in modo che i forum siano in gran parte auto moderati. Sebbene ciò possa significare che il tuo messaggio è stato ingiustamente cancellato da qualcuno, significa che solo i messaggi approvati da tutti rimarranno sul forum. Se riscontri problemi che non possono essere risolti con questa politica, ti preghiamo di contattarci nella pagina Privacy.

Per eliminare un messaggio, tocca o fai clic sul messaggio stesso e ti verrà mostrato il messaggio con l'icona del cestino della spazzatura sotto. Tocca l'icona del cestino e il messaggio dovrebbe essere rimosso in pochi secondi.

<img src="http://atomjump.com/wp/wp-content/uploads/2019/03/delete-example-150x150.png">

Nota: dopo che un messaggio è stato pubblicato, hai 20 minuti (per un'installazione predefinita di AtomJump, incluso AtomJump.com) per eliminare il messaggio prima che altri feed robot abbiano la possibilità di copiare il messaggio.

## Per fermare le immagini sfarfallanti su Android

Su un telefono Android, se ti connetti a un server di messaggistica "http" non sicuro e la "Modalità Lite" delle impostazioni del browser del tuo telefono è attivata, probabilmente troverai ii maghi sembrano lampeggiare ogni 5 secondi circa. Ti consigliamo di disattivare la "Modalità Lite" per interrompere lo sfarfallio nella pagina delle impostazioni del browser Chrome del tuo telefono.

In alternativa, se al server è associato un dominio “https”, il problema dovrebbe scomparire.
 
 

__** Questa funzionalità è attiva per impostazione predefinita su AtomJump.com, ma potrebbe non esserlo se il tuo provider non ha incluso questi plug-in.__
