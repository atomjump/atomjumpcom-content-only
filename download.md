<img src="https://atomjump.com/images/logo80.png">

# atomjumpcom-content-only
This repository is a public copy of the important content on https://atomjump.com, 
in case the servers should be down.


# AtomJump Software Downloads


## Latest Stable

* https://frontcdn.atomjump.com/atomjump-messaging/loop-server-4.4.2.zip

## Connector

* https://frontcdn.atomjump.com/atomjump-messaging/loop-1.4.1.zip

## Fast Server

* https://frontcdn.atomjump.com/atomjump-messaging/loop-server-fast-0.7.8.zip

## Branding

* https://frontcdn.atomjump.com/atomjump-messaging/official-atomjump-branding-1.0.2.zip

## Apps

* https://frontcdn.atomjump.com/atomjump-messaging/messaging-1.6.6apache.zip


## Plugins

* https://frontcdn.atomjump.com/atomjump-messaging/change_language-1.1.8.zip
* https://frontcdn.atomjump.com/atomjump-messaging/db_searcher-1.0.3.zip
* https://frontcdn.atomjump.com/atomjump-messaging/emoticons_basic-1.0.3.zip
* https://frontcdn.atomjump.com/atomjump-messaging/emoticons_large-1.1.4.zip
* https://frontcdn.atomjump.com/atomjump-messaging/hate_speech_detector-0.1.1.zip
* https://frontcdn.atomjump.com/atomjump-messaging/help_is_coming-1.1.0.zip
* https://frontcdn.atomjump.com/atomjump-messaging/inserter-1.0.7.zip
* https://frontcdn.atomjump.com/atomjump-messaging/livewiki-1.4.1.zip
* https://frontcdn.atomjump.com/atomjump-messaging/message_stats-0.1.0.zip
* https://frontcdn.atomjump.com/atomjump-messaging/nags_reporter-1.0.5.zip
* https://frontcdn.atomjump.com/atomjump-messaging/notifications-1.9.1.zip
* https://frontcdn.atomjump.com/atomjump-messaging/rss_feed-1.0.4.zip
* https://frontcdn.atomjump.com/atomjump-messaging/shortmail-1.0.4.zip
* https://frontcdn.atomjump.com/atomjump-messaging/medimage_export-0.3.3.zip
* https://frontcdn.atomjump.com/atomjump-messaging/remove_aged-0.0.6.zip
* https://frontcdn.atomjump.com/atomjump-messaging/overflow-0.2.4.zip
