<img src="https://atomjump.com/images/logo80.png">

# atomjumpcom-content-only
This repository is a public copy of the important content on https://atomjump.com, 
in case the servers should be down.



# Contacting AtomJump

We’re headquartered in New Zealand

* __Phone (Reception):__ +64 3 242 0252
* __Phone (2nd line):__ +64 22 162 3660
* __Phone (3rd Line/SMS/After hours NZ 9am – 5pm):__ +64 22 256 6275
* __Note:__ Please leave a phone message if we are not available in the office.
* __E-mail Us:__ webmaster AT atomjump.com
* __Meeting Us:__ Please get in touch via one of the methods above to arrange a meeting.
* __Office Address:__ 99 Richmond Hill Road, Sumner, Christchurch 8081, New Zealand
* __Registered Post:__ AtomJump Ltd.  99 Richmond Hill Road, Sumner, Christchurch 8081, New Zealand

